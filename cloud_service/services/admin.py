from django.contrib import admin

from services.models import (
    ActionCloud,
    InstanceActionValueCloud,
    InstanceCloud,
    ServiceCloud,
)

# Register your models here.
admin.site.register(ServiceCloud)
admin.site.register(ActionCloud)
admin.site.register(InstanceCloud)
admin.site.register(InstanceActionValueCloud)
