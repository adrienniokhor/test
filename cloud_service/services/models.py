from datetime import date
from django.db import models
from django.utils import timezone

from django.forms import ModelForm
from django.urls import reverse
from computedfields.models import ComputedFieldsModel, computed
from groupe_service.models import GroupServiceCloud

from user_cloud.models import UserCloud

#
state = [
    ("stopped", "Arrêté"),
    ("started", "Démarré"),
    ("pending", "En cours de Démarrage"),
    ("open", "En cours de Construction"),
    ("deleting", "En cours de Suppression"),
    ("stopping", "En cours d'Arret"),
    ("revoking", "En cours de révocation"),
    ("revoked", "Révoqué"),
    ("error", "Erreur"),
    ("expire", "Expirée"),
]
currency = [("fcfa", "FCFA"), ("euro", "EURO"), ("dollar", "Dollar")]

#
class ActionCloud(ComputedFieldsModel):
    name = models.CharField("Nom", max_length=60, null=False, blank=True)
    description = models.TextField("Description")
    executor = models.CharField("Executeur", max_length=60, null=False, blank=True)
    enabled = models.BooleanField("Statut", default=True)
    dynamic = models.BooleanField("Dynamique", default=False)

    @computed(
        models.CharField("Code", max_length=60, null=False, blank=True),
        depends=[("self", ["id"])],
    )
    def code(self):
        seq = f"00000000{str(self.id)}"
        seq_finale = seq[-8:]
        return f'ACT_{str(date.today().strftime("%Y"))}_{seq_finale}'

    #
    def __str__(self):
        return self.name

    #
    def get_absolute_url(self):
        if self.code:
            return f"/adm/account/list/action/?code={self.code}"
        return reverse("admin_action")


#
class ServiceCloud(ComputedFieldsModel):
    name = models.CharField("Nom", max_length=60, null=False, blank=False)
    price = models.FloatField("Prix", null=False, blank=True)
    currency = models.CharField(
        "Devis", max_length=60, null=False, blank=True, choices=currency, default="fcfa"
    )
    description = models.TextField("Description")
    groupe_service = models.ForeignKey(
        GroupServiceCloud,
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        related_name="services_groupe_service",
    )
    action = models.ManyToManyField(
        ActionCloud, blank=True, related_name="action_service"
    )
    image = models.ImageField(
        "Image", upload_to="service_cloud/photos", null=False, blank=True
    )
    executor = models.CharField("Executeur", max_length=60, null=False, blank=True)
    enabled = models.BooleanField("Statut", default=True)

    @computed(
        models.CharField("Code", max_length=60, null=False, blank=True),
        depends=[("self", ["id"])],
    )
    def code(self):
        seq = f"00000000{str(self.id)}"
        seq_finale = seq[-8:]
        return f'SERV_{str(date.today().strftime("%Y"))}_{seq_finale}'

    #
    def __str__(self):
        return f"{self.groupe_service.name} - {self.name}"

    #
    def get_absolute_url(self):
        if self.code:
            return f"/adm/account/list/service/?code={self.code}"
        return reverse("admin_service")


#
class InstanceCloud(ComputedFieldsModel):
    date_created = models.DateTimeField("Date de création", auto_now_add=True)
    date_revoked = models.DateTimeField("Date de révocation", null=True, blank=True)
    state = models.CharField(
        "Etat", max_length=60, null=False, blank=True, choices=state, default="open"
    )
    service = models.ForeignKey(
        ServiceCloud, on_delete=models.CASCADE, related_name="instance_service"
    )
    owner = models.ForeignKey(
        UserCloud,
        on_delete=models.CASCADE,
        related_name="instance_proprietaire",
        null=True,
        blank=True,
    )
    remote_id = models.CharField(
        "Identifient Distant",
        max_length=100,
        null=False,
        blank=True,
        default="xxxxxxxxxxxxxxxxx",
    )
    build_number = models.CharField(
        "Numéro de build", max_length=60, null=False, blank=True, default="xxxxxx"
    )
    build_date = models.DateTimeField("Date de construction", null=True, blank=True)
    code_commande = models.CharField("Matricule", max_length=60, null=False, blank=True)

    @computed(
        models.CharField("Matricule", max_length=60, null=False, blank=True),
        depends=[("self", ["id"])],
    )
    def code(self):
        seq = f"00000000{str(self.id)}"
        seq_finale = seq[-8:]
        return f'INST_{str(date.today().strftime("%Y"))}_{seq_finale}'

    #
    def __str__(self):
        return self.code

    #
    def get_absolute_url(self):
        if self.code:
            return f"/adm/account/list/instance/?code={self.code}"
        return reverse("admin_instance")

    #
    def get_percent(self):
        if self.state == "expire":
            return "100%"
        try:
            today_now = timezone.now()
            diff_1 = (today_now - self.date_created).total_seconds()
            if diff_1 <= 0:
                return "O%"
            diff_2 = (self.date_revoked - self.date_created).total_seconds()
            if diff_2 <= 0:
                return "100%"
            percent = float((diff_1 / diff_2) * 100)
            return f"{percent:.2f}%"
        except Exception:
            return None


#
class InstanceActionValueCloud(ComputedFieldsModel):
    instance = models.ForeignKey(
        InstanceCloud,
        on_delete=models.CASCADE,
        related_name="instance_action_value_instance",
    )
    action = models.ForeignKey(
        ActionCloud,
        on_delete=models.CASCADE,
        related_name="instance_action_value_action",
    )
    value = models.TextField("Valeur", null=False, blank=True)

    @computed(
        models.CharField("Matricule", max_length=60, null=False, blank=True),
        depends=[("self", ["id"])],
    )
    def code(self):
        seq = f"00000000{str(self.id)}"
        seq_finale = seq[-8:]
        return f'INVA_{str(date.today().strftime("%Y"))}_{seq_finale}'

    #
    def __str__(self):
        return f"{self.instance.code} - {self.action.name} - {self.value}"


#
class ForfaitCloud(ComputedFieldsModel):
    durree = models.PositiveSmallIntegerField(
        "Durrée",
        null=False,
        blank=False,
    )
    reduction = models.PositiveSmallIntegerField(
        "Réduction",
        null=False,
        blank=False,
    )
    enabled = models.BooleanField("Statut", default=True)

    @computed(
        models.CharField("code", max_length=60, null=False, blank=True),
        depends=[("self", ["id"])],
    )
    def code(self):
        seq = f"00000000{str(self.id)}"
        seq_finale = seq[-8:]
        return f'FORFT_{str(date.today().strftime("%Y"))}_{seq_finale}'

    #
    def __str__(self):
        return f"{self.code} - {self.durree} Mois - {self.reduction} %"

    #
    def get_absolute_url(self):
        if self.code:
            return f"/adm/account/list/forfait/?code={self.code}"
        return reverse("admin_forfait")
    class Meta:
        ordering =("reduction",)

# #***********************************************************************************************************************#
#
class ServiceCloudForm(ModelForm):
    class Meta:
        model = ServiceCloud
        fields = [
            "name",
            "price",
            "currency",
            "description",
            "groupe_service",
            "action",
            "image",
            "executor",
        ]


#
class ActionCloudForm(ModelForm):
    class Meta:
        model = ActionCloud
        fields = ["name", "description", "executor", "dynamic"]


#
class InstanceCloudForm(ModelForm):
    class Meta:
        model = InstanceCloud
        fields = ["service", "owner", "state", "date_revoked"]


#
class InstanceActionValueCloudForm(ModelForm):
    class Meta:
        model = InstanceActionValueCloud
        fields = ["instance", "action", "value"]


#
class ForfaitCloudForm(ModelForm):
    class Meta:
        model = ForfaitCloud
        fields = ["durree", "reduction"]
