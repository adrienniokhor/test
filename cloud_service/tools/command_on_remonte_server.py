import contextlib
import os
import socket
from threading import Thread
import paramiko
from io import StringIO
import time


class Commands:
    client = None
    session = None
    transport = None
    shell = None

    def __init__(self, hostname, username, key_string, retry_time=0):
        self.retry_time = retry_time
        self.client = paramiko.SSHClient()
        self.client.load_system_host_keys()
        # self.client.load_host_keys(os.path.expanduser('~/.ssh/known_hosts'))
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        #
        not_really_a_file = StringIO(key_string)
        private_key = (
            self.get_key_obj(paramiko.RSAKey, pkey_obj=not_really_a_file)
            or self.get_key_obj(paramiko.DSSKey, pkey_obj=not_really_a_file)
            or self.get_key_obj(paramiko.ECDSAKey, pkey_obj=not_really_a_file)
            or self.get_key_obj(paramiko.Ed25519Key, pkey_obj=not_really_a_file)
        )
        not_really_a_file.close()
        #
        self.client.connect(hostname, username=username, pkey=private_key)
        self.transport = self.client.get_transport()
        self.session = self.client.get_transport().open_session()
        self.session.invoke_shell()
        #
    def get_key_obj(self, pkeyobj, pkey_file=None, pkey_obj=None, password=None):
        if pkey_file:
            with open(pkey_file) as fo:
                with contextlib.suppress(Exception):
                    return pkeyobj.from_private_key(fo, password=password)
        else:
            try:
                return pkeyobj.from_private_key(pkey_obj, password=password)
            except Exception:
                pkey_obj.seek(0)

    def close_connection(self):
        if(self.client != None):
            self.client.close()

    def run_cmd(self, cmd_list:list):
        outdata = b"\n"
        if not cmd_list:
            cmd_list = ["\n"]
        else:
            cmd_list.insert(0, "\n")
        for command in cmd_list:
            self.session.send(command + "\n")
            while not self.session.exit_status_ready():
                 # Reading from output streams
                while not self.session.recv_ready() and not self.session.recv_stderr_ready():
                    time.sleep(1)
                # Reading from output streams
                time.sleep(1)
                if self.session.recv_ready():
                    # outdata += b"\n" + self.session.recv(1024)
                    # outdata += b"\n" + self.session.recv(len(self.session.in_buffer))
                    # while self.session.recv_ready():
                    #     outdata += b"\n" + self.session.recv(len(self.session.in_buffer))
                    # break
                    while data := self.session.recv(len(self.session.in_buffer)):
                        outdata += b"\n" + data
                        if not self.session.recv_ready():
                            break
                    break
                if self.session.recv_stderr_ready():
                    # outdata += b"\n" + self.session.recv_stderr(1024)
                    # outdata += b"\n" + self.session.recv_stderr(len(self.session.in_stderr_buffer))
                    # while self.session.recv_ready():
                    #     outdata += b"\n" + self.session.recv(len(self.session.in_buffer))
                    while data := self.session.recv_stderr(len(self.session.in_stderr_buffer)):
                        outdata += b"\n" + data
                        if not self.session.recv_stderr_ready():
                            break
                    break
        outdata += b"\n" 
        return outdata


def get_key_obj(pkeyobj, pkey_file=None, pkey_obj=None, password=None):
    if pkey_file:
        with open(pkey_file) as fo:
            try:
                return pkeyobj.from_private_key(fo, password=password)
            except Exception:
                pass
    else:
        try:
            return pkeyobj.from_private_key(pkey_obj, password=password)
        except Exception:
            pkey_obj.seek(0)


class SSH:
    def __init__(self, buffer:str, channel: paramiko.Channel = None):
        self.buffer = buffer
        self.channel = channel
        self.transport = None

    def connect(
        self,
        host,
        user,
        password=None,
        ssh_key=None,
        port=22,
        timeout=30,
        term="xterm",
        pty_width=80,
        pty_height=24,
    ):
        try:
            ssh_client = paramiko.SSHClient()
            ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

            if ssh_key:
                key = (
                    get_key_obj(paramiko.RSAKey, pkey_obj=ssh_key, password=password)
                    or get_key_obj(paramiko.DSSKey, pkey_obj=ssh_key, password=password)
                    or get_key_obj(
                        paramiko.ECDSAKey, pkey_obj=ssh_key, password=password
                    )
                    or get_key_obj(
                        paramiko.Ed25519Key, pkey_obj=ssh_key, password=password
                    )
                )

                ssh_client.connect(
                    username=user, hostname=host, port=port, pkey=key, timeout=timeout
                )
            else:
                ssh_client.connect(
                    username=user,
                    password=password,
                    hostname=host,
                    port=port,
                    timeout=timeout,
                )

            transport = ssh_client.get_transport()
            self.transport = transport
            self.channel = transport.open_session()
            self.channel.get_pty(term=term, width=pty_width, height=pty_height)
            self.channel.invoke_shell()
        except socket.timeout:
            self.buffer = f"{self.buffer}\nla connexion ssh a expiré"
            self.close()
        except Exception as e:
            print("#########################################")
            print(e)
            print("#########################################")
            self.close()

    def resize_pty(self, cols, rows):
        self.channel.resize_pty(width=cols, height=rows)

    def send(self, data):
        try:
            self.channel.send(data)
            self.channel.recv_exit_status()
        except Exception:
            self.close()

    def output(self):
        try:
            while data := self.channel.recv(1024).decode("utf-8"):
                self.buffer = f"{self.buffer}{data}"
        except Exception:
            self.close()

    def close(self):
        self.buffer = f"{self.buffer}\nfermer la connexion"
        self.channel.close()

    def shell(self, data):
        Thread(target=self.send, args=(data,)).start()
        Thread(target=self.output).start()
    def get_console_resulst(self):
        return self.buffer


# def main():
#     key_str = """"""
#     mytest = Commands("35.88.121.4", "ubuntu", key_str.lstrip())
#     #mytest.run_cmd(host_ip=argv[0], cmd_list=argv[ 1 ])
#     # outpout = mytest.run_cmd(["sudo apt-get autoremove -y","curl https://www.deepl.com/translator","sudo apt-get update -y","cd /var","sudo apt-get update -y","ls -la .","sudo apt-get upgrade -y","cat ettt.txt", "pwd"])
#     lsit_commandes= ["git clone https://github.com/adrienniokhor/docker-dolibarr.git" , "ls -la ./docker-dolibarr","cd ./docker-dolibarr;export DOLI_IMAGE_TAG=17.0.2-php8.1;pwd", "docker-compose up -d > output.log 2>&1;docker ps"]
#     #lsit_commandes= ["docker image ls","docker ps -a"]
#     outpout = mytest.run_cmd(lsit_commandes)
#     mytest.close_connection()
#     print(outpout.decode())
#     print("This method is not meant to be used directly")
#     return


# if __name__ == "__main__":
#     main()