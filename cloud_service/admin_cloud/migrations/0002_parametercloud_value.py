# Generated by Django 4.0.4 on 2022-09-12 20:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("admin_cloud", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="parametercloud",
            name="value",
            field=models.CharField(default=-2012, max_length=100),
            preserve_default=False,
        ),
    ]
