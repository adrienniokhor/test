# Generated by Django 4.0.4 on 2022-09-13 19:01

import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("admin_cloud", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="parametercloud",
            name="value",
            field=models.CharField(
                default=2000000, max_length=200, verbose_name="Valeur"
            ),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="parametercloud",
            name="created_date",
            field=models.DateTimeField(
                default=django.utils.timezone.now, verbose_name="Date création"
            ),
        ),
        migrations.AlterField(
            model_name="parametercloud",
            name="description",
            field=models.TextField(verbose_name="Déscription"),
        ),
        migrations.AlterField(
            model_name="parametercloud",
            name="name",
            field=models.CharField(max_length=40, unique=True, verbose_name="Nom"),
        ),
    ]
