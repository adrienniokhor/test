# Generated by Django 4.0.4 on 2022-09-28 19:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("admin_cloud", "0003_sessioncloud"),
    ]

    operations = [
        migrations.DeleteModel(
            name="SessionCloud",
        ),
    ]
