from django import forms
from django.shortcuts import render
import io
from django.http import HttpResponseRedirect
from django.shortcuts import render
from admin_cloud.forms import AddActionForm
from cloud_service.settings import PAGINATION_DEFAULT_PAGINATION

from services.models import ActionCloud, ImageSystemeCloud
from django.core.paginator import Paginator
from django.contrib import messages #import messages
from django.contrib.auth.decorators import user_passes_test


@user_passes_test(lambda u: u.is_superuser,login_url='/adm/account/login',redirect_field_name='next_page')
def addImageSysteme(request):
    if request.method == 'POST':
        form_image_systeme = AddActionForm(request.POST)
        if not form_image_systeme.is_valid():
            return render(request, 'admin_account.html',context={'form_image_systeme':form_image_systeme,'object':'image_systeme','action':'add'})
        data = form_action.cleaned_data
        ImageSysteme = ImageSystemeCloud.objects.create(
            name=data['name'],
            description=data['description'],
            executor=data['executor'],      
        )
        ImageSysteme.save()
        return HttpResponseRedirect('/adm/account/list/action/')
    form_action = AddActionForm()
    print('image_systemeimage_systemeimage_systeme')
    return render(request, 'admin_account.html',context={'form_image_systeme':form_image_systeme,'object':'image_systeme','action':'add'})

#
@user_passes_test(lambda u: u.is_superuser,login_url='/adm/account/login',redirect_field_name='next_page') 
def actionImageSysteme(request):
    if 'activer' in request.POST:
        select_items = request.POST.getlist('select_items')
        print(select_items)
        for item in select_items:
            imagesysteme = ImageSystemeCloud.objects.get(id=int(item))
            if not imagesysteme.enabled:
                imagesysteme.enabled = True
                imagesysteme.save()
    # 
    elif 'deactiver' in request.POST:
        select_items = request.POST.getlist('select_items')
        print(select_items)
        for item in select_items:
            imagesysteme = ImageSystemeCloud.objects.get(id=int(item))
            if imagesysteme.enabled:
                imagesysteme.enabled = False
                imagesysteme.save()
    # 
    elif 'supprimer' in request.POST:
        select_items = request.POST.getlist('select_items')
        print(select_items)
        for item in select_items:
            imagesysteme = ImageSystemeCloud.objects.get(id=int(item))
            imagesysteme.delete()
    return HttpResponseRedirect('/adm/account/list/action/')

#

@user_passes_test(lambda u: u.is_superuser,login_url='/adm/account/login',redirect_field_name='next_page') 
def getImageSyteme(request):
    if code := request.GET.get('code'):
        if image_systeme := ImageSystemeCloud.objects.get(code=code):
            return render(request, 'admin_account.html',context={'image_systeme_detail':image_systeme,'object':'action','action':'list'})
    image_systemes = ImageSystemeCloud.objects.all().order_by('-id')
    pages = Paginator(image_systemes,PAGINATION_DEFAULT_PAGINATION)
    if page_number := request.GET.get('page'):
        page_number = int(page_number)
        if page_number > pages.num_pages or page_number < 1:
            page_number = 1
        page = pages.page(page_number)
    else:
        page=pages.page(1)
    return render(request,'admin_account.html',context={'page':page,'object':'image_systeme','action':'list'})

