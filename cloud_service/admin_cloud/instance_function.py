#
from datetime import datetime

from dateutil.relativedelta import relativedelta

from analytics.models import Event
from api_service.boto3 import boto_operation_ec2
from api_service.jenkins.job import call_jenkins_job
from services.models import InstanceActionValueCloud, InstanceCloud, ServiceCloud


#
#
def runInstance(commande):
    commande_lines = commande.commmande_ligne_commande.all()
    for commande_line in commande_lines.iterator():
        instance_has_erros = False
        service = ServiceCloud.objects.filter(id=commande_line.service.id)[0]
        owner = commande.proprietaire
        for _ in range(commande_line.quantity):
            instance_return = createRemoteInstance(
                service, owner, commande.code, commande_line.durree
            )
            if not instance_return:
                continue
            for action in service.action.iterator():
                return_action_value = executeAction(instance_return, action)
                if not return_action_value:
                    instance_has_erros = True
                    break
            instance_return.state = "error" if instance_has_erros else "open"
            instance_return.save()
    return True


#
def createRemoteInstance(service, owner, code_commande, durree=1):
    build_number = call_jenkins_job(service.executor)
    instance = {
        "service": service,
        "owner": owner,
        "date_created": datetime.now(),
        "state": "open",
        "build_number": build_number,
        "build_date": datetime.now(),
        "code_commande": code_commande,
    }
    instance["date_revoked"] = datetime.now() + relativedelta(months=durree)
    instance_return = InstanceCloud.objects.create(**instance)
    instance_return.save()
    create_instance_event = Event(
        name="Create Instance",
        object="instance",
        code=instance_return.code,
        created=datetime.now(),
    )
    create_instance_event.save()
    return instance_return


#
def executeAction(instance, action):
    remote_id = instance.remote_id
    executor_action = action.executor
    # Call Jenkins API to execute action And Get Value Return
    # Create InstanceActionValue with incoming value, action and instance
    # Return InstanceActionValue if success
    # Return False if fail
    action_dict = {"instance": instance, "action": action, "value": action.name}
    action_return = InstanceActionValueCloud.objects.create(**action_dict)
    action_return.save()
    return action_return


#
# Stop Instance Function
def stopInstance(instance: InstanceCloud):
    info_return = False
    if instance.remote_id:
        result = boto_operation_ec2.stop_instance(instance.remote_id)
        if result and result.get("StoppingInstances"):
            current_state = result.get("StoppingInstances")[0].get("CurrentState") or {
                "Code": -1,
                "Name": "error",
            }
            if (
                current_state.get("Code") != None
                and current_state.get("Name")
                and (
                    (
                        current_state.get("Code") == 64
                        and current_state.get("Name") == "stopping"
                    )
                    or (
                        current_state.get("Code") == 80
                        and current_state.get("Name") == "stopped"
                    )
                )
            ):
                instance.state = "stopped"
                info_return = True
            else:
                instance.state = "error"
                info_return = False
        #
        if instance_instance_value := instance.instance_action_value_instance.all():
            for element in instance_instance_value:
                if element.action.name == "IP":
                    element.value = ""
                    element.save()
        instance.save()
    return info_return


#
# Start Instance Function
def startInstance(instance: InstanceCloud):
    info_return = False
    if instance.remote_id:
        result = boto_operation_ec2.start_instance(instance.remote_id)
        if result and result.get("StartingInstances"):
            current_state = result.get("StartingInstances")[0].get("CurrentState") or {
                "Code": -1,
                "Name": "error",
            }
            if (
                current_state.get("Code") != None
                and current_state.get("Name")
                and (
                    (
                        current_state.get("Code") == 0
                        and current_state.get("Name") == "pending"
                    )
                    or (
                        current_state.get("Code") == 16
                        and current_state.get("Name") == "running"
                    )
                )
            ):
                instance.state = "pending"
                info_return = True

            else:
                instance.state = "error"
                info_return = False

        instance.save()
    return info_return


#
# Terminated Instance Function
def terminateInstance(instance: InstanceCloud):
    info_return = False
    if instance.remote_id:
        result = boto_operation_ec2.terminate_instance(instance.remote_id)
        if result and result.get("TerminatingInstances"):
            current_state = result.get("TerminatingInstances")[0].get(
                "CurrentState"
            ) or {"Code": -1, "Name": "error"}
            if (
                current_state.get("Code") != None
                and current_state.get("Name")
                and current_state.get("Code") == 48
                and current_state.get("Name") == "terminated"
            ):
                info_return = True
    return info_return


#
# Cheick if Instance Exist Function
def check_instance_exists(instance: InstanceCloud):
    if instance.remote_id:
        result = boto_operation_ec2.describe_instance_with_id(instance.remote_id)
        if result and result[0].get("Instances"):
            remote_instance = result[0].get("Instances")[0]
            if remote_instance.get("InstanceId") == instance.remote_id:
                return True
    return False


#
# Revoked Instance Function
def revokedInstance(instance: InstanceCloud, check_instance: bool):
    if check_instance:
        if result := stopInstance(instance):
            instance.state = "revoked"
            instance.save()
        return result
    else:
        try:
            if instance_instance_value := instance.instance_action_value_instance.all():
                for element in instance_instance_value:
                    if element.action.name == "IP":
                        element.value = ""
                        element.save()
            instance.state = "revoked"
            instance.save()
            return True
        except Exception:
            return False


#
# Compare local instance and remote instance
def compareInstance(list_instance_id=None):  # sourcery skip: low-code-quality
    list_remote_remote_id = []
    list_instance_id = []
    if select_items := list_instance_id:
        list_local_instances = InstanceCloud.objects.filter(id__in=list(select_items))
        list_local_remote_id = [
            local_instance.remote_id for local_instance in list_local_instances
        ]

        list_local_id = [local_instance.id for local_instance in list_local_instances]
        result = boto_operation_ec2.describe_list_instances_with_list_id(
            list_local_remote_id
        )

        instances_details_reservations = result.get("Reservations")
        for instances_details in instances_details_reservations:
            for instance in instances_details.get("Instances"):
                if instance.get("InstanceId"):
                    list_remote_remote_id.append(instance.get("InstanceId"))
                    if instance_local := next(
                        (
                            elt
                            for elt in list_local_instances
                            if elt.remote_id == instance.get("InstanceId")
                        ),
                        None,
                    ):
                        list_instance_id.append(instance_local.id)
                        remote_state = instance.get("State")
                        if (
                            remote_state
                            and remote_state.get("Code")
                            and remote_state.get("Name")
                        ):
                            if (
                                remote_state.get("Code") == 0
                                and remote_state.get("Name") == "pending"
                            ):
                                if (
                                    instance_local.state
                                    and instance_local.state == "pending"
                                ):
                                    continue
                                instance_local.state = "pending"
                                instance_local.save()
                            elif (
                                remote_state.get("Code") == 16
                                and remote_state.get("Name") == "running"
                            ):
                                if (
                                    instance_local.state
                                    and instance_local.state == "started"
                                ):
                                    continue
                                instance_local.state = "started"
                                instance_local.save()
                            elif remote_state.get("Code") == 16 and remote_state.get(
                                "Name"
                            ) in ["stopping", "stopped"]:
                                if (
                                    not instance_local.state
                                    or instance_local.state
                                    not in ["stopped", "revoked"]
                                ):
                                    instance_local.state = "stopped"
                                    instance_local.save()
        if diff_instance_with_select := list(
            set(list_local_id) - set(list_instance_id)
        ):
            list_instance_without_corres = [
                next(elt for elt in list_local_instances if elt.id == local_id)
                for local_id in diff_instance_with_select
            ]

            list_id_instance_without_corres = []
            for instance_without_corres in list_instance_without_corres:
                if instance_without_corres.state and instance_without_corres.state in [
                    "open",
                    "expire",
                ]:
                    continue
                list_id_instance_without_corres.append(instance_without_corres.code)
                instance_without_corres.state = "expire"
                instance_without_corres.save()
    else:
        result = boto_operation_ec2.describe_all_instances()
        instances_details_reservations = result.get("Reservations")
        for instances_details in instances_details_reservations:
            for instance in instances_details.get("Instances"):
                if instance.get("InstanceId"):
                    list_remote_remote_id.append(instance.get("InstanceId"))
                    if instance_local := InstanceCloud.objects.filter(
                        remote_id=instance.get("InstanceId")
                    ):
                        instance_local = instance_local[0]
                        list_instance_id.append(instance_local.id)
                        remote_state = instance.get("State")
                        if (
                            remote_state
                            and remote_state.get("Code")
                            and remote_state.get("Name")
                        ):
                            if (
                                remote_state.get("Code") == 0
                                and remote_state.get("Name") == "pending"
                            ):
                                if (
                                    instance_local.state
                                    and instance_local.state == "pending"
                                ):
                                    continue
                                instance_local.state = "pending"
                                instance_local.save()
                            elif (
                                remote_state.get("Code") == 16
                                and remote_state.get("Name") == "running"
                            ):
                                if (
                                    instance_local.state
                                    and instance_local.state == "started"
                                ):
                                    continue
                                instance_local.state = "started"
                                instance_local.save()
                            elif remote_state.get("Code") == 16 and remote_state.get(
                                "Name"
                            ) in ["stopping", "stopped"]:
                                if (
                                    not instance_local.state
                                    or instance_local.state
                                    not in ["stopped", "revoked"]
                                ):
                                    instance_local.state = "stopped"
                                    instance_local.save()
        if list_instance_id:
            missing_remote_instances = InstanceCloud.objects.exclude(
                id__in=list_instance_id
            )

        else:
            missing_remote_instances = InstanceCloud.objects.all()
        for missing_remote_instance in missing_remote_instances:
            if missing_remote_instance.state and missing_remote_instance.state in [
                "open",
                "expire",
            ]:
                continue
            missing_remote_instance.state = "expire"
            missing_remote_instance.save()
        list_local_instances = InstanceCloud.objects.all()
        list_local_remote_id = [
            local_instance.remote_id for local_instance in list_local_instances
        ]
    #


#
# Revoke Instance
def checkExpireInstance(instance: InstanceCloud):
    info_return = False
    if instance.remote_id:
        result = boto_operation_ec2.stop_instance(instance.remote_id)
        if result and result.get("StoppingInstances"):
            current_state = result.get("StoppingInstances")[0].get("CurrentState") or {
                "Code": -1,
                "Name": "error",
            }
            if (
                current_state.get("Code") != None
                and current_state.get("Name")
                and (
                    (
                        current_state.get("Code") == 64
                        and current_state.get("Name") == "stopping"
                    )
                    or (
                        current_state.get("Code") == 80
                        and current_state.get("Name") == "stopped"
                    )
                )
            ):
                instance.state = "expire"
                info_return = True
            else:
                instance.state = "error"
                info_return = False
        #
        if instance_instance_value := instance.instance_action_value_instance.all():
            for element in instance_instance_value:
                if element.action.name == "IP":
                    element.value = ""
                    element.save()
        instance.save()
    return info_return
