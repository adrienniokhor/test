import io

from django import forms
from django.contrib import messages  # import messages
from django.contrib.auth.decorators import user_passes_test
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import render

from admin_cloud.forms import AddActionForm
from cloud_service.settings import PAGINATION_DEFAULT_PAGINATION
from services.models import ActionCloud


@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def addAction(request):
    if request.method == "POST":
        form_action = AddActionForm(request.POST)
        if not form_action.is_valid():
            return render(
                request,
                "admin_account.html",
                context={
                    "form_action": form_action,
                    "object": "action",
                    "action": "add",
                },
            )
        data = form_action.cleaned_data
        Action = ActionCloud.objects.create(
            name=data["name"],
            description=data["description"],
            executor=data["executor"],
        )
        Action.save()
        return HttpResponseRedirect("/adm/account/list/action/")
    form_action = AddActionForm()
    return render(
        request,
        "admin_account.html",
        context={"form_action": form_action, "object": "action", "action": "add"},
    )


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def actionAction(request):
    if "activer" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            action = ActionCloud.objects.get(id=int(item))
            if not action.enabled:
                action.enabled = True
                action.save()
    #
    elif "deactiver" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            action = ActionCloud.objects.get(id=int(item))
            if action.enabled:
                action.enabled = False
                action.save()
    #
    elif "supprimer" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            action = ActionCloud.objects.get(id=int(item))
            action.delete()
    return HttpResponseRedirect("/adm/account/list/action/")


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def getAction(request):
    if code := request.GET.get("code"):
        if action := ActionCloud.objects.get(code=code):
            return render(
                request,
                "admin_account.html",
                context={"action_detail": action, "object": "action", "action": "list"},
            )
    actions = ActionCloud.objects.all().order_by("-id")
    pages = Paginator(actions, PAGINATION_DEFAULT_PAGINATION)
    if page_number := request.GET.get("page"):
        page_number = int(page_number)
        if page_number > pages.num_pages or page_number < 1:
            page_number = 1
        page = pages.page(page_number)
    else:
        page = pages.page(1)
    # messages.debug(request, '%s SQL statements were executed.')
    # messages.info(request, 'Three credits remain in your account.')
    # messages.success(request, 'Profile details updated.')
    # messages.warning(request, 'Your account expires in three days.')
    # messages.error(request, 'Document deleted.')
    return render(
        request,
        "admin_account.html",
        context={"page": page, "object": "action", "action": "list"},
    )
