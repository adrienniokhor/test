import datetime
import io

from django import forms
from django.contrib import messages
from django.contrib.auth.decorators import user_passes_test
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from admin_cloud.forms import UserForm
from analytics.models import Event
from cloud_service.settings import PAGINATION_DEFAULT_PAGINATION
from services.models import UserCloud
from user_cloud.models import UserCloud  # import messages


@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def addUser(request):
    if request.method == "POST":
        form_user = UserForm(request.POST)
        if not form_user.is_valid():
            return render(
                request,
                "admin_account.html",
                context={"form_user": form_user, "object": "user", "action": "add"},
            )
        data = form_user.cleaned_data
        user = UserCloud.objects.create_user(
            first_name=data["first_name"],
            last_name=data["last_name"],
            phone=data["phone"],
            address=data["address"],
            country=data["country"],
            email=data["email"],
            username=data["email"],
        )
        user.save()
        register_user = Event(
            name="Registered",
            object="user",
            code=user.code,
            created=datetime.datetime.now(),
        )
        register_user.save()
        return HttpResponseRedirect("/adm/account/list/user/")
    form_user = UserForm()
    return render(
        request,
        "admin_account.html",
        context={"form_user": form_user, "object": "user", "action": "add"},
    )


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def actionUser(request):
    if "activer" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            user = UserCloud.objects.get(id=int(item))
            if not user.enabled:
                user.enabled = True
                user.save()
    #
    elif "deactiver" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            user = UserCloud.objects.get(id=int(item))
            if user.enabled:
                user.enabled = False
                user.save()
    #
    elif "supprimer" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            user = UserCloud.objects.get(id=int(item))
            user.delete()
    return HttpResponseRedirect("/adm/account/list/user/")


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def getUser(request):
    if code := request.GET.get("code"):
        if user := UserCloud.objects.get(code=code, is_superuser=False):
            return render(
                request,
                "admin_account.html",
                context={"user_detail": user, "object": "user", "action": "list"},
            )
    users = UserCloud.objects.filter(is_superuser=False).order_by("-id")
    pages = Paginator(users, PAGINATION_DEFAULT_PAGINATION)
    if page_number := request.GET.get("page"):
        page_number = int(page_number)
        if page_number > pages.num_pages or page_number < 1:
            page_number = 1
        page = pages.page(page_number)
    else:
        page = pages.page(1)
    return render(
        request,
        "admin_account.html",
        context={"page": page, "object": "user", "action": "list"},
    )


#
