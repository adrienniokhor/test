# importing the necessary libraries
from datetime import date, datetime
from io import BytesIO

from django.http import HttpResponse
from django.template.loader import get_template, render_to_string
from django.views.generic import View
from xhtml2pdf import pisa

from commande.models import CommandeCloud, PaiementCloud


def html_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("utf-8")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type="application/pdf")
    return None


# Creating a class based view
class GeneratePdf(View):
    def get(self, request, *args, **kwargs):
        if code := request.GET.get("code"):
            if commande := CommandeCloud.objects.filter(code=code):
                data = commande[0]
                date_emission = datetime.now()
                if data.state == "paid":
                    state_facture = data.state

                    open("templates/temp.html", "w").write(
                        render_to_string(
                            "facture.html",
                            {
                                "data": data,
                                "state_facture": state_facture,
                                "date_emission": date_emission,
                            },
                        )
                    )
                    pdf = html_to_pdf("temp.html")
                    return HttpResponse(pdf, content_type="application/pdf")
                elif data.state != "paid":

                    open("templates/temp.html", "w").write(
                        render_to_string(
                            "facture.html",
                            {"data": data, "date_emission": date_emission},
                        )
                    )
                    pdf = html_to_pdf("temp.html")
                    return HttpResponse(pdf, content_type="application/pdf")
