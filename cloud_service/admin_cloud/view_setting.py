import re

import phonenumbers
from django.contrib.auth import authenticate, logout
from django.contrib.auth.decorators import user_passes_test
from django.contrib.sessions.models import Session
from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.utils import timezone
from django_countries import countries

from admin_cloud.models import ParameterCloud
from user_cloud.models import UserCloud


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def adminSetting(request):
    admins = UserCloud.objects.filter(is_superuser=True)
    context = {"object": "setting", "admins": admins}
    context |= get_context_content_base(request)
    return render(request, "admin_account.html", context=context)


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def adminEditPassword(request):
    if not request.POST:
        return redirect("/adm/account/setting")
    password = request.POST.get("admin_password")
    old_password = request.POST.get("admin_old_password")
    confirme_password = request.POST.get("admin_confirme_password")
    msg_edit_password = None
    msg_edit_old_password = None
    if not authenticate(username=request.user.username, password=old_password):
        msg_edit_old_password = "Mot de passe incorrect"
    elif password != confirme_password:
        msg_edit_password = "Mot de passe non confirmé"
    elif not check_valid_password(password):
        msg_edit_password = "mot de passe : 8 caractéres au mimimun,au moin une majuscule,une minuscule,un chiffre et un caratére spécial"

    else:
        request.user.set_password(password)
        request.user.save()
        logout(request)
        return redirect("/adm/account/login")
    context = {
        "object": "setting",
        "msg_edit_password": msg_edit_password,
        "msg_edit_old_password": msg_edit_old_password,
    }

    context |= get_context_content_base(request)
    return render(request, "admin_account.html", context=context)


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def adminEditProfil(request):
    context = get_context_content_base(request)
    if not request.POST:
        return redirect("/adm/account/setting")
    admin_edit = {
        "last_name": request.POST.get("last_name"),
        "first_name": request.POST.get("first_name"),
        "email": request.POST.get("email"),
        "phone": request.POST.get("phone"),
        "address": request.POST.get("address"),
        "country": request.POST.get("country"),
    }
    msg_email = check_exist_email(admin_edit.get("email"), request.user.email)
    msg_first_name = check_first_name(admin_edit.get("first_name"))
    msg_last_name = check_last_name(admin_edit.get("last_name"))
    msg_phone = check_phone(admin_edit.get("phone"))
    admin = context.get("admin_user")
    admin.last_name = admin_edit.get("last_name")
    admin.first_name = admin_edit.get("first_name")
    admin.email = admin_edit.get("email")
    admin.phone = admin_edit.get("phone")
    admin.address = admin_edit.get("address")
    admin.country = admin_edit.get("country")
    if not msg_email and not msg_first_name and not msg_last_name and not msg_phone:
        admin.save()
        logout(request)
        return redirect("/adm/account/login")
    context.update(
        {
            "object": "setting",
            "msg_email": msg_email,
            "msg_first_name": msg_first_name,
            "msg_last_name": msg_last_name,
            "msg_phone": msg_phone,
        }
    )
    context.update({"admin_user": admin})
    return render(request, "admin_account.html", context=context)


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def getParameters(request):
    all_parameters = ParameterCloud.objects.all()
    list_parameters = [
        {
            "id": elt.id,
            "name": elt.name,
            "value": elt.value,
            "description": elt.description,
            "created_date": elt.created_date,
        }
        for elt in all_parameters
    ]
    return JsonResponse({"parameters": list_parameters})


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def getParameter(request, id):
    try:
        if not is_ajax(request):
            return JsonResponse({})
        if parameter := ParameterCloud.objects.get(pk=id):
            dict_parameter = {
                "id": parameter.id,
                "name": parameter.name,
                "value": parameter.value,
                "description": parameter.description,
                "created_date": parameter.created_date,
            }
            return JsonResponse({"parameter": dict_parameter})
        return JsonResponse()
    except Exception:
        return JsonResponse({"msg": "error"})


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def deleteParameter(request, id):
    if not is_ajax(request):
        return JsonResponse({})
    try:
        ParameterCloud.objects.get(pk=id).delete()
        return JsonResponse({"msg": "success"})
    except Exception:
        return JsonResponse({"msg": "error"})


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def newParameter(request):
    if not request.POST or not is_ajax(request):
        return JsonResponse({})
    msg_name = None if request.POST.get("param_name") else "Le nom ne peut être vide"
    msg_value = (
        None if request.POST.get("param_value") else "Le valeur ne peut être vide"
    )
    if msg_name and msg_value:
        return JsonResponse({"msg_name": msg_name, "msg_value": msg_value})
    if request.POST.get("param_id"):
        if ParameterCloud.objects.exclude(id=request.POST.get("param_id")).filter(
            name=request.POST.get("param_name")
        ):
            return JsonResponse(
                {
                    "msg_name": "Le nom du paramètre doit être unique",
                    "msg_value": msg_value,
                }
            )
    elif ParameterCloud.objects.filter(name=request.POST.get("param_name")):
        return JsonResponse(
            {"msg_name": "Le nom du paramètre doit être unique", "msg_value": msg_value}
        )
    try:
        if request.POST.get("param_id"):
            parameter_cloud = ParameterCloud.objects.get(
                pk=request.POST.get("param_id")
            )
        else:
            parameter_cloud = ParameterCloud()
        parameter_cloud.name = request.POST.get("param_name")
        parameter_cloud.description = request.POST.get("param_desc")
        parameter_cloud.value = request.POST.get("param_value")
        parameter_cloud.save()
        return JsonResponse({"msg": "success"})
    except Exception:
        return JsonResponse({"msg": "error"})


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def enable_disable_admin(request, id, status):
    if not is_ajax(request):
        return JsonResponse({})
    try:
        if admin_user := UserCloud.objects.get(pk=id):
            if status and status == "enable":
                admin_user.enabled = True
            elif status and status == "disable":
                admin_user.enabled = False
            admin_user.save()
            return JsonResponse({"msg": "success"})
    except Exception:
        return JsonResponse({"msg": "error"})


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def editPassword(request, id):
    try:
        if not request.POST:
            return redirect("/adm/account/setting")
        password = request.POST.get("admin_password")
        confirme_password = request.POST.get("admin_confirme_password")
        if password != confirme_password:
            return JsonResponse({"msg": "Mot de passe non confirmé"})
        elif not check_valid_password(password):
            return JsonResponse(
                {
                    "msg": "mot de passe : 8 caractères au minimum,au moins une majuscule,une minuscule, un chiffre et un caractère spécial"
                }
            )
        else:
            if admin := UserCloud.objects.get(pk=id):
                admin.set_password(password)
                admin.save()
                return JsonResponse({"msg": "success"})
    except Exception as e:
        return JsonResponse({"msg": "Une erreur s'est produite"})


#
def is_ajax(request):
    return request.META.get("HTTP_X_REQUESTED_WITH") == "XMLHttpRequest"


#
def get_context_content_base(request):
    admin_user = UserCloud.objects.get(pk=request.user.id)
    list_country_dict = [
        {"code": country[0], "name": country[1]} for country in list(countries)
    ]
    return {"admin_user": admin_user, "list_countries": list_country_dict}


#
def check_valid_password(password):
    flag = 0
    while True:
        if len(password) < 8:
            flag = -1
        elif not re.search("[a-z]", password):
            flag = -1
        elif not re.search("[A-Z]", password):
            flag = -1
        elif not re.search("[0-9]", password):
            flag = -1
        elif not re.search("[_@$]", password):
            flag = -1
        elif re.search("\s", password):
            flag = -1
        else:
            flag = 0
        break
    return flag != -1


#
def check_exist_email(email, email_admin):
    if not email:
        return "Le champ email est obligatoire"
    if UserCloud._default_manager.exclude(email=email_admin).filter(email=email):
        return "Cet email est déjà utilisé"


#
def check_first_name(first_name):
    if not first_name:
        return "Le champ prénom est obligatoire"
    if len(first_name) < 3:
        return "Prénom invalide"


#
def check_last_name(last_name):
    if not last_name:
        return "Le champ nom est obligatoire"
    if len(last_name) < 3:
        return "Nom invalide"


#
def check_phone(phone):
    if not phone:
        return "Le champ téléphone est obligatoire"
    phone_number = phonenumbers.parse(phone, "SN")
    if not phonenumbers.is_valid_number(phone_number):
        return "Téléphone invalide"
