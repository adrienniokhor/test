from django.contrib.auth.decorators import user_passes_test
from django.http import HttpResponseRedirect
from django.shortcuts import render

from user_cloud.models import OffreSurMesure


@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def getadmOffresurmesure(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/user/login")
    if code := request.GET.get("code"):
        context = getadmOffresurmesureDetail(code=code)
        return render(
            request,
            "admin_account.html",
            context={
                "offresurmesure_detail": context,
                "object": "Offresurmesuredetail",
            },
        )
    Offresurmesure = OffreSurMesure.objects.all().order_by("-id")
    return render(
        request,
        "admin_account.html",
        context={"Offresurmesure": Offresurmesure, "object": "Offresurmesure"},
    )


# @user_passes_test(lambda u: u.is_superuser,login_url='/adm/account/login',redirect_field_name='next_page')
def getadmOffresurmesureDetail(code):
    if commande := OffreSurMesure.objects.filter(code=code):
        commande = commande[0]
        return commande
