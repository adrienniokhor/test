from django.contrib.auth.base_user import BaseUserManager
from django.db import models
from django.utils import timezone

# from django.utils.translation import ugettext_lazy as _


class CustomUserManager(BaseUserManager):
    """
    Create and save a User with the given email and password.
    """

    def create_user(self, email, password, **extra_fields):
        """
        Create and save a User with the given email and password.
        """
        if not email:
            raise ValueError(_("The Email must be set"))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")
        return self.create_user(email, password, **extra_fields)


#
class ParameterCloud(models.Model):
    name = models.CharField("Nom", max_length=40, unique=True, blank=False)
    value = models.CharField("Valeur", max_length=200, blank=False)
    description = models.TextField("Déscription")
    created_date = models.DateTimeField("Date création", default=timezone.now)
