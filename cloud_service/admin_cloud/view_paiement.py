import asyncio
from datetime import date, datetime

from django.contrib.auth.decorators import user_passes_test
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from kombu.exceptions import OperationalError
from redis.exceptions import ConnectionError

from admin_cloud.forms import AddPaiementForm
from admin_cloud.tasks import task_runApplication, task_runInstance
from analytics.models import Event
from cloud_service.push_notification import notify_customer
from cloud_service.settings import PAGINATION_DEFAULT_PAGINATION
from cloud_service.utils import notify_on_paiement_valid
from commande.models import PaiementCloud


import contextlib
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def getPaiement(request):
    if code := request.GET.get("code"):
        # pylint: disable=E1101
        if paiement := PaiementCloud.objects.get(code=code):
            return render(
                request,
                "admin_account.html",
                context={
                    "paiement_detail": paiement,
                    "object": "paiement",
                    "action": "list",
                },
            )
    # pylint: disable=E1101
    instances = PaiementCloud.objects.all().order_by("-id")
    pages = Paginator(instances, PAGINATION_DEFAULT_PAGINATION)
    if page_number := request.GET.get("page"):
        page_number = int(page_number)
        if page_number > pages.num_pages or page_number < 1:
            page_number = 1
        page = pages.page(page_number)
    else:
        page = pages.page(1)
    return render(
        request,
        "admin_account.html",
        context={"page": page, "object": "paiement", "action": "list"},
    )


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def addPaiement(request):
    if request.method == "POST":
        form_paiement = AddPaiementForm(request.POST)
        if not form_paiement.is_valid():
            return render(
                request,
                "admin_account.html",
                context={
                    "form_paiement": form_paiement,
                    "object": "paiement",
                    "action": "add",
                },
            )
        data = form_paiement.cleaned_data
        # pylint: disable=E1101
        paiement = PaiementCloud.objects.create(
            date_created=datetime.now(),
            date_paiement=data.get("date_paiement"),
            commande=data.get("commande"),
            mode_paiement=data.get("mode_paiement"),
            numTransaction_or_numCarte=data.get("numTransaction_or_numCarte"),
            state="open",
            devise=data.get("devise"),
            montant=data.get("montant"),
            proprietaire=data.get("commande").proprietaire,
        )
        paiement.save()
        create_paiement_event = Event(
            name="Create Paiement",
            object="paiement",
            code=paiement.code,
            created=datetime.now(),
        )
        create_paiement_event.save()
        return HttpResponseRedirect("/adm/account/list/paiement/")
    default_data = {"date_paiement": date.today()}
    form_paiement = AddPaiementForm(initial=default_data)
    return render(
        request,
        "admin_account.html",
        context={"form_paiement": form_paiement, "object": "paiement", "action": "add"},
    )


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def actionPaiement(request):  # sourcery skip: low-code-quality
    if request.method == "POST":
        #
        if "valider" in request.POST:
            select_items = request.POST.getlist("select_items")
            for item in select_items:
                # pylint: disable=E1101
                paiement = PaiementCloud.objects.get(id=int(item))
                if paiement.state == "validat":
                    continue
                paiement.state = "validat"
                paiement.date_valid = datetime.now()
                paiement.save()
                with contextlib.suppress(OperationalError, ConnectionError):
                    asyncio.set_event_loop(asyncio.new_event_loop())
                    asyncio.run(
                        notify_customer(
                            paiement.proprietaire.id,
                            "Votre paiement est bien validé <br> Les ressouces demandées seront bientôt disponibles <br> Merci de patienter !!!!",  # noqa: E501
                        )
                    )
                    commande = paiement.commande
                    if validerCommande(commande):
                        task_runInstance.delay(paiement.id, commande.id)
                        task_runApplication.delay(commande.id)
                    notify_on_paiement_valid.delay(paiement.id)
        elif "annuler" in request.POST:
            select_items = request.POST.getlist("select_items")
            for item in select_items:
                # pylint: disable=E1101
                paiement = PaiementCloud.objects.get(id=int(item))
                paiement.state = "cancel"
                paiement.date_cancel = datetime.now()
                paiement.save()
        elif "supprimer" in request.POST:
            select_items = request.POST.getlist("select_items")
            for item in select_items:
                # pylint: disable=E1101
                paiement = PaiementCloud.objects.get(id=int(item))
                paiement.delete()
        elif "rejeter" in request.POST:
            select_items = request.POST.getlist("select_items")
            for item in select_items:
                # pylint: disable=E1101
                paiement = PaiementCloud.objects.get(id=int(item))
                paiement.state = "reject"
                paiement.date_denial = datetime.now()
                paiement.save()
        elif "rembourser" in request.POST:
            select_items = request.POST.getlist("select_items")
            for item in select_items:
                # pylint: disable=E1101
                paiement = PaiementCloud.objects.get(id=int(item))
                paiement.state = "refund"
                paiement.save()
        elif "rembourser_partiel" in request.POST:
            select_items = request.POST.getlist("select_items")
            for item in select_items:
                # pylint: disable=E1101
                paiement = PaiementCloud.objects.get(id=int(item))
                paiement.state = "refund_partiel"
                paiement.save()
            #
    return redirect("/adm/account/list/paiement/", error="error")


##
def validerCommande(commande):
    try:
        commande.state = "paid"
        commande.save()
        return True
    except Exception:
        return False


##
def task_runInstance_2(paiement_id, commande_id):
    # pylint: disable=C0415
    from admin_cloud.instance_function import (
        createRemoteInstance,
        executeAction,
    )
    from commande.models import CommandeCloud
    from services.models import ServiceCloud
    from time import sleep

    #
    # pylint: disable=E1101
    commande = CommandeCloud.objects.get(id=commande_id)
    commande_lines = commande.commmande_ligne_commande.all()
    for commande_line in commande_lines:
        instance_has_erros = False
        # pylint: disable=E1101
        service = ServiceCloud.objects.get(id=commande_line.service.id)
        owner = commande.proprietaire
        for _ in range(commande_line.quantity):
            sleep(10)
            instance_return = createRemoteInstance(
                service, owner, commande.code, commande_line.durree
            )
            if not instance_return:
                continue
            for action in service.action.all():
                return_action_value = executeAction(instance_return, action)
                if not return_action_value:
                    instance_has_erros = True
                    break
            instance_return.state = "error" if instance_has_erros else "open"
            instance_return.save()
    return True
