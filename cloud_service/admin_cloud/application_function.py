from datetime import datetime

from dateutil.relativedelta import relativedelta

from analytics.models import Event
from api_service.boto3 import boto_operation_ec2
from api_service.jenkins.job import call_jenkins_job
from application.models import ApplicationCloud
from commande.models import CommandeCloud
from services.models import InstanceCloud

#
#
def runApplication(commande: CommandeCloud):
    commande_app_lines = commande.commmande_ligne_app_commande.all()
    for commande_app_line in commande_app_lines:
        owner = commande.proprietaire
        for _ in range(commande_app_line.quantity):
            application_return = createRemoteApplication(
                commande_app_line.app_name,
                commande_app_line.package,
                owner, commande.code,
                commande_app_line.durree
            )
            if not application_return:
                continue
            application_return.state = "open"
            application_return.save()
    return True


#
def createRemoteApplication(app_name, app_package, owner, code_commande, durree=1):
    kwargs  = {"image" : "ami-0fd3c09329d8abdb8"}
    build_number = call_jenkins_job("aws-ansible", **kwargs)
    application = {
        "app_name": app_name,
        "app_package": app_package,
        "owner": owner,
        "date_created": datetime.now(),
        "state": "open",
        "build_number": build_number,
        "build_date": datetime.now(),
        "code_commande": code_commande,
    }
    application["date_revoked"] = datetime.now() + relativedelta(months=durree)
    # pylint: disable=E1101
    application_return = ApplicationCloud.objects.create(**application)
    application_return.save()
    create_application_event = Event(
        name="Create Application",
        object="application",
        code=application_return.code,
        created=datetime.now(),
    )
    create_application_event.save()
    return application_return

#
# Stop Application Function
def stopApplication(application: ApplicationCloud):
    info_return = False
    if application.remote_id:
        result = boto_operation_ec2.stop_instance(application.remote_id)
        if result and result.get("StoppingInstances"):
            current_state = result.get("StoppingInstances")[0].get("CurrentState") or {
                "Code": -1,
                "Name": "error",
            }
            if (
                current_state.get("Code") is not None
                and current_state.get("Name")
                and (
                    (
                        current_state.get("Code") == 64
                        and current_state.get("Name") == "stopping"
                    )
                    or (
                        current_state.get("Code") == 80
                        and current_state.get("Name") == "stopped"
                    )
                )
            ):
                application.state = "stopped"
                info_return = True
            else:
                application.state = "error"
                info_return = False
        #
        application.ip_address = ""
        application.save()
    return info_return


#
# Start Application Function
def startApplication(application: ApplicationCloud):
    info_return = False
    if application.remote_id:
        result = boto_operation_ec2.start_instance(application.remote_id)
        if result and result.get("StartingInstances"):
            current_state = result.get("StartingInstances")[0].get("CurrentState") or {
                "Code": -1,
                "Name": "error",
            }
            if (
                current_state.get("Code") is not None
                and current_state.get("Name")
                and (
                    (
                        current_state.get("Code") == 0
                        and current_state.get("Name") == "pending"
                    )
                    or (
                        current_state.get("Code") == 16
                        and current_state.get("Name") == "running"
                    )
                )
            ):
                application.state = "pending"
                info_return = True

            else:
                application.state = "error"
                info_return = False

        application.save()
    return info_return


#
# Terminated Instance Function
def terminateApplication(application: ApplicationCloud):
    info_return = False
    if application.remote_id:
        result = boto_operation_ec2.terminate_instance(application.remote_id)
        if result and result.get("TerminatingInstances"):
            current_state = result.get("TerminatingInstances")[0].get(
                "CurrentState"
            ) or {"Code": -1, "Name": "error"}
            if (
                current_state.get("Code") is not None
                and current_state.get("Name")
                and current_state.get("Code") == 48
                and current_state.get("Name") == "terminated"
            ):
                info_return = True
    return info_return


#
# Cheick if Application Exist Function
def check_application_exists(application: ApplicationCloud):
    if application.remote_id:
        result = boto_operation_ec2.describe_instance_with_id(application.remote_id)
        if result and result[0].get("Instances"):
            remote_instance = result[0].get("Instances")[0]
            if remote_instance.get("InstanceId") == application.remote_id:
                return True
    return False


#
# Revoked Application Function
def revokedApplication(application: ApplicationCloud, check_instance: bool):
    if check_instance:
        if result := stopApplication(application):
            application.state = "revoked"
            application.save()
        return result
    else:
        application.state = "revoked"
        application.ip_address = ""
        application.save()
        return True


#
# Compare local instance and remote instance
def compareInstance(list_instance_id=None):  # sourcery skip: low-code-quality
    list_remote_remote_id = []
    list_instance_id = []
    if select_items := list_instance_id:
        # pylint: disable=E1101
        list_local_instances = InstanceCloud.objects.filter(id__in=list(select_items))
        list_local_remote_id = [
            local_instance.remote_id for local_instance in list_local_instances
        ]

        list_local_id = [local_instance.id for local_instance in list_local_instances]
        result = boto_operation_ec2.describe_list_instances_with_list_id(
            list_local_remote_id
        )

        instances_details_reservations = result.get("Reservations")
        for instances_details in instances_details_reservations:
            for instance in instances_details.get("Instances"):
                if instance.get("InstanceId"):
                    list_remote_remote_id.append(instance.get("InstanceId"))
                    if instance_local := next(
                        (
                            elt
                            for elt in list_local_instances
                            if elt.remote_id == instance.get("InstanceId")
                        ),
                        None,
                    ):
                        list_instance_id.append(instance_local.id)
                        remote_state = instance.get("State")
                        if (
                            remote_state
                            and remote_state.get("Code")
                            and remote_state.get("Name")
                        ):
                            if (
                                remote_state.get("Code") == 0
                                and remote_state.get("Name") == "pending"
                            ):
                                if (
                                    instance_local.state
                                    and instance_local.state == "pending"
                                ):
                                    continue
                                instance_local.state = "pending"
                                instance_local.save()
                            elif (
                                remote_state.get("Code") == 16
                                and remote_state.get("Name") == "running"
                            ):
                                if (
                                    instance_local.state
                                    and instance_local.state == "started"
                                ):
                                    continue
                                instance_local.state = "started"
                                instance_local.save()
                            elif remote_state.get("Code") == 16 and remote_state.get(
                                "Name"
                            ) in ["stopping", "stopped"]:
                                if (
                                    not instance_local.state
                                    or instance_local.state
                                    not in ["stopped", "revoked"]
                                ):
                                    instance_local.state = "stopped"
                                    instance_local.save()
        if diff_instance_with_select := list(
            set(list_local_id) - set(list_instance_id)
        ):
            list_instance_without_corres = [
                next(elt for elt in list_local_instances if elt.id == local_id)
                for local_id in diff_instance_with_select
            ]

            list_id_instance_without_corres = []
            for instance_without_corres in list_instance_without_corres:
                if instance_without_corres.state and instance_without_corres.state in [
                    "open",
                    "expire",
                ]:
                    continue
                list_id_instance_without_corres.append(instance_without_corres.code)
                instance_without_corres.state = "expire"
                instance_without_corres.save()
    else:
        result = boto_operation_ec2.describe_all_instances()
        instances_details_reservations = result.get("Reservations")
        for instances_details in instances_details_reservations:
            for instance in instances_details.get("Instances"):
                if instance.get("InstanceId"):
                    list_remote_remote_id.append(instance.get("InstanceId"))
                    # pylint: disable=E1101
                    if instance_local := InstanceCloud.objects.filter(
                        remote_id=instance.get("InstanceId")
                    ):
                        instance_local = instance_local[0]
                        list_instance_id.append(instance_local.id)
                        remote_state = instance.get("State")
                        if (
                            remote_state
                            and remote_state.get("Code")
                            and remote_state.get("Name")
                        ):
                            if (
                                remote_state.get("Code") == 0
                                and remote_state.get("Name") == "pending"
                            ):
                                if (
                                    instance_local.state
                                    and instance_local.state == "pending"
                                ):
                                    continue
                                instance_local.state = "pending"
                                instance_local.save()
                            elif (
                                remote_state.get("Code") == 16
                                and remote_state.get("Name") == "running"
                            ):
                                if (
                                    instance_local.state
                                    and instance_local.state == "started"
                                ):
                                    continue
                                instance_local.state = "started"
                                instance_local.save()
                            elif remote_state.get("Code") == 16 and remote_state.get(
                                "Name"
                            ) in ["stopping", "stopped"]:
                                if (
                                    not instance_local.state
                                    or instance_local.state
                                    not in ["stopped", "revoked"]
                                ):
                                    instance_local.state = "stopped"
                                    instance_local.save()
        if list_instance_id:
            # pylint: disable=E1101
            missing_remote_instances = InstanceCloud.objects.exclude(
                id__in=list_instance_id
            )

        else:
            # pylint: disable=E1101
            missing_remote_instances = InstanceCloud.objects.all()
        for missing_remote_instance in missing_remote_instances:
            if missing_remote_instance.state and missing_remote_instance.state in [
                "open",
                "expire",
            ]:
                continue
            missing_remote_instance.state = "expire"
            missing_remote_instance.save()
        # pylint: disable=E1101
        list_local_instances = InstanceCloud.objects.all()
        list_local_remote_id = [
            local_instance.remote_id for local_instance in list_local_instances
        ]
    #


#
# Revoke Application
def checkExpireApplication(application: ApplicationCloud):
    info_return = False
    if application.remote_id:
        result = boto_operation_ec2.stop_instance(application.remote_id)
        if result and result.get("StoppingInstances"):
            current_state = result.get("StoppingInstances")[0].get("CurrentState") or {
                "Code": -1,
                "Name": "error",
            }
            if (
                current_state.get("Code") is not None
                and current_state.get("Name")
                and (
                    (
                        current_state.get("Code") == 64
                        and current_state.get("Name") == "stopping"
                    )
                    or (
                        current_state.get("Code") == 80
                        and current_state.get("Name") == "stopped"
                    )
                )
            ):
                application.state = "expire"
                info_return = True
            else:
                application.state = "error"
                info_return = False
        #
        application.ip_address = ""
        application.save()
    return info_return
