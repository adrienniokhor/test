from django import forms
from django.contrib.auth.decorators import user_passes_test
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render

from admin_cloud.forms import AddCommandeForm
from api_service.jenkins.instance_hosting import launchInstance
from cloud_service.settings import PAGINATION_DEFAULT_PAGINATION
from commande.models import CommandeCloud


@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def addCommande(request):
    if request.method == "POST":
        form_commande = AddCommandeForm(request.POST)
        if not form_commande.is_valid():
            return render(
                request,
                "admin_account.html",
                context={
                    "form_commande": form_commande,
                    "object": "commande",
                    "action": "add",
                },
            )
        data = form_commande.cleaned_data
        form_commande = AddCommandeForm.objects.create(
            name=data["name"],
            description=data["description"],
            executor=data["executor"],
        )
        form_commande.save()
        return redirect("admin_account.html")
    form_commande = AddCommandeForm()
    return render(
        request,
        "admin_account.html",
        context={"form_commande": form_commande, "object": "commande", "action": "add"},
    )


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def getCommande(request):
    if code := request.GET.get("code"):
        context = getCommandeDetail(request=request, code=code)
        return render(request, "admin_account.html", context=context)
    commandes = CommandeCloud.objects.all().order_by("-id")
    pages = Paginator(commandes, PAGINATION_DEFAULT_PAGINATION)
    if page_number := request.GET.get("page"):
        page_number = int(page_number)
        if page_number > pages.num_pages or page_number < 1:
            page_number = 1
        page = pages.page(page_number)
    else:
        page = pages.page(1)
    return render(
        request,
        "admin_account.html",
        context={"page": page, "object": "commande", "action": "list"},
    )


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def getCommandeDetail(request, code):
    if commande := CommandeCloud.objects.filter(code=code):
        commande = commande[0]
        prev_commande = (
            CommandeCloud.objects.filter(id__lt=commande.id)
            .exclude(id=commande.id)
            .order_by("-id")
            .first()
        )
        #
        next_commande = (
            CommandeCloud.objects.filter(id__gt=commande.id)
            .exclude(id=commande.id)
            .order_by("id")
            .first()
        )
        #
        return {
            "admin_commande": commande,
            "object": "commande",
            "action": "detail",
            "next_commande": next_commande,
            "prev_commande": prev_commande,
        }
    else:
        commandes = CommandeCloud.objects.all().order_by("-id")
        pages = Paginator(commandes, PAGINATION_DEFAULT_PAGINATION)
        page = pages.page(1)
        return {"page": page, "object": "commande", "action": "list"}


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def actionCommande(request):

    # jenkinsService()
    if "valider" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            commande = CommandeCloud.objects.get(id=int(item))
            if commande.state != "open":
                commande.state = "open"
                commande.save()
    #
    elif "annuler" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            commande = CommandeCloud.objects.get(id=int(item))
            if commande.state != "cancel":
                commande.state = "cancel"
                commande.save()
    #
    elif "supprimer" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            commande = CommandeCloud.objects.get(id=int(item))
            commande.delete()
    #
    return HttpResponseRedirect("/adm/account/list/commande/")


#
def jenkinsService():
    info = launchInstance("job_cloud")
    print("************************************************")
    print(info)
    print("*************************************************")


#
