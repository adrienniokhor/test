from bootstrap_modal_forms.generic import BSModalUpdateView
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator

from admin_cloud.action_edit.form.user import UserModelForm
from user_cloud.models import UserCloud


# @user_passes_test(lambda u: u.is_superuser,login_url='/adm/account/login',redirect_field_name='next_page')
@method_decorator(login_required, name="dispatch")
# @method_decorator(user_passes_test,name='dispatch')
class updateUser(BSModalUpdateView):
    #
    model = UserCloud
    template_name = "edit/user.html"
    form_class = UserModelForm
    success_message = "L'utilisateur est bien modifié."
    # success_url = reverse_lazy('admin_user')

    #
    # def form_valid(self, form):
    #     print('################################################')
    #     print(form.is_valid)
    #     print('################################################')
    #     print(form.cleaned_data)
    #     print('################################################')
    #     return super().form_valid(form)

    #
    @method_decorator(login_required(login_url="/adm/account/login"))
    @method_decorator(
        user_passes_test(
            lambda u: u.is_superuser,
            login_url="/adm/account/login",
            redirect_field_name="next_page",
        )
    )
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
