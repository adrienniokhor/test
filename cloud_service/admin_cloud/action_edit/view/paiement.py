from bootstrap_modal_forms.generic import BSModalUpdateView
from django.contrib.auth.decorators import login_required, user_passes_test
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator

from admin_cloud.action_edit.form.paiement import PaiementModelForm
from commande.models import PaiementCloud


class updatePaiement(BSModalUpdateView):
    model = PaiementCloud
    template_name = "edit/paiement.html"
    form_class = PaiementModelForm
    success_message = "Le paiement est bien modifié."
    # success_url = reverse_lazy('admin_paiement')

    #
    @method_decorator(login_required(login_url="/adm/account/login"))
    @method_decorator(
        user_passes_test(
            lambda u: u.is_superuser,
            login_url="/adm/account/login",
            redirect_field_name="next_page",
        )
    )
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
