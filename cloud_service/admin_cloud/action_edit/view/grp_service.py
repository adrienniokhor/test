from bootstrap_modal_forms.generic import BSModalUpdateView
from django.contrib.auth.decorators import login_required, user_passes_test
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator

from admin_cloud.action_edit.form.grp_service import GrpServiceModelForm
from groupe_service.models import GroupServiceCloud


class updateGroupeService(BSModalUpdateView):
    model = GroupServiceCloud
    template_name = "edit/grp_service.html"
    form_class = GrpServiceModelForm
    success_message = "Le groupe de service est bien modifié."
    # success_url = reverse_lazy('admin_groupe_service')

    #
    @method_decorator(login_required(login_url="/adm/account/login"))
    @method_decorator(
        user_passes_test(
            lambda u: u.is_superuser,
            login_url="/adm/account/login",
            redirect_field_name="next_page",
        )
    )
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
