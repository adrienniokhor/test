from bootstrap_modal_forms.generic import BSModalUpdateView
from django.contrib.auth.decorators import login_required, user_passes_test
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator

from admin_cloud.action_edit.form.forfait import ForfaitModelForm
from services.models import ForfaitCloud


class updateForfait(BSModalUpdateView):
    model = ForfaitCloud
    template_name = "edit/forfait.html"
    form_class = ForfaitModelForm
    success_message = "Le forfait est bien modifiée."
    # success_url = reverse_lazy('admin_action')

    #
    @method_decorator(login_required(login_url="/adm/account/login"))
    @method_decorator(
        user_passes_test(
            lambda u: u.is_superuser,
            login_url="/adm/account/login",
            redirect_field_name="next_page",
        )
    )
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
