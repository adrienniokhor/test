from bootstrap_modal_forms.forms import BSModalModelForm
from django import forms

from services.models import InstanceCloud, ServiceCloud
from user_cloud.models import UserCloud


class InstanceModelForm(BSModalModelForm):
    class Meta:
        model = InstanceCloud
        fields = [
            "date_revoked",
            "state",
            "service",
            "owner",
            "remote_id",
            "build_number",
            "build_date",
        ]
        style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
        devis = [
            ("", "--------"),
            ("fcfa", "FCFA"),
            ("euros", "EUROS"),
            ("dollar", "Dollar"),
        ]
        widgets = {
            "date_created": forms.DateTimeInput(attrs={"class": style_class}),
            "date_revoked": forms.DateTimeInput(attrs={"class": style_class}),
            "state": forms.Select(attrs={"class": style_class}),
            "code": forms.TextInput(attrs={"class": style_class}),
            "service": forms.Select(attrs={"class": style_class}),
            "owner": forms.Select(attrs={"class": style_class}),
            "remote_id": forms.TextInput(attrs={"class": style_class}),
            "build_number": forms.NumberInput(attrs={"class": style_class}),
            "build_date": forms.DateTimeInput(attrs={"class": style_class}),
        }

    #
    def __init__(self, *args, **kwargs):
        super(InstanceModelForm, self).__init__(*args, **kwargs)
        #
        if kwargs.get("instance") and kwargs.get("instance").owner:
            self.fields["owner"].queryset = UserCloud.objects.filter(
                enabled=True
            ) | UserCloud.objects.filter(id=kwargs.get("instance").owner.id)
        else:
            self.fields["owner"].queryset = UserCloud.objects.filter(enabled=True)
        #
        if kwargs.get("instance") and kwargs.get("instance").service:
            self.fields["service"].queryset = ServiceCloud.objects.filter(
                enabled=True
            ) | ServiceCloud.objects.filter(id=kwargs.get("instance").service.id)
        else:
            self.fields["service"].queryset = ServiceCloud.objects.filter(enabled=True)
