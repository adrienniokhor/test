# from attr import fields
from bootstrap_modal_forms.forms import BSModalModelForm
from django import forms
from django_countries.data import COUNTRIES

from user_cloud.models import UserCloud


class UserModelForm(BSModalModelForm):
    class Meta:
        model = UserCloud

        fields = ["first_name", "last_name", "phone", "address", "country", "email"]
        style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
        widgets = {
            "first_name": forms.TextInput(attrs={"class": style_class}),
            "last_name": forms.TextInput(attrs={"class": style_class}),
            "email": forms.EmailInput(attrs={"class": style_class}),
            "phone": forms.TextInput(attrs={"class": style_class}),
            "address": forms.TextInput(attrs={"class": style_class}),
            "country": forms.Select(attrs={"class": style_class}),
        }

    #
    def __init__(self, *args, **kwargs):
        country_list = sorted(COUNTRIES.items())
        country_list.insert(0, (None, "(select country)"))
        super(UserModelForm, self).__init__(*args, **kwargs)

    #     #
    # if kwargs.get('instance') and kwargs.get('instance').country:
    #     self.fields['country'].queryset = country_list
    # else:
    #     self.fields['country'].queryset = country_list
    #     #
    #     if kwargs.get('instance') and kwargs.get('instance').service:
    #         self.fields['service'].queryset = ServiceCloud.objects.filter(enabled=True) | ServiceCloud.objects.filter(id=kwargs.get('instance').service.id)
    #     else:
    #         self.fields['service'].queryset = ServiceCloud.objects.filter(enabled=True)

    def clean_email(self):
        email = self.data["email"]
        if UserCloud._default_manager.exclude(email=self.instance.email).filter(
            email=email
        ):
            raise forms.ValidationError("Cet email est déjà utilisé.")
        return email

    #
    def clean(self, *args, **kwargs):
        self.clean_email()
        return super(UserModelForm, self).clean(*args, **kwargs)


# #
