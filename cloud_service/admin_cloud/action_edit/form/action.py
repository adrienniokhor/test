from bootstrap_modal_forms.forms import BSModalModelForm
from django import forms

from services.models import ActionCloud


class ActionModelForm(BSModalModelForm):
    class Meta:
        model = ActionCloud
        fields = ["name", "description", "executor", "dynamic"]
        #
        style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
        style_class_check_box = (
            "item-center text-center mx-4 my-auto flex w-4 h-4 text-blue-600"
        )
        widgets = {
            "name": forms.TextInput(attrs={"class": style_class}),
            "description": forms.Textarea(attrs={"rows": 4, "class": style_class}),
            "executor": forms.TextInput(attrs={"class": style_class}),
            "dynamic": forms.RadioSelect(
                choices=[(True, "OUI"), (False, "NON")],
                attrs={"class": style_class_check_box},
            ),
        }
