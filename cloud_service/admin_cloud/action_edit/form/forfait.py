from bootstrap_modal_forms.forms import BSModalModelForm
from django import forms

from services.models import ForfaitCloud


class ForfaitModelForm(BSModalModelForm):
    class Meta:
        model = ForfaitCloud
        fields = ["durree", "reduction"]
        #
        style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
        widgets = {
            "durree": forms.NumberInput(
                attrs={
                    "class": style_class,
                    "min": "1",
                }
            ),
            "reduction": forms.NumberInput(
                attrs={
                    "class": style_class,
                    "min": "0",
                }
            ),
        }
