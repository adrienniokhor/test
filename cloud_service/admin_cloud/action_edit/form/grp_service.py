from bootstrap_modal_forms.forms import BSModalModelForm
from django import forms

from groupe_service.models import GroupServiceCloud


class GrpServiceModelForm(BSModalModelForm):
    class Meta:
        model = GroupServiceCloud
        fields = ["name", "description", "image", "info_sup"]
        #
        style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
        style_class_check_box = (
            "item-center text-center mx-4 my-auto flex w-4 h-4 text-blue-600"
        )
        widgets = {
            "name": forms.TextInput(attrs={"class": style_class}),
            "description": forms.Textarea(attrs={"rows": 4, "class": style_class}),
            "image": forms.FileInput(attrs={"class": style_class}),
            "info_sup": forms.Textarea(attrs={"rows": 4, "class": style_class}),
        }
