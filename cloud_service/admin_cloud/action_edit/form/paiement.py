from bootstrap_modal_forms.forms import BSModalModelForm
from django import forms

from commande.models import CommandeCloud, ModePaiementCloud, PaiementCloud
from user_cloud.models import UserCloud


class PaiementModelForm(BSModalModelForm):
    class Meta:
        model = PaiementCloud
        fields = [
            "date_paiement",
            "commande",
            "mode_paiement",
            "numTransaction_or_numCarte",
            "state",
            "proprietaire",
            "montant",
            "devise",
        ]
        style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
        widgets = {
            "date_paiement": forms.DateInput(attrs={"class": style_class}),
            "commande": forms.Select(attrs={"class": style_class}),
            "mode_paiement": forms.Select(attrs={"class": style_class}),
            "numTransaction_or_numCarte": forms.TextInput(attrs={"class": style_class}),
            "state": forms.Select(attrs={"class": style_class}),
            "proprietaire": forms.Select(attrs={"class": style_class}),
            "montant": forms.NumberInput(attrs={"class": style_class}),
            "devise": forms.Select(attrs={"class": style_class}),
        }

    #
    def __init__(self, *args, **kwargs):
        super(PaiementModelForm, self).__init__(*args, **kwargs)
        #
        if kwargs.get("instance") and kwargs.get("instance").commande:
            self.fields["commande"].queryset = CommandeCloud.objects.filter(
                state__in=["open", "validat"]
            ) | CommandeCloud.objects.filter(id=kwargs.get("instance").commande.id)
        else:
            self.fields["commande"].queryset = CommandeCloud.objects.filter(
                state__in=["open", "validat"]
            )
        #
        if kwargs.get("instance") and kwargs.get("instance").mode_paiement:
            self.fields["mode_paiement"].queryset = ModePaiementCloud.objects.filter(
                enabled=True
            ) | ModePaiementCloud.objects.filter(
                id=kwargs.get("instance").mode_paiement.id
            )
        else:
            self.fields["mode_paiement"].queryset = ModePaiementCloud.objects.filter(
                enabled=True
            )
        #
        if kwargs.get("instance") and kwargs.get("instance").proprietaire:
            self.fields["proprietaire"].queryset = UserCloud.objects.filter(
                enabled=True
            ) | UserCloud.objects.filter(id=kwargs.get("instance").proprietaire.id)
        else:
            self.fields["proprietaire"].queryset = UserCloud.objects.filter(
                enabled=True
            )
