from bootstrap_modal_forms.forms import BSModalModelForm
from django import forms

from groupe_service.models import GroupServiceCloud
from services.models import ServiceCloud


class ServiceModelForm(BSModalModelForm):
    class Meta:
        model = ServiceCloud
        fields = [
            "name",
            "price",
            "currency",
            "description",
            "groupe_service",
            "action",
            "image",
            "executor",
        ]
        #
        style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
        style_class_check_box = (
            "mx-4 item-center text-center  my-auto w-4 h-4 flex text-blue-600"
        )
        devis = [
            ("", "--------"),
            ("fcfa", "FCFA"),
            ("euros", "EUROS"),
            ("dollar", "Dollar"),
        ]
        #
        widgets = {
            "name": forms.TextInput(attrs={"class": style_class}),
            "image": forms.FileInput(attrs={"class": style_class}),
            "description": forms.Textarea(attrs={"class": style_class}),
            "price": forms.NumberInput(attrs={"class": style_class}),
            "currency": forms.Select(attrs={"class": style_class}),
            "action": forms.CheckboxSelectMultiple(
                attrs={"class": style_class_check_box}
            ),
            "groupe_service": forms.Select(attrs={"class": style_class}),
            "executor": forms.TextInput(attrs={"class": style_class}),
        }

    #
    def __init__(self, *args, **kwargs):
        super(ServiceModelForm, self).__init__(*args, **kwargs)
        #
        if kwargs.get("instance") and kwargs.get("instance").groupe_service:
            self.fields["groupe_service"].queryset = GroupServiceCloud.objects.filter(
                enabled=True
            ) | GroupServiceCloud.objects.filter(
                id=kwargs.get("instance").groupe_service.id
            )
        else:
            self.fields["groupe_service"].queryset = GroupServiceCloud.objects.filter(
                enabled=True
            )
