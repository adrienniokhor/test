import functools
from datetime import date, datetime, timedelta

from django.contrib.auth.decorators import user_passes_test
from django.db.models import Count, Q, Sum
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render

from admin_cloud.view_setting import is_ajax
from analytics.models import Event
from commande.models import CommandeCloud, PaiementCloud
from services.models import InstanceCloud
from user_cloud.models import UserCloud


@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def adminAccount(request):
    today = date.today()
    yesterday = today - timedelta(days=1)
    one_week_ago = today - timedelta(days=7)
    thirty_days_ago = today - timedelta(days=30)
    #

    #
    count_client = UserCloud.objects.filter(is_superuser=False).count()
    count_instance = InstanceCloud.objects.count()
    commande_count = CommandeCloud.objects.count()
    paiement_count = PaiementCloud.objects.count()
    #
    login_event = Event.objects.filter(
        name="Logged In", object="user", created__gte=today
    ).count()
    logout_event = Event.objects.filter(
        name="Logged Out", object="user", created__gte=today
    ).count()
    #
    create_instance_event_today = Event.objects.filter(
        name="Create Instance", object="instance", created__gte=today
    ).count()
    create_instance_event_yesterday = Event.objects.filter(
        name="Create Instance",
        object="instance",
        created__gte=yesterday,
        created__lt=today,
    ).count()
    #
    create_user_event_today = Event.objects.filter(
        name="Registered", object="user", created__gte=today
    ).count()
    create_user_event_yesterday = Event.objects.filter(
        name="Registered", object="user", created__gte=yesterday, created__lt=today
    ).count()
    #
    create_commande_event_today = Event.objects.filter(
        name="Create Commande", object="commande", created__gte=today
    ).count()
    create_commande_event_yesterday = Event.objects.filter(
        name="Create Commande",
        object="commande",
        created__gte=yesterday,
        created__lt=today,
    ).count()
    #
    create_paiement_event_today = Event.objects.filter(
        name="Create Paiement", object="paiement", created__gte=today
    ).count()
    create_paiement_event_yesterday = Event.objects.filter(
        name="Create Paiement",
        object="paiement",
        created__gte=yesterday,
        created__lt=today,
    ).count()
    #
    create_instance_pourcent = percent(
        create_instance_event_today, create_instance_event_yesterday
    )
    create_user_pourcent = percent(create_user_event_today, create_user_event_yesterday)
    create_commande_pourcent = percent(
        create_commande_event_today, create_commande_event_yesterday
    )
    create_paiement_pourcent = percent(
        create_paiement_event_today, create_paiement_event_yesterday
    )
    #
    data = {"today": today, "thirty_days_ago": thirty_days_ago}
    #
    client = {
        "count": count_client,
        "login": login_event,
        "logout": logout_event,
        "create_user_pourcent": create_user_pourcent,
    }
    instance = {
        "count": count_instance,
        "create_instance_event_today": create_instance_event_today,
        "create_instance_event_yesterday": create_instance_event_yesterday,
        "create_instance_pourcent": create_instance_pourcent,
    }
    commande = {
        "count": commande_count,
        "create_commande_event_today": create_commande_event_today,
        "create_commande_event_yesterday": create_commande_event_yesterday,
        "create_commande_pourcent": create_commande_pourcent,
    }
    paiement = {
        "count": paiement_count,
        "create_paiement_event_today": create_paiement_event_today,
        "create_paiement_event_yesterday": create_paiement_event_yesterday,
        "create_paiement_pourcent": create_paiement_pourcent,
    }
    #
    return render(
        request,
        "admin_account.html",
        context={
            "client": client,
            "instance": instance,
            "commande": commande,
            "paiement": paiement,
            "data": data,
        },
    )

    #


#
def percent(a: int, b: int):
    pourcentage = ((a - b) / b) * 100 if b != 0 else a * 100
    #
    return float("{:.2f}".format(pourcentage))


#
def get_analytic_values_graph_slicing(request):
    if not request.POST or not is_ajax(request):
        group_by = "day"
        date_end = datetime.now()
        date_start = date_end - timedelta(days=30)
    else:
        group_by = request.POST.get("group_by")
        date_start = request.POST.get("date_start")
        date_end = request.POST.get("date_end")
    #
    if not date_start or not date_end:
        group_by = "day"
        date_end = datetime.now()
        date_start = date_end - timedelta(days=30)
    #
    def compare_date_group_by(date_group_by_a, date_group_by_b):
        if group_by == "day":
            return 1 if date_group_by_a > date_group_by_b else -1
        elif group_by == "week":
            date_group_by_a = (
                date_group_by_a.replace("sem", "").replace("(", "-").replace(")", "")
            )
            date_group_by_b = (
                date_group_by_b.replace("sem", "").replace("(", "-").replace(")", "")
            )
            week_month_year_a = date_group_by_a.split("-")
            week_month_year_b = date_group_by_b.split("-")
            #
            if int(week_month_year_a[2]) == int(week_month_year_b[2]):
                if int(week_month_year_a[1]) == int(week_month_year_b[1]):
                    return (
                        1
                        if int(week_month_year_a[0]) > int(week_month_year_b[0])
                        else -1
                    )
                elif int(week_month_year_a[1]) > int(week_month_year_b[1]):
                    return 1
                else:
                    return -1
            elif int(week_month_year_a[2]) > int(week_month_year_b[2]):
                return 1
            else:
                return -1
        elif group_by == "month":
            month_year_a = date_group_by_a.split("-")
            month_year_b = date_group_by_b.split("-")
            #
            if int(month_year_a[1]) == int(month_year_b[1]):
                return 1 if int(month_year_a[0]) > int(month_year_b[0]) else -1
            elif int(month_year_a[1]) > int(month_year_b[1]):
                return 1
            else:
                return -1
        elif group_by == "year":
            return 1 if int(date_group_by_a) > int(date_group_by_b) else -1

    #
    events = (
        Event.objects.filter(created__gte=date_start, created__lte=date_end)
        .extra({"date_event": "date(created)"})
        .values("name", "object", "date_event")
        .annotate(count=Count("id"))
        .order_by("date_event")
    )
    categories = sorted({item.get("date_event") for item in events})

    if group_by == "day":
        events = (
            Event.objects.filter(created__gte=date_start, created__lte=date_end)
            .values("name", "object", "created__date")
            .annotate(count=Count("id"))
            .order_by("created__date")
        )
        events_dict = [
            {
                "date_event": f"{elt.get('created__date')}",
                "count": elt.get("count"),
                "name": elt.get("name"),
            }
            for elt in events
        ]
    elif group_by == "week":
        events = (
            Event.objects.filter(created__gte=date_start, created__lte=date_end)
            .values(
                "name", "object", "created__week", "created__month", "created__year"
            )
            .annotate(count=Count("id"))
            .order_by("created__week", "created__month", "created__year")
        )
        events_dict = [
            {
                "date_event": f"sem{elt.get('created__week')}({elt.get('created__month')}-{elt.get('created__year')})",
                "count": elt.get("count"),
                "name": elt.get("name"),
            }
            for elt in events
        ]
    elif group_by == "month":
        events = (
            Event.objects.filter(created__gte=date_start, created__lte=date_end)
            .values("name", "object", "created__month", "created__year")
            .annotate(count=Count("id"))
            .order_by("created__month", "created__year")
        )
        events_dict = [
            {
                "date_event": f"{elt.get('created__month')}-{elt.get('created__year')}",
                "count": elt.get("count"),
                "name": elt.get("name"),
            }
            for elt in events
        ]
    elif group_by == "year":
        events = (
            Event.objects.filter(created__gte=date_start, created__lte=date_end)
            .values("name", "object", "created__year")
            .annotate(count=Count("id"))
            .order_by("created__year")
        )
        events_dict = [
            {
                "date_event": f"{elt.get('created__year')}",
                "count": elt.get("count"),
                "name": elt.get("name"),
            }
            for elt in events
        ]
    else:
        events_dict = []

    liste_date_event = [item.get("date_event") for item in events_dict]
    categories_sort = sorted(
        list(set(liste_date_event)), key=functools.cmp_to_key(compare_date_group_by)
    )
    categories = list(categories_sort)
    series_key = list({item.get("name") for item in events_dict})
    series = {item: [0 for _ in categories] for item in series_key}
    for i in range(len(categories)):
        for elt in events_dict:
            if elt.get("date_event") == categories[i]:
                series[elt.get("name")][i] = elt.get("count")
    series_finale = list(series.items())
    series_dict = [{"name": item[0], "data": item[1]} for item in series_finale]

    return JsonResponse({"categories": list(categories), "series": series_dict})


#
def get_analytic__for_piechart_3d(date_start, date_end):
    #
    def get_analytic_paiement_for_piechart_3d():
        paiement_valid = PaiementCloud.objects.filter(
            date_created__gte=date_start, date_created__lte=date_end, state="validat"
        ).aggregate(Sum("montant"))
        paiement_paid = PaiementCloud.objects.filter(
            date_created__gte=date_start, date_created__lte=date_end, state="open"
        ).aggregate(Sum("montant"))
        paiement_cancel_or_denial = PaiementCloud.objects.filter(
            date_created__gte=date_start,
            date_created__lte=date_end,
            state__in=["reject", "cancel"],
        ).aggregate(Sum("montant"))
        data = [
            {"name": "validé", "y": paiement_valid.get("montant__sum")},
            {"name": "en cours", "y": paiement_paid.get("montant__sum")},
            {
                "name": "annulé ou rejeté",
                "y": paiement_cancel_or_denial.get("montant__sum"),
            },
        ]
        return [{"name": "Paiement", "data": data}]

    #
    def get_analytic_commande_for_piechart_3d():
        commande_valid = CommandeCloud.objects.filter(
            date_created__gte=date_start,
            date_created__lte=date_end,
            state__in=["validat", "paid"],
        ).aggregate(Count("id"))
        commande_open = CommandeCloud.objects.filter(
            date_created__gte=date_start, date_created__lte=date_end, state="open"
        ).aggregate(Count("id"))
        commande_cancel = CommandeCloud.objects.filter(
            date_created__gte=date_start, date_created__lte=date_end, state="cancel"
        ).aggregate(Count("id"))
        data = [
            {"name": "validé", "y": commande_valid.get("id__count")},
            {"name": "en cours", "y": commande_open.get("id__count")},
            {"name": "annulé", "y": commande_cancel.get("id__count")},
        ]
        return [{"name": "Commande", "data": data}]

    #
    def get_analytic_instance_for_piechart_3d():
        instnace_start_or_open = InstanceCloud.objects.filter(
            date_created__gte=date_start,
            date_created__lte=date_end,
            state__in=["started", "pending"],
        ).aggregate(Count("id"))
        instance_error_or_expire = InstanceCloud.objects.filter(
            date_created__gte=date_start,
            date_created__lte=date_end,
            state__in=["revoking", "revoked", "error", "expire"],
        ).aggregate(Count("id"))
        instance_stop = InstanceCloud.objects.filter(
            date_created__gte=date_start,
            date_created__lte=date_end,
            state__in=["stopped", "stopping"],
        ).aggregate(Count("id"))
        data = [
            {"name": "démarré", "y": instnace_start_or_open.get("id__count")},
            {
                "name": "expiré ou erreur",
                "y": instance_error_or_expire.get("id__count"),
            },
            {"name": "arrêté", "y": instance_stop.get("id__count")},
        ]
        return [{"name": "Instance", "data": data}]

    #
    content = {
        "analytic_paiement": get_analytic_paiement_for_piechart_3d(),
        "analytic_commande": get_analytic_commande_for_piechart_3d(),
        "analytic_instance": get_analytic_instance_for_piechart_3d(),
    }
    return content


#
def get_analytic_for_corechart(request):
    if not request.POST or not is_ajax(request):
        group_by = "day"
        date_end = datetime.now()
        date_start = date_end - timedelta(days=30)
    else:
        group_by = request.POST.get("group_by")
        date_start = request.POST.get("date_start")
        date_end = request.POST.get("date_end")
    #
    if not date_start or not date_end:
        group_by = "day"
        date_end = datetime.now()
        date_start = date_end - timedelta(days=30)
    #
    def get_result_paiement():
        if group_by == "day":
            result_paiement = (
                PaiementCloud.objects.filter(
                    date_created__gte=date_start, date_created__lte=date_end
                )
                .values(
                    "date_created__date",
                )
                .annotate(sum=Sum("montant"))
                .order_by("-date_created__date")
            )
            return [
                [f"{elt.get('date_created__date')}", elt.get("sum")]
                for elt in result_paiement
            ]
        elif group_by == "week":
            result_paiement = (
                PaiementCloud.objects.filter(
                    date_created__gte=date_start, date_created__lte=date_end
                )
                .values(
                    "date_created__week",
                    "date_created__month",
                    "date_created__year",
                )
                .annotate(sum=Sum("montant"))
                .order_by(
                    "-date_created__year", "-date_created__month", "-date_created__week"
                )
            )
            return [
                [
                    f"sem{elt.get('date_created__week')}({elt.get('date_created__month')}-{elt.get('date_created__year')})",
                    elt.get("sum"),
                ]
                for elt in result_paiement
            ]
        elif group_by == "month":
            result_paiement = (
                PaiementCloud.objects.filter(
                    date_created__gte=date_start, date_created__lte=date_end
                )
                .values(
                    "date_created__month",
                    "date_created__year",
                )
                .annotate(sum=Sum("montant"))
                .order_by("-date_created__year", "-date_created__month")
            )
            return [
                [
                    f"{elt.get('date_created__month')}-{elt.get('date_created__year')}",
                    elt.get("sum"),
                ]
                for elt in result_paiement
            ]
        elif group_by == "year":
            result_paiement = (
                PaiementCloud.objects.filter(
                    date_created__gte=date_start, date_created__lte=date_end
                )
                .values(
                    "date_created__year",
                )
                .annotate(sum=Sum("montant"))
                .order_by("-date_created__year")
            )
            return [
                [f"{elt.get('date_created__year')}", elt.get("sum")]
                for elt in result_paiement
            ]
        else:
            return []

    #
    def get_result_commande():
        if group_by == "day":
            result_commande = (
                CommandeCloud.objects.filter(
                    date_created__gte=date_start, date_created__lte=date_end
                )
                .values(
                    "date_created__date",
                )
                .annotate(count=Count("id"))
                .order_by("-date_created__date")
            )
            return [
                [f"{elt.get('date_created__date')}", elt.get("count")]
                for elt in result_commande
            ]
        elif group_by == "week":
            result_commande = (
                CommandeCloud.objects.filter(
                    date_created__gte=date_start, date_created__lte=date_end
                )
                .values(
                    "date_created__week",
                    "date_created__month",
                    "date_created__year",
                )
                .annotate(count=Count("id"))
                .order_by(
                    "-date_created__year", "-date_created__month", "-date_created__week"
                )
            )
            return [
                [
                    f"sem{elt.get('date_created__week')}({elt.get('date_created__month')}-{elt.get('date_created__year')})",
                    elt.get("count"),
                ]
                for elt in result_commande
            ]
        elif group_by == "month":
            result_commande = (
                CommandeCloud.objects.filter(
                    date_created__gte=date_start, date_created__lte=date_end
                )
                .values(
                    "date_created__month",
                    "date_created__year",
                )
                .annotate(count=Count("id"))
                .order_by("-date_created__year", "-date_created__month")
            )
            return [
                [
                    f"{elt.get('date_created__month')}-{elt.get('date_created__year')}",
                    elt.get("count"),
                ]
                for elt in result_commande
            ]
        elif group_by == "year":
            result_commande = (
                CommandeCloud.objects.filter(
                    date_created__gte=date_start, date_created__lte=date_end
                )
                .values(
                    "date_created__year",
                )
                .annotate(count=Count("id"))
                .order_by("-date_created__year")
            )
            return [
                [f"{elt.get('date_created__year')}", elt.get("count")]
                for elt in result_commande
            ]
        else:
            return []

    #
    def get_result_instance():
        if group_by == "day":
            result_instance = (
                InstanceCloud.objects.filter(
                    date_created__gte=date_start, date_created__lte=date_end
                )
                .values(
                    "date_created__date",
                )
                .annotate(count=Count("id"))
                .order_by("-date_created__date")
            )
            return [
                [f"{elt.get('date_created__date')}", elt.get("count")]
                for elt in result_instance
            ]
        elif group_by == "week":
            result_instance = (
                InstanceCloud.objects.filter(
                    date_created__gte=date_start, date_created__lte=date_end
                )
                .values(
                    "date_created__week",
                    "date_created__month",
                    "date_created__year",
                )
                .annotate(count=Count("id"))
                .order_by(
                    "-date_created__year", "-date_created__month", "-date_created__week"
                )
            )
            return [
                [
                    f"sem{elt.get('date_created__week')}({elt.get('date_created__month')}-{elt.get('date_created__year')})",
                    elt.get("count"),
                ]
                for elt in result_instance
            ]
        elif group_by == "month":
            result_instance = (
                InstanceCloud.objects.filter(
                    date_created__gte=date_start, date_created__lte=date_end
                )
                .values(
                    "date_created__month",
                    "date_created__year",
                )
                .annotate(count=Count("id"))
                .order_by("-date_created__year", "-date_created__month")
            )
            return [
                [
                    f"{elt.get('date_created__month')}-{elt.get('date_created__year')}",
                    elt.get("count"),
                ]
                for elt in result_instance
            ]
        elif group_by == "year":
            result_instance = (
                InstanceCloud.objects.filter(
                    date_created__gte=date_start, date_created__lte=date_end
                )
                .values(
                    "date_created__year",
                )
                .annotate(count=Count("id"))
                .order_by("-date_created__year")
            )
            return [
                [f"{elt.get('date_created__year')}", elt.get("count")]
                for elt in result_instance
            ]

    #
    data_analytic_commande = get_result_commande()
    data_analytic_instance = get_result_instance()
    data_analytic_paiement = get_result_paiement()
    #
    l = []
    for elt in data_analytic_commande:
        l.append(elt[0])
    for elt in data_analytic_instance:
        l.append(elt[0])
    for elt in data_analytic_paiement:
        l.append(elt[0])
    #
    def compare_date_group_by(date_group_by_a, date_group_by_b):
        if group_by == "day":
            return 1 if date_group_by_a > date_group_by_b else -1
        elif group_by == "week":
            date_group_by_a = (
                date_group_by_a.replace("sem", "").replace("(", "-").replace(")", "")
            )
            date_group_by_b = (
                date_group_by_b.replace("sem", "").replace("(", "-").replace(")", "")
            )
            week_month_year_a = date_group_by_a.split("-")
            week_month_year_b = date_group_by_b.split("-")
            #
            if int(week_month_year_a[2]) == int(week_month_year_b[2]):
                if int(week_month_year_a[1]) == int(week_month_year_b[1]):
                    return (
                        1
                        if int(week_month_year_a[0]) > int(week_month_year_b[0])
                        else -1
                    )
                elif int(week_month_year_a[1]) > int(week_month_year_b[1]):
                    return 1
                else:
                    return -1
            elif int(week_month_year_a[2]) > int(week_month_year_b[2]):
                return 1
            else:
                return -1
        elif group_by == "month":
            month_year_a = date_group_by_a.split("-")
            month_year_b = date_group_by_b.split("-")
            #
            if int(month_year_a[1]) == int(month_year_b[1]):
                return 1 if int(month_year_a[0]) > int(month_year_b[0]) else -1
            elif int(month_year_a[1]) > int(month_year_b[1]):
                return 1
            else:
                return -1
        elif group_by == "year":
            return 1 if int(date_group_by_a) > int(date_group_by_b) else -1

    abcis_value = sorted(list(set(l)), key=functools.cmp_to_key(compare_date_group_by))

    def convert_data_analytic_list_to_dict(list_data_analytic):
        return {elt[0]: elt[1] for elt in list_data_analytic}

    #
    data_analytic_commande_dict = convert_data_analytic_list_to_dict(
        data_analytic_commande
    )
    data_analytic_instance_dict = convert_data_analytic_list_to_dict(
        data_analytic_instance
    )
    data_analytic_paiement_dict = convert_data_analytic_list_to_dict(
        data_analytic_paiement
    )
    data_analytic_commande_list = []
    data_analytic_instance_list = []
    data_analytic_paiement_list = []
    for elt in abcis_value:
        if data_analytic_commande_dict.get(elt):
            data_analytic_commande_list.append(data_analytic_commande_dict.get(elt))
        else:
            data_analytic_commande_list.append(0)
        #
        if data_analytic_instance_dict.get(elt):
            data_analytic_instance_list.append(data_analytic_instance_dict.get(elt))
        else:
            data_analytic_instance_list.append(0)
        #
        if data_analytic_paiement_dict.get(elt):
            data_analytic_paiement_list.append(data_analytic_paiement_dict.get(elt))
        else:
            data_analytic_paiement_list.append(0)
    #
    data = {
        "data_analytic__barchart": {
            "analytic_commande": {
                "categories": list(abcis_value),
                "series": {"name": "Commandes", "data": data_analytic_commande_list},
            },
            "analytic_instance": {
                "categories": list(abcis_value),
                "series": {"name": "Instances", "data": data_analytic_instance_list},
            },
            "analytic_paiement": {
                "categories": list(abcis_value),
                "series": {"name": "Paiements", "data": data_analytic_paiement_list},
            },
        },
        "data_analytic__for_piechart_3d": get_analytic__for_piechart_3d(
            date_start, date_end
        ),
    }
    #
    return JsonResponse(data)


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def flower_view(request):
    response = HttpResponse()
    path = request.get_full_path()
    path = path.replace("flower", "rewolf", 1)
    response["X-Accel-Redirect"] = path
    return response
