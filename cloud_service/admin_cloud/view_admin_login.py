from django.contrib import auth
from django.contrib.auth import get_user_model
from django.shortcuts import redirect, render

from user_cloud.models import UserCloud


def login_admin(request):
    UserModel = get_user_model()
    if request.user.is_authenticated and request.user.is_superuser:
        if request.GET.get("next_page"):
            return redirect(request.GET.get("next_page"))
        return redirect("admin_account")
    elif request.method == "POST":
        email = request.POST.get("email")
        password = request.POST.get("password")
        try:
            if user := UserModel.objects.get(email=email):
                if user := auth.authenticate(username=user.username, password=password):
                    if user.is_superuser:
                        auth.login(request, user)
                        admin = UserCloud.objects.get(id=request.user.id)
                        if not admin.enabled:
                            auth.logout(request)
                            return render(
                                request,
                                "admin_login.html",
                                context={
                                    "msg": "Votre compte est déactivé",
                                    "next_page": request.GET.get("next_page"),
                                },
                            )
                        if request.GET.get("next_page"):
                            return redirect(request.GET.get("next_page"))
                    else:
                        auth.logout(request)
                        return render(
                            request,
                            "admin_login.html",
                            context={
                                "msg": "Vous n'êtes pas autorisé à accéder à cette page.",
                                "next_page": request.GET.get("next_page"),
                            },
                        )
                    auth.logout(request)
                    return redirect("admin_account")
            return render(
                request,
                "admin_login.html",
                context={"error": True, "next_page": request.GET.get("next_page")},
            )
        except UserModel.DoesNotExist:
            return render(
                request,
                "admin_login.html",
                context={"error": True, "next_page": request.GET.get("next_page")},
            )
        except Exception as e:
            return render(
                request,
                "admin_login.html",
                context={"error": True, "next_page": request.GET.get("next_page")},
            )
    else:
        return render(
            request,
            "admin_login.html",
            context={"next_page": request.GET.get("next_page")},
        )
