from django import forms
from django.contrib.auth.decorators import user_passes_test
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import render

from admin_cloud.forms import AddGroupServiceForm
from cloud_service.settings import PAGINATION_DEFAULT_PAGINATION
from groupe_service.models import GroupServiceCloud


@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def addGroupeService(request):
    if request.method == "POST":
        form = AddGroupServiceForm(request.POST, request.FILES)
        if not form.is_valid():
            return render(
                request,
                "admin_account.html",
                context={"form": form, "action": "add", "object": "groupe_service"},
            )
        data = form.cleaned_data
        GroupService = GroupServiceCloud.objects.create(
            name=data["name"],
            image=data["image"],
            description=data["description"],
            info_sup=data["info_sup"],
        )
        GroupService.save()
        return HttpResponseRedirect("/adm/account/list/groupe_service")
    form = AddGroupServiceForm()
    return render(
        request,
        "admin_account.html",
        context={"form": form, "action": "add", "object": "groupe_service"},
    )


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def actionGroupeService(request):
    if "activer" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            grpservice = GroupServiceCloud.objects.get(id=int(item))
            if not grpservice.enabled:
                grpservice.enabled = True
                grpservice.save()
    #
    elif "deactiver" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            grpservice = GroupServiceCloud.objects.get(id=int(item))
            if grpservice.enabled:
                grpservice.enabled = False
                grpservice.save()
    #
    elif "supprimer" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            grpservice = GroupServiceCloud.objects.get(id=int(item))
            grpservice.delete()
    #
    return HttpResponseRedirect("/adm/account/list/groupe_service/")


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def getGroupeService(request):
    if code := request.GET.get("code"):
        if grpservice := GroupServiceCloud.objects.get(code=code):
            return render(
                request,
                "admin_account.html",
                context={
                    "grp_service_detail": grpservice,
                    "action": "list",
                    "object": "groupe_service",
                },
            )
    grp_service_all = GroupServiceCloud.objects.all().order_by("-id")
    pages = Paginator(grp_service_all, PAGINATION_DEFAULT_PAGINATION)
    if page_number := request.GET.get("page"):
        page_number = int(page_number)
        if page_number > pages.num_pages or page_number < 1:
            page_number = 1
        page = pages.page(page_number)
    else:
        page = pages.page(1)
    return render(
        request,
        "admin_account.html",
        context={"page": page, "object": "groupe_service", "action": "list"},
    )
