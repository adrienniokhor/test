import datetime
import io

from django import forms
from django.contrib import messages
from django.contrib.auth.decorators import user_passes_test
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from admin_cloud.forms import UserForm
from analytics.models import Event
from cloud_service.settings import PAGINATION_DEFAULT_PAGINATION
from services.models import UserCloud
from user_cloud.models import UserCloud  # import messages


@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def addAdmin(request):
    if request.method == "POST":
        form_user = UserForm(request.POST)
        if not form_user.is_valid():
            return render(
                request,
                "admin_account.html",
                context={"form_user": form_user, "object": "admin", "action": "add"},
            )
        data = form_user.cleaned_data
        user = UserCloud.objects.create_user(
            first_name=data["first_name"],
            last_name=data["last_name"],
            phone=data["phone"],
            address=data["address"],
            country=data["country"],
            email=data["email"],
            username=data["email"],
            is_superuser=True,
        )
        user.save()
        register_user = Event(
            name="Registered",
            object="user",
            code=user.code,
            created=datetime.datetime.now(),
        )
        register_user.save()
        return HttpResponseRedirect("/adm/account/setting")
    form_user = UserForm()
    return render(
        request,
        "admin_account.html",
        context={"form_user": form_user, "object": "admin", "action": "add"},
    )
