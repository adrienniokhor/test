from random import choices

from django import forms
from django.contrib.auth.decorators import user_passes_test
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import render

from admin_cloud.forms import AddServiceForm
from cloud_service.settings import PAGINATION_DEFAULT_PAGINATION
from groupe_service.models import GroupServiceCloud
from services.models import ActionCloud, ServiceCloud


@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def addService(request):
    if request.method == "POST":
        form_service = AddServiceForm(request.POST, request.FILES)
        if not form_service.is_valid():
            return render(
                request,
                "admin_account.html",
                context={
                    "form_service": form_service,
                    "object": "service",
                    "action": "add",
                },
            )
        data = form_service.cleaned_data
        actions = list(data["action"])
        Service = ServiceCloud.objects.create(
            name=data["name"],
            image=data["image"],
            description=data["description"],
            price=data["price"],
            currency=data["currency"],
            groupe_service=data["groupe_service"],
            executor=data["executor"],
        )
        Service.action.set(actions)
        Service.save()
        return HttpResponseRedirect("/adm/account/list/service")
    form_service = AddServiceForm()
    return render(
        request,
        "admin_account.html",
        context={"form_service": form_service, "object": "service", "action": "add"},
    )


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def actionService(request):
    if "activer" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            service = ServiceCloud.objects.get(id=int(item))
            if not service.enabled:
                service.enabled = True
                service.save()
    #
    elif "deactiver" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            service = ServiceCloud.objects.get(id=int(item))
            if service.enabled:
                service.enabled = False
                service.save()
    #
    elif "supprimer" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            service = ServiceCloud.objects.get(id=int(item))
            service.delete()
    return HttpResponseRedirect("/adm/account/list/service/")


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def getService(request):
    if code := request.GET.get("code"):
        if service := ServiceCloud.objects.get(code=code):
            return render(
                request,
                "admin_account.html",
                context={
                    "service_detail": service,
                    "object": "service",
                    "action": "list",
                },
            )
    services = ServiceCloud.objects.all().order_by("-id")
    pages = Paginator(services, PAGINATION_DEFAULT_PAGINATION)
    if page_number := request.GET.get("page"):
        page_number = int(page_number)
        if page_number > pages.num_pages or page_number < 1:
            page_number = 1
        page = pages.page(page_number)
    else:
        page = pages.page(1)
    return render(
        request,
        "admin_account.html",
        context={"page": page, "object": "service", "action": "list"},
    )
