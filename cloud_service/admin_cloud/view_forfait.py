from django.contrib.auth.decorators import user_passes_test
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import render

from admin_cloud.forms import AddForfaitCloudForm
from cloud_service.settings import PAGINATION_DEFAULT_PAGINATION
from services.models import ForfaitCloud


@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def addForfait(request):
    if request.method == "POST":
        form_forfait = AddForfaitCloudForm(request.POST)
        if not form_forfait.is_valid():
            return render(
                request,
                "admin_account.html",
                context={
                    "form_forfait": form_forfait,
                    "object": "forfait",
                    "action": "add",
                },
            )
        data = form_forfait.cleaned_data
        forfait = ForfaitCloud.objects.create(
            durree=data["durree"],
            reduction=data["reduction"],
        )
        forfait.save()
        return HttpResponseRedirect("/adm/account/list/forfait/")
    form_forfait = AddForfaitCloudForm()
    return render(
        request,
        "admin_account.html",
        context={"form_forfait": form_forfait, "object": "forfait", "action": "add"},
    )


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def actionForfait(request):
    if "activer" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            forfait = ForfaitCloud.objects.get(id=int(item))
            if not forfait.enabled:
                forfait.enabled = True
                forfait.save()
    #
    elif "deactiver" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            forfait = ForfaitCloud.objects.get(id=int(item))
            if forfait.enabled:
                forfait.enabled = False
                forfait.save()
    #
    elif "supprimer" in request.POST:
        select_items = request.POST.getlist("select_items")
        print(select_items)
        for item in select_items:
            forfait = ForfaitCloud.objects.get(id=int(item))
            forfait.delete()
    return HttpResponseRedirect("/adm/account/list/forfait/")


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def getForfait(request):
    forfaits = ForfaitCloud.objects.all().order_by("-id")
    pages = Paginator(forfaits, PAGINATION_DEFAULT_PAGINATION)
    if page_number := request.GET.get("page"):
        page_number = int(page_number)
        if page_number > pages.num_pages or page_number < 1:
            page_number = 1
        page = pages.page(page_number)
    else:
        page = pages.page(1)
    return render(
        request,
        "admin_account.html",
        context={"page": page, "object": "forfait", "action": "list"},
    )
