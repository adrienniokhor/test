"""cloud_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from admin_cloud.action_edit.view.action import updateAction
from admin_cloud.action_edit.view.admin import deleteAdmin, updateAdmin
from admin_cloud.action_edit.view.forfait import updateForfait
from admin_cloud.action_edit.view.grp_service import updateGroupeService
from admin_cloud.action_edit.view.instance import updateInstance
from admin_cloud.action_edit.view.paiement import updatePaiement
from admin_cloud.action_edit.view.service import updateService
from admin_cloud.action_edit.view.user import updateUser
from admin_cloud.geneted_pdf import GeneratePdf
from admin_cloud.user_view import actionUser, addUser, getUser
from admin_cloud.view_action import actionAction, addAction, getAction
from admin_cloud.view_admin import addAdmin
from admin_cloud.view_admin_login import login_admin
from admin_cloud.view_commande import actionCommande, addCommande, getCommande
from admin_cloud.view_forfait import actionForfait, addForfait, getForfait
from admin_cloud.view_groupe_service import (
    actionGroupeService,
    addGroupeService,
    getGroupeService,
)
from admin_cloud.view_instance import (
    actionInstance,
    addInstance,
    compareInstance,
    getInstance,
    getInstanceFile,
)
from admin_cloud.view_offresurmesure import getadmOffresurmesure
from admin_cloud.view_paiement import actionPaiement, addPaiement, getPaiement
from admin_cloud.view_service import actionService, addService, getService
from admin_cloud.view_setting import (
    adminEditPassword,
    adminEditProfil,
    adminSetting,
    deleteParameter,
    editPassword,
    enable_disable_admin,
    getParameter,
    getParameters,
    newParameter,
)
from admin_cloud.views import (
    adminAccount,
    flower_view,
    get_analytic_for_corechart,
    get_analytic_values_graph_slicing,
)

urlpatterns = [
    path("adm/account/", adminAccount, name="admin_account"),
    # login
    path("adm/account/login/", login_admin, name="login_admin"),
    # URL FOR LIST OBJECT
    path(
        "adm/account/list/groupe_service/",
        getGroupeService,
        name="admin_groupe_service",
    ),
    path("adm/account/list/service/", getService, name="admin_service"),
    path("adm/account/list/action/", getAction, name="admin_action"),
    path("adm/account/list/instance/", getInstance, name="admin_instance"),
    path(
        "adm/account/list/instance/<str:code>/<str:filename>",
        getInstanceFile,
        name="admin_instance_file",
    ),
    path("adm/account/list/commande/", getCommande, name="admin_commande"),
    path("adm/account/list/paiement/", getPaiement, name="admin_paiement"),
    path("adm/account/list/user/", getUser, name="admin_user"),
    path(
        "adm/account/list/offresurmesure/",
        getadmOffresurmesure,
        name="admin_offresurmesure",
    ),
    path("adm/account/list/forfait/", getForfait, name="admin_forfait"),
    # URL FOR ADD OBJECT
    path("adm/account/add/service/", addService, name="add_service"),
    path("adm/account/add/action/", addAction, name="add_action"),
    path("adm/account/add/instance/", addInstance, name="add_instance"),
    path("adm/account/add/commande/", addCommande, name="add_commande"),
    path(
        "adm/account/add/groupe_service/", addGroupeService, name="add_groupe_service"
    ),
    path("adm/account/add/paiement/", addPaiement, name="admin_add_paiement"),
    path("adm/account/add/user/", addUser, name="admin_add_user"),
    path("adm/account/add/forfait/", addForfait, name="admin_add_forfait"),
    # URL FOR DELETE OBJECT
    # path('adm/account/delete/service/', deleteService,name='delete_service'),
    # path('adm/account/delete/action/', deleteAction,name='delete_action'),
    # path('adm/account/delete/instance/', deleteInstance,name='delete_instance'),
    # path('adm/account/delete/commande/', deleteCommande,name='delete_commande'),
    # path('adm/account/delete/groupe_service/', deleteGroupeService,name='delete_groupe_service'),
    # path('adm/account/delete/paiement/', deletePaiement,name='delete_paiement'),
    # URL FOR UPDATE OBJECT
    path(
        "adm/account/update/service/<str:pk>",
        updateService.as_view(),
        name="update_service",
    ),
    path(
        "adm/account/update/action/<str:pk>",
        updateAction.as_view(),
        name="update_action",
    ),
    path(
        "adm/account/update/instance/<str:pk>",
        updateInstance.as_view(),
        name="update_instance",
    ),
    # path('adm/account/update/commande/', updateCommande,name='update_commande'),
    path(
        "adm/account/update/groupe_service/<str:pk>",
        updateGroupeService.as_view(),
        name="update_groupe_service",
    ),
    path(
        "adm/account/update/paiement/<str:pk>",
        updatePaiement.as_view(),
        name="update_paiement",
    ),
    path("adm/account/update/user/<str:pk>", updateUser.as_view(), name="update_user"),
    path(
        "adm/account/update/forfait/<str:pk>",
        updateForfait.as_view(),
        name="update_forfait",
    ),
    # URL FOR ACTION ON OBJECT
    path("adm/account/action/service/", actionService, name="action_service"),
    path("adm/account/action/action/", actionAction, name="action_action"),
    path("adm/account/action/instance/", actionInstance, name="action_instance"),
    path("adm/account/action/commande/", actionCommande, name="action_commande"),
    path(
        "adm/account/action/groupe_service/",
        actionGroupeService,
        name="action_groupe_service",
    ),
    path("adm/account/action/paiement/", actionPaiement, name="action_paiement"),
    path("adm/account/action/user/", actionUser, name="action_user"),
    path("adm/account/action/forfait/", actionForfait, name="action_forfait"),
    # URL FOR ACTION USING WEB SERVICE
    path("adm/account/instance/compare/", compareInstance, name="compare_instance"),
    # Analytics
    path(
        "adm/account/analytics/slice/group/",
        get_analytic_values_graph_slicing,
        name="analytics_slice_group",
    ),
    path(
        "adm/account/analytics/drawchart",
        get_analytic_for_corechart,
        name="analytics_for_drawchart",
    ),
    # generation des factures
    path("adm/account/action/facture/", GeneratePdf.as_view()),
    # Setting Admin
    path("adm/account/setting/", adminSetting, name="admin_setting"),
    path(
        "adm/account/setting/password",
        adminEditPassword,
        name="admin_setting_edit_password",
    ),
    path(
        "adm/account/setting/profil", adminEditProfil, name="admin_setting_edit_profil"
    ),
    # Parameter Admin
    path("adm/account/setting/param/new", newParameter, name="admin_setting_new_param"),
    path(
        "adm/account/setting/param/get", getParameters, name="admin_setting_get_param"
    ),
    path(
        "adm/account/setting/param/get/<int:id>",
        getParameter,
        name="admin_setting_get_param_with_id",
    ),
    path(
        "adm/account/setting/param/delete/<int:id>",
        deleteParameter,
        name="admin_setting_delete_param_with_id",
    ),
    # Edit Admin
    path(
        "adm/account/update/admin/<str:pk>", updateAdmin.as_view(), name="update_admin"
    ),
    path(
        "adm/account/delete/admin/<str:pk>", deleteAdmin.as_view(), name="delete_admin"
    ),
    path(
        "adm/account/enable_disable/admin/<int:id>/<str:status>",
        enable_disable_admin,
        name="admin_enable_disable",
    ),
    path("adm/account/add/admin/", addAdmin, name="add_admin"),
    path(
        "adm/account/edit_password/admin/<int:id>",
        editPassword,
        name="admin_edit_password",
    ),
    # Flower
    path("adm/account/flower/", flower_view, name="admin_flower"),
]
