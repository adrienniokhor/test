import datetime
import mimetypes
import os

from botocore.exceptions import ClientError as BotoClientError
from django.contrib import messages
from django.contrib.auth.decorators import user_passes_test
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse
from django.shortcuts import render

from admin_cloud.forms import AddInstanceForm
from analytics.models import Event
from cloud_service.settings import KEYFILES_DIRS, PAGINATION_DEFAULT_PAGINATION
from services.models import InstanceActionValueCloud, InstanceCloud

from .tasks import (
    task_Compare_local_and_remote_instance,
    task_DeleteInstance,
    task_RevokeInstance,
    task_SartInstance,
    task_StopInstance,
)


@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def addInstance(request):
    if request.method == "POST":
        form_instance = AddInstanceForm(request.POST)
        if not form_instance.is_valid():
            return render(
                request,
                "admin_account.html",
                context={
                    "form_instance": form_instance,
                    "object": "instance",
                    "action": "add",
                },
            )
        data = form_instance.cleaned_data
        instance = InstanceCloud.objects.create(
            date_created=data["date_created"],
            date_revoked=data["date_revoked"],
            state=data["state"],
            code=data["code"],
            service=data["service"],
            owner=data["owner"],
        )
        instance.save()
        create_instance_event = Event(
            name="Create Instance",
            object="instance",
            code=instance.code,
            created=datetime.datetime.now(),
        )
        create_instance_event.save()
        return HttpResponseRedirect("/adm/account/list/instance/")
    form_instance = AddInstanceForm()
    return render(
        request,
        "admin_account.html",
        context={"form_instance": form_instance, "object": "instance", "action": "add"},
    )


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def actionInstance(request):  # sourcery skip: low-code-quality
    #
    try:
        #
        if "arreter" in request.POST:
            select_items = request.POST.getlist("select_items")
            for item in select_items:
                if instance := InstanceCloud.objects.get(id=int(item)):
                    if instance.state and instance.state == "revoked":
                        messages.warning(
                            request,
                            f"Instance {instance.code}: est révoquée , donc arrêtée !!",
                        )
                    elif instance.state and instance.state == "stopped":
                        messages.warning(
                            request, f"Instance {instance.code}: est déja arrêtée"
                        )
                    else:
                        instance.state = "stopping"
                        instance.save()
                        messages.info(
                            request, f"Instance {instance.code}: est en cours d'arrêt"
                        )
            #
            task_StopInstance.delay(select_items)
        #
        elif "demarrer" in request.POST:
            select_items = request.POST.getlist("select_items")
            for item in select_items:
                if instance := InstanceCloud.objects.get(id=int(item)):
                    if instance.state and instance.state == "started":
                        messages.warning(
                            request, f"Instance {instance.code}: est déja démarrée"
                        )
                    elif instance.state and instance.state == "pending":
                        messages.warning(
                            request,
                            f"Instance {instance.code}: est en cours de démarrage Veiller patienter !!",
                        )
                    else:
                        instance.state = "pending"
                        instance.save()
                        messages.info(
                            request,
                            f"Instance {instance.code}: est en cours de démarrage",
                        )
            #
            task_SartInstance.delay(select_items)
        #
        elif "supprimer" in request.POST:
            select_items = request.POST.getlist("select_items")
            for item in select_items:
                if instance := InstanceCloud.objects.get(id=int(item)):
                    instance.state = "deleting"
                    instance.save()
                    messages.info(
                        request, f"Instance {instance.code}: est bien supprimée"
                    )
            #
            task_DeleteInstance.delay(select_items)
        #
        elif "resilier" in request.POST:
            select_items = request.POST.getlist("select_items")
            for item in select_items:
                if instance := InstanceCloud.objects.get(id=int(item)):
                    if instance.state and instance.state == "revoked":
                        messages.warning(
                            request, f"Instance {instance.code}: est déja révoquée"
                        )
                    else:
                        instance.state = "revoking"
                        instance.save()
                        messages.info(
                            request, f"Instance {instance.code}: est bien révoquée"
                        )
            #
            task_RevokeInstance.delay(select_items)
        #
        elif "compare" in request.POST:
            messages.info(request, "Le Rapprochement est en cours")
            list_instance_id = request.POST.getlist("select_items")
            task_Compare_local_and_remote_instance.delay(list_instance_id)
    except BotoClientError:
        messages.error(
            request,
            "Une erreur s'est produite, veuillez vérifier que l'action a correctement abouti ,un rapproche peut-être  nécessaire !!",
        )
    except Exception:
        messages.error(
            request, "Une erreur s'est produite, un rapproche peut être nécessaire !!"
        )
    return HttpResponseRedirect("/adm/account/list/instance/")


#
#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def getInstance(request):
    if code := request.GET.get("code"):
        if instance := InstanceCloud.objects.get(code=code):
            return render(
                request,
                "admin_account.html",
                context={
                    "instance_detail": instance,
                    "object": "instance",
                    "action": "list",
                },
            )

    instances = InstanceCloud.objects.all().order_by("-id")
    pages = Paginator(instances, PAGINATION_DEFAULT_PAGINATION)
    if page_number := request.GET.get("page"):
        page_number = int(page_number)
        if page_number > pages.num_pages or page_number < 1:
            page_number = 1
        page = pages.page(page_number)
    else:
        page = pages.page(1)
    return render(
        request,
        "admin_account.html",
        context={"page": page, "object": "instance", "action": "list"},
    )


#
#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def compareInstance(request):
    messages.info(request, "Le Rapprochement est en cours")
    task_Compare_local_and_remote_instance.delay(request)


#
#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def getInstanceFile(request, code=None, filename=None):
    if not filename:
        return render(request, "admin.html")
    if instance_action_value := InstanceActionValueCloud.objects.get(code=code):
        filepath = os.path.join(KEYFILES_DIRS, filename)
        if not os.path.exists(filepath):
            with open(filepath, "w") as f:
                f.write(instance_action_value.value)
        else:
            with open(filepath, "r") as file:
                if file.read() != instance_action_value.value:
                    with open(filepath, "w") as f:
                        f.write(instance_action_value.value)
        path = open(filepath, "r")
        mime_type, _ = mimetypes.guess_type(filepath)
        response = HttpResponse(path, content_type=mime_type)
        response["Content-Disposition"] = f"attachment; filename={filename}"
        return response


#
