from django import forms
from django_countries.data import COUNTRIES

from commande.models import CommandeCloud, ModePaiementCloud, PaiementCloud
from groupe_service.models import GroupServiceCloud
from services.models import ActionCloud, ForfaitCloud, InstanceCloud, ServiceCloud
from user_cloud.models import UserCloud

#

#
class AddPaiementForm(forms.Form):
    devise = [("fcfa", "FCFA"), ("euro", "EURO"), ("dollar", "Dollar")]
    style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
    # code = forms.CharField(label='Nom', max_length=100,widget=forms.TextInput(attrs={'class':style_class}))
    # date_created = forms.DateTimeField(label='Date Création',widget=forms.DateInput(attrs={'class':style_class}))
    date_paiement = forms.DateField(
        label="Date Paiement", widget=forms.DateInput(attrs={"class": style_class})
    )
    commande = forms.ModelChoiceField(
        label="Commande",
        queryset=CommandeCloud.objects.filter(state__in=["open", "validat"]),
        widget=forms.Select(attrs={"class": style_class}),
    )
    mode_paiement = forms.ModelChoiceField(
        label="Mode de Paiement",
        queryset=ModePaiementCloud.objects.filter(enabled=True),
        widget=forms.Select(attrs={"class": style_class}),
    )
    numTransaction_or_numCarte = forms.CharField(
        label="Numéro Transaction ou Numero de carte",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    montant = forms.FloatField(
        label="Montant", widget=forms.NumberInput(attrs={"class": style_class})
    )
    devise = forms.CharField(
        label="Devis",
        max_length=100,
        widget=forms.Select(choices=devise, attrs={"class": style_class}),
    )

    class Meta:
        model = PaiementCloud


#
class AddInstanceForm(forms.Form):
    state = [
        ("", "----"),
        ("stopped", "Arrêté"),
        ("started", "Démarré"),
        ("pending", "En cours de Démarrage"),
        ("open", "En cours de Construction"),
        ("revoked", "Révoqué"),
        ("error", "Erreur"),
    ]
    style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
    date_created = forms.DateField(
        label="Date de création",
        input_formats=["%d-%m-%Y"],
        widget=forms.DateInput(attrs={"class": style_class}),
    )
    date_revoked = forms.DateField(
        label="Date de révocation",
        input_formats=["%d-%m-%Y"],
        widget=forms.DateTimeInput(attrs={"class": style_class}),
    )
    state = forms.CharField(
        label="Etat", widget=forms.Select(choices=state, attrs={"class": style_class})
    )
    code = forms.CharField(
        label="Matricule",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    service = forms.ModelChoiceField(
        label="Service",
        queryset=ServiceCloud.objects.filter(enabled=True),
        widget=forms.Select(attrs={"class": style_class}),
    )
    owner = forms.ModelChoiceField(
        label="Propietaire",
        queryset=UserCloud.objects.filter(enabled=True),
        widget=forms.Select(attrs={"class": style_class}),
    )

    class Meta:
        model = InstanceCloud


#
class AddServiceForm(forms.Form):
    devis = [
        ("", "--------"),
        ("fcfa", "FCFA"),
        ("euros", "EUROS"),
        ("dollar", "Dollar"),
    ]
    style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
    # style_class_checkbox = 'form-control px-1 py-1 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline'
    style_class_checkbox = "mx-2 item-center text-center flex flex-wrap gap-4"
    name = forms.CharField(
        label="Nom",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    image = forms.ImageField(
        label="Image",
        max_length=100,
        widget=forms.FileInput(attrs={"class": style_class}),
    )
    description = forms.CharField(
        label="Description",
        max_length=100,
        widget=forms.Textarea(attrs={"class": style_class}),
    )
    price = forms.FloatField(
        label="Prix", widget=forms.NumberInput(attrs={"class": style_class})
    )
    currency = forms.CharField(
        label="Devis",
        max_length=100,
        widget=forms.Select(choices=devis, attrs={"class": style_class}),
    )
    action = forms.ModelMultipleChoiceField(
        label="Action",
        queryset=ActionCloud.objects.filter(enabled=True),
        widget=forms.CheckboxSelectMultiple(attrs={"class": style_class_checkbox}),
    )
    groupe_service = forms.ModelChoiceField(
        queryset=GroupServiceCloud.objects.filter(enabled=True),
        widget=forms.Select(attrs={"class": style_class}),
    )
    executor = forms.CharField(
        label="Exécuteur",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )

    class Meta:
        model = ServiceCloud


#
class AddGroupServiceForm(forms.Form):
    style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
    name = forms.CharField(
        label="Nom", max_length=50, widget=forms.TextInput(attrs={"class": style_class})
    )
    image = forms.ImageField(
        label="Image", widget=forms.FileInput(attrs={"class": style_class})
    )
    description = forms.CharField(
        label="Description",
        max_length=100,
        widget=forms.Textarea(attrs={"rows": 4, "class": style_class}),
    )
    info_sup = forms.CharField(
        label="Informations Supplémentaires",
        max_length=100,
        widget=forms.Textarea(attrs={"rows": 4, "class": style_class}),
    )

    class Meta:
        model = GroupServiceCloud


#
class AddCommandeForm(forms.Form):

    style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
    date_created = forms.CharField(
        label="Date de création",
        max_length=100,
        widget=forms.DateTimeInput(attrs={"class": style_class}),
    )
    price = forms.CharField(
        label="Prix",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    TVA = forms.CharField(
        label="TVA",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    total_price = forms.CharField(
        label="Prix Total",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    state = forms.CharField(
        label="Etat", max_length=100, widget=forms.Select(attrs={"class": style_class})
    )
    code = forms.CharField(
        label="Matricule",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    proprietaire = forms.CharField(
        label="Propietaire",
        max_length=100,
        widget=forms.Select(attrs={"class": style_class}),
    )

    class Meta:
        model = CommandeCloud


#
class AddActionForm(forms.Form):

    style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
    style_class_check_box = "item-center text-center mx-4 flex flex-wrap gap-4"
    name = forms.CharField(
        label="Nom",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    description = forms.CharField(
        label="Description",
        max_length=100,
        widget=forms.Textarea(attrs={"rows": 1, "class": style_class}),
    )
    executor = forms.CharField(
        label="Executeur",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    dynamic = forms.BooleanField(
        label="Dynamique",
        widget=forms.RadioSelect(
            choices=[(True, "OUI"), (False, "NON")],
            attrs={"class": style_class_check_box},
        ),
        initial=True,
    )

    class Meta:
        model = ActionCloud


#
class UserForm(forms.Form):
    country_list = sorted(COUNTRIES.items())
    country_list.insert(0, (None, "(select country)"))
    style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
    first_name = forms.CharField(
        label="First Name",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    last_name = forms.CharField(
        label="Last Name",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    email = forms.EmailField(
        label="Email",
        max_length=100,
        widget=forms.EmailInput(attrs={"class": style_class}),
    )
    phone = forms.CharField(
        label="Phone",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    address = forms.CharField(
        label="Address",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    country = forms.ChoiceField(
        label="Country",
        choices=country_list,
        required=True,
        widget=forms.Select(attrs={"class": style_class}),
    )
    #
    class Meta:
        model = UserCloud

    #
    def clean_email(self):
        email = self.data["email"]
        if (
            UserCloud.objects.filter(email=email).exists()
            or UserCloud.objects.filter(username=email).exists()
        ):
            raise forms.ValidationError("Cet email est déjà utilisé.")
        return self.data["email"]

    #
    def clean(self, *args, **kwargs):
        self.clean_email()
        return super(UserForm, self).clean(*args, **kwargs)


#
#
class AddForfaitCloudForm(forms.Form):
    style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
    durree = forms.IntegerField(
        label="Durrée",
        min_value=1,
        required=True,
        widget=forms.NumberInput(attrs={"class": style_class}),
    )
    reduction = forms.IntegerField(
        label="Réduction",
        min_value=0,
        required=True,
        widget=forms.NumberInput(attrs={"class": style_class}),
    )

    class Meta:
        model = ForfaitCloud
