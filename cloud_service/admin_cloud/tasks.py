from celery import shared_task


@shared_task
def task_runInstance(paiement_id, commande_id):
    # pylint: disable=C0415
    import contextlib
    from admin_cloud.instance_function import (
        createRemoteInstance,
        executeAction,
    )
    from commande.models import CommandeCloud
    from services.models import ServiceCloud
    from cloud_service.push_notification import notify_customer
    from time import sleep
    import asyncio
    #
    # pylint: disable=E1101
    commande = CommandeCloud.objects.get(id=commande_id)
    commande_lines = commande.commmande_ligne_commande.all()
    for commande_line in commande_lines:
        with contextlib.suppress(Exception):
            instance_has_erros = False
            # pylint: disable=E1101
            service = ServiceCloud.objects.get(id=commande_line.service.id)
            owner = commande.proprietaire
            for _ in range(commande_line.quantity):
                sleep(10)
                instance_return = createRemoteInstance(
                    service, owner, commande.code, commande_line.durree
                )
                if not instance_return:
                    continue
                for action in service.action.all():
                    return_action_value = executeAction(instance_return, action)
                    if not return_action_value:
                        instance_has_erros = True
                        break
                instance_return.state = "error" if instance_has_erros else "open"
                instance_return.save()
                asyncio.set_event_loop(asyncio.new_event_loop())
                asyncio.run(
                    notify_customer(
                        owner.id,
                        f"Votre instance {instance_return.code} est en cours de construction et sera bientôt disponible <br> Merci de patienter !!!!",  # noqa: E501
                    )
                )
    return True


#
#
@shared_task
def task_runApplication(commande_id):
    # pylint: disable=C0415
    import contextlib
    from admin_cloud.application_function import createRemoteApplication
    from commande.models import CommandeCloud
    from cloud_service.push_notification import notify_customer
    from time import sleep
    import asyncio
    #
    # pylint: disable=E1101
    commande = CommandeCloud.objects.get(id=commande_id)
    commande_app_lines = commande.commmande_ligne_app_commande.all()
    for commande_app_line in commande_app_lines:
        with contextlib.suppress(Exception):
            owner = commande.proprietaire
            for _ in range(commande_app_line.quantity):
                sleep(10)
                application_return = createRemoteApplication(
                    commande_app_line.app_name,
                    commande_app_line.package,
                    owner, commande.code,
                    commande_app_line.durree
                )
                if not application_return:
                    continue
                application_return.state = "open"
                application_return.save()
                asyncio.set_event_loop(asyncio.new_event_loop())
                asyncio.run(
                    notify_customer(
                        owner.id,
                        f"Votre application {application_return.code} est en cours de construction et sera bientôt disponible <br> Merci de patienter !!!!",  # noqa: E501
                    )
                )
    return True


#
#
@shared_task
def task_DeleteInstance(list_instancer_id):
    # pylint: disable=C0415
    from admin_cloud.instance_function import check_instance_exists, terminateInstance
    from services.models import InstanceCloud
    #
    for item in list_instancer_id:
        # pylint: disable=E1101
        if instance := InstanceCloud.objects.get(id=int(item)):
            try:
                if check_instance_exists(instance):
                    terminateInstance(instance)
                instance.delete()
            except Exception:
                continue
    return True


#
#
@shared_task
def task_DeleteApplication(list_application_id):
    # pylint: disable=C0415
    from admin_cloud.application_function import check_application_exists, terminateApplication
    from application.models import ApplicationCloud
    #
    for item in list_application_id:
        # pylint: disable=E1101
        if application := ApplicationCloud.objects.get(id=int(item)):
            try:
                if check_application_exists(application):
                    terminateApplication(application)
                application.delete()
            except Exception:
                continue
    return True


#
#
@shared_task
def task_SartInstance(list_instancer_id):
    # pylint: disable=C0415
    from admin_cloud.instance_function import check_instance_exists, startInstance
    from services.models import InstanceCloud
    #
    for item in list_instancer_id:
        # pylint: disable=E1101
        if instance := InstanceCloud.objects.get(id=int(item)):
            try:
                if check_instance_exists(instance):
                    startInstance(instance)
            except Exception:
                continue
    return True


#
#
@shared_task
def task_SartApplication(list_application_id):
    # pylint: disable=C0415
    from admin_cloud.application_function import check_application_exists, startApplication
    from application.models import ApplicationCloud
    #
    for item in list_application_id:
        # pylint: disable=E1101
        if application := ApplicationCloud.objects.get(id=int(item)):
            try:
                if check_application_exists(application):
                    startApplication(application)
            except Exception:
                continue
    return True


#
#
@shared_task
def task_StopInstance(list_instancer_id):
    # pylint: disable=C0415
    from admin_cloud.instance_function import check_instance_exists, stopInstance
    from services.models import InstanceCloud
    #
    for item in list_instancer_id:
        # pylint: disable=E1101
        if instance := InstanceCloud.objects.get(id=int(item)):
            try:
                if check_instance_exists(instance):
                    stopInstance(instance)
            except Exception:
                continue
    return True


#
#
@shared_task
def task_StopApplication(list_application_id):
    # pylint: disable=C0415
    from admin_cloud.application_function import check_application_exists, stopApplication
    from application.models import ApplicationCloud
    #
    for item in list_application_id:
        # pylint: disable=E1101
        if application := ApplicationCloud.objects.get(id=int(item)):
            try:
                if check_application_exists(application):
                    stopApplication(application)
            except Exception:
                continue
    return True


#
#
@shared_task
def task_RevokeInstance(list_instancer_id):
    # pylint: disable=C0415
    from admin_cloud.instance_function import check_instance_exists, revokedInstance
    from services.models import InstanceCloud
    #
    for item in list_instancer_id:
        # pylint: disable=E1101
        if instance := InstanceCloud.objects.get(id=int(item)):
            try:
                revokedInstance(instance, check_instance_exists(instance))
            except Exception:
                continue
    return True


#
#
@shared_task
def task_RevokeApplication(list_application_id):
    # pylint: disable=C0415
    from admin_cloud.application_function import check_application_exists, revokedApplication
    from application.models import ApplicationCloud

    #
    for item in list_application_id:
        # pylint: disable=E1101
        if application := ApplicationCloud.objects.get(id=int(item)):
            try:
                revokedApplication(application, check_application_exists(application))
            except Exception:
                continue
    return True


#
#
@shared_task
def task_Compare_local_and_remote_instance(list_instance_id):
    # pylint: disable=C0415
    from admin_cloud.instance_function import compareInstance

    compareInstance(list_instance_id)
    return True


#
#
@shared_task(name="check_revoke_instance_after_date_revoke")
def task_check_instance_revoke():
    # pylint: disable=C0415
    from admin_cloud.instance_function import checkExpireInstance
    from services.models import InstanceCloud
    from datetime import datetime
    # pylint: disable=E1101
    instances_to_revoked = InstanceCloud.objects.filter(
        date_revoked__lte=datetime.now()
    ).exclude(state="expire")
    for instance_to_revoked in instances_to_revoked:
        try:
            checkExpireInstance(instance_to_revoked)
        except Exception:
            continue
    return True


#
#
@shared_task(name="check_revoke_instance_after_date_revoke")
def task_check_application_revoke():
    # pylint: disable=C0415
    from admin_cloud.application_function import checkExpireApplication
    from application.models import ApplicationCloud
    from datetime import datetime
    # pylint: disable=E1101
    applications_to_revoked = ApplicationCloud.objects.filter(
        date_revoked__lte=datetime.now()
    ).exclude(state="expire")
    for application_to_revoked in applications_to_revoked:
        try:
            checkExpireApplication(application_to_revoked)
        except Exception:
            continue
    return True


@shared_task(name="background_task_dep_app",)
def background_task_dep_app(id_app):
    # pylint: disable=C0415
    from api_service.jenkins.job import call_jenkins_job_dep_app
    from application.models import ApplicationCloud
    import contextlib
    # pylint: disable=E1101
    with contextlib.suppress(ApplicationCloud.DoesNotExist):
        application = ApplicationCloud.objects.get(id=id_app)
        if application.ip_address and application.ssh_key:
            kwargs  = {
                "app_name": f"{application.app_name}".lower(),
                "code_app": f"{application.code}",
                "ssh_key": f"{application.ssh_key}",
                "ip_host": f"{application.ip_address}"
            }
            application.build_number_for_dep = call_jenkins_job_dep_app("application_deployer", **kwargs)
            application.save()



# @user_passes_test(lambda u: u.is_superuser,login_url='/adm/account/login',redirect_field_name='next_page') 
# def actionInstance(request):  # sourcery skip: low-code-quality
#     if 'arreter' in request.POST:
#         select_items = request.POST.getlist('select_items')
#         for item in select_items:
#             if instance := InstanceCloud.objects.get(id=int(item)):
#                 try:
#                     if instance.state and instance.state=='revoked':
#                         messages.warning(request, f"Instance {instance.code}: est révoquée , donc arrêtée !!")
#                     elif instance.state and instance.state=='stopped':
#                         messages.warning(request, f"Instance {instance.code}: est déja arrêtée")
#                     elif check_instance := check_instance_exists(instance):
#                         if info_return := stopInstance(instance):
#                             messages.info(request, f"Instance {instance.code}: est bien arrêtée")
#                         else:
#                             messages.error(request, f"Instance {instance.code}: Une erreur c'est produite")
#                     else:
#                         messages.error(request, f"Remote ID {instance.remote_id}: ne correspond à aucun serveur distant, veillez effectuer un rapprochement")
#                 except BotoClientError as error :
#                     messages.error(request, f"Remote ID {instance.remote_id}: Une erreur c'est produite, Veillez vérifier que l'action a correctement aboutie ,Un rapproche peut être nécessaire !!")
#                 except Exception as error :
#                     messages.error(request, f"Instance {instance.code}: Une erreur c'est produite, Un rapproche peut être nécessaire !!")
#     elif 'demarrer' in request.POST:
#         select_items = request.POST.getlist('select_items')
#         for item in select_items:
#             if instance := InstanceCloud.objects.get(id=int(item)):
#                 try:
#                     if instance.state and instance.state=='started':
#                         messages.warning(request, f"Instance {instance.code}: est déja démarrée")
#                     elif instance.state and instance.state=='pending':
#                         messages.warning(request, f"Instance {instance.code}: est en cours de démarrage Veiller patienter !!")
#                     elif check_instance := check_instance_exists(instance):
#                         if info_return := startInstance(instance):
#                             messages.info(request, f"Instance {instance.code}: est bien démarrée")
#                         else:
#                             messages.error(request, f"Instance {instance.code}: Une erreur c'est produite")
#                     else:
#                         messages.error(request, f"Remote ID {instance.remote_id}: ne correspond à aucun serveur distant, veillez effectuer un rapprochement")
#                 except BotoClientError as error :
#                     messages.error(request, f"Remote ID {instance.remote_id}: Une erreur c'est produite, Veillez vérifier que l'action a correctement aboutie ,Un rapproche peut être nécessaire !!")
#                 except Exception as error :
#                     messages.error(request, f"Instance {instance.code}: Une erreur c'est produite, Un rapproche peut être nécessaire !!")
#     elif 'supprimer' in request.POST:
#         select_items = request.POST.getlist('select_items')
#         for item in select_items:
#             if instance := InstanceCloud.objects.get(id=int(item)):
#                 try:
#                     if check_instance := check_instance_exists(instance):
#                         info_return = terminateInstance(instance)
#                         if not info_return:
#                             messages.warning(request, f"Remote ID {instance.remote_id}: : Une erreur s'est produite lors de la suppression du serveur distant, Veillez vérifier si sa suppression a bien réussie")
#                     else:
#                         messages.warning(request, f"Remote ID {instance.remote_id}: ne correspond à aucun serveur distant, veillez effectuer un rapprochement")
#                     instance.delete()
#                     messages.info(request, f"Instance {instance.code}: est bien supprimée")
#                 except BotoClientError as error :
#                     messages.error(request, f"Remote ID {instance.remote_id}: Une erreur c'est produite, Veillez vérifier que l'action a correctement aboutie ,Un rapproche peut être nécessaire !!")
#                 except Exception as error :
#                     messages.error(request, f"Instance {instance.code}: Une erreur c'est produite, Un rapproche peut être nécessaire !!")
#     elif 'resilier' in request.POST:
#         select_items = request.POST.getlist('select_items')
#         for item in select_items:
#             if instance := InstanceCloud.objects.get(id=int(item)):
#                 try:
#                     if instance.state and instance.state=='revoked':
#                         messages.warning(request, f"Instance {instance.code}: est déja révoquée")
#                     else:
#                         if not (check_instance := check_instance_exists(instance)):
#                             messages.error(request, f"Remote ID {instance.remote_id}: ne correspond à aucun serveur distant, veillez effectuer un rapprochement")
#                         if info_return := revokedInstance(instance,check_instance):
#                             messages.info(request, f"Instance {instance.code}: est bien révoquée")
#                         else:
#                             messages.error(request, f"Instance {instance.code}: Une erreur c'est produite")
                        
#                 except BotoClientError as error :
#                     messages.error(request, f"Remote ID {instance.remote_id}: Une erreur c'est produite, Veillez vérifier que l'action a correctement aboutie ,Un rapproche peut être nécessaire !!")
#                 except Exception as error :
#                     messages.error(request, f"Instance {instance.code}: Une erreur c'est produite, Un rapproche peut être nécessaire !!")
#         #
#     elif 'compare' in request.POST:
#         compareInstance(request)
#     return HttpResponseRedirect('/adm/account/list/instance/')
