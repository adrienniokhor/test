from datetime import datetime

from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.contrib import auth, messages
from django.contrib.auth import get_user_model

from analytics.models import Event
from groupe_service.views import validerPanier
from user_cloud.models import UserCloud



def login(request):
    UserModel = get_user_model()
    if request.user.is_authenticated:
        if request.GET.get("next_page"):
            return redirect(request.GET.get("next_page"))
        return redirect("account")
    elif request.method == "POST":
        email = request.POST.get("email")
        password = request.POST.get("password")
        try:
            if user := UserModel.objects.get(email=email):
                if not user.is_active:
                    messages.error(
                        request,
                        f"Votre compte n'est pas actif , un e-mail de confirmation a été envoyé sur cet email { user.email } pour la finalisation l'inscription , veillez vérifier votre boite de réception .Remarque : Vérifiez votre dossier spam.",
                    )
                    return logout_force(request)
                if user := auth.authenticate(username=user.username, password=password):
                    auth.login(request, user)
                    user_connect = UserCloud.objects.get(id=request.user.id)
                    proprietaire = user_connect
                    login_user = Event(
                        name="Logged In",
                        object="user",
                        code=proprietaire.code,
                        created=datetime.now(),
                    )
                    login_user.save()
                    if request.GET.get("next_page"):
                        if (
                            request.GET.get("next_page")
                            == "/groupe_service/panier_services"
                        ):
                            return validerPanier(request)     
                        else:
                            return redirect(request.GET.get("next_page"))
                    elif request.GET.get("next"):
                        return redirect(request.GET.get("next"))
                    return redirect("account")
                #
            return logout_force(request)
        except UserModel.DoesNotExist:
            return logout_force(request)
        except Exception:
            return logout_force(request)
    else:
        return logout_force(request)
#


def logout(request):
    if request.user and request.user.id:
        if user_connect := UserCloud.objects.filter(id=request.user.id):
            logout_user = Event(
                name="Logged Out",
                object="user",
                code=user_connect[0].code,
                created=datetime.now(),
            )
            logout_user.save()
    panier = request.session["panier"] if "panier" in request.session else {}
    panier_application = (
        {}
        if "panier_application" not in request.session
        else request.session["panier_application"]
    )
    auth.logout(request)
    request.session["panier"] = panier
    request.session["panier_application"] = panier_application
    return HttpResponseRedirect("/")


def logout_force(request):
    panier = request.session["panier"] if "panier" in request.session else {}
    panier_application = (
        {}
        if "panier_application" not in request.session
        else request.session["panier_application"]
    )
    auth.logout(request)
    request.session["panier"] = panier
    request.session["panier_application"] = panier_application
    return render(
        request, "login.html", context={"next_page": request.GET.get("next_page")}
    )
