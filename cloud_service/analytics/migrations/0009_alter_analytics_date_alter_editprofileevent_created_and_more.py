# Generated by Django 4.0.4 on 2022-08-11 13:06

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        (
            "analytics",
            "0008_alter_analytics_date_alter_editprofileevent_created_and_more",
        ),
    ]

    operations = [
        migrations.AlterField(
            model_name="analytics",
            name="date",
            field=models.DateTimeField(
                default=datetime.datetime(2022, 8, 11, 13, 6, 31, 242219)
            ),
        ),
        migrations.AlterField(
            model_name="editprofileevent",
            name="created",
            field=models.DateTimeField(
                default=datetime.datetime(2022, 8, 11, 13, 6, 31, 244361)
            ),
        ),
        migrations.AlterField(
            model_name="event",
            name="created",
            field=models.DateTimeField(
                default=datetime.datetime(2022, 8, 11, 13, 6, 31, 242696)
            ),
        ),
        migrations.AlterField(
            model_name="loginevent",
            name="created",
            field=models.DateTimeField(
                default=datetime.datetime(2022, 8, 11, 13, 6, 31, 243486)
            ),
        ),
        migrations.AlterField(
            model_name="logoutevent",
            name="created",
            field=models.DateTimeField(
                default=datetime.datetime(2022, 8, 11, 13, 6, 31, 244753)
            ),
        ),
        migrations.AlterField(
            model_name="registerevent",
            name="created",
            field=models.DateTimeField(
                default=datetime.datetime(2022, 8, 11, 13, 6, 31, 243112)
            ),
        ),
        migrations.AlterField(
            model_name="viewpageevent",
            name="created",
            field=models.DateTimeField(
                default=datetime.datetime(2022, 8, 11, 13, 6, 31, 243883)
            ),
        ),
    ]
