from django.db import models
import datetime

# Create your models here.
class Analytics(models.Model):
    name = name = models.CharField(max_length=40)
    stats = models.IntegerField(default=0)
    date = models.DateTimeField(auto_now_add=True)


class Event(models.Model):
    name = models.CharField(max_length=40)
    code = models.CharField(max_length=40)
    object = models.CharField(max_length=40)
    ip = models.CharField(max_length=40)
    session = models.CharField(max_length=40)
    created = models.DateTimeField(auto_now_add=True)


class RegisterEvent(models.Model):
    usercode = models.CharField(max_length=40)
    created = models.DateTimeField(auto_now_add=True)


class LoginEvent(models.Model):
    usercode = models.CharField(max_length=40)
    created = models.DateTimeField(auto_now_add=True)


class ViewPageEvent(models.Model):
    ip = models.CharField(max_length=40)
    session = models.CharField(max_length=40)
    created = models.DateTimeField(auto_now_add=True)


class EditProfileEvent(models.Model):
    usercode = models.TextField()
    created = models.DateTimeField(auto_now_add=True)


class LogoutEvent(models.Model):
    usercode = models.CharField(max_length=40)
    created = models.DateTimeField(auto_now_add=True)
