import datetime
import re
from django import forms
from django.shortcuts import redirect, render
from django_countries.data import COUNTRIES

from user_cloud.models import UserCloud
from analytics.models import Event

from django.http import HttpResponse
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_str
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import get_template
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.core.mail import EmailMessage
from .password_token_generator import account_activation_token
from django.contrib import messages
from django_countries import countries

# Create your views here.
def register(request):
    if request.method == "POST":
        form = RegisterUserForm(request.POST)
        if not form.is_valid():
            return render(
                request,
                "register.html",
                context={"form": form, "next_page": request.GET.get("next_page")},
            )
        data = form.cleaned_data
        user = UserCloud.objects.create_user(
            first_name=data["first_name"],
            last_name=data["last_name"],
            phone=data["phone"],
            address=data["address"],
            country=data["country"],
            password=data["password"],
            email=data["email"],
            username=data["email"],
        )
        user.save()
        register_user = Event(
            name="Registered",
            object="user",
            code=user.code,
            created=datetime.datetime.now(),
        )
        register_user.save()
        #
        user.is_active = False  # Deactivate account till it is confirmed
        user.save()
        current_site = get_current_site(request)
        # domain=current_site.domain
        domain = "coridigital.com"
        mail_subject = "Activez votre compte sur cori digital"
        message = get_template("account_activation_email.html").render(
            {
                "user": user,
                "domain": domain,
                "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                "token": account_activation_token.make_token(user),
                "next_page": request.GET.get("next_page") or "",
                "protocol": "https" if request.is_secure() else "http",
            }
        )

        to_email = user.email
        email = EmailMessage(mail_subject, message, to=[to_email])
        email.content_subtype = "html"
        if email.send():
            messages.success(
                request,
                (
                    f"Cher {user.first_name} {user.last_name}, veuillez vous rendre dans votre boîte de réception e-mail {user.email} et cliquez sur \
            le lien d'activation reçu pour confirmer et terminer l'inscription. Remarque : Vérifiez votre dossier spam."
                ),
            )
        else:
            messages.error(
                request,
                f"Problème d'envoi de l'e-mail de confirmation à {to_email}, vérifiez si vous l'avez saisi correctement.",
            )
        #
        if request.GET.get("next_page"):
            return redirect("/user/login?next_page=" + request.GET.get("next_page"))
        return redirect("/user/login")
    form = RegisterUserForm()
    return render(
        request,
        "register.html",
        context={"form": form, "next_page": request.GET.get("next_page")},
    )


#
def activate_account(request, uidb64, token):
    User = get_user_model()
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        messages.success(
            request,
            (
                "Merci pour votre e-mail de confirmation. Vous pouvez maintenant vous connecter à votre compte."
            ),
        )
    else:
        messages.warning(
            request,
            (
                "Le lien de confirmation n'était pas valide, peut-être parce qu'il a déjà été utilisé."
            ),
        )
    #
    if request.GET.get("next_page"):
        return redirect("/user/login?next_page=" + request.GET.get("next_page"))
    return redirect("/user/login")


#
class RegisterUserForm(forms.Form):
    country_list = list(countries)
    country_list.insert(0, (None, "(choisissez votre pays)"))
    style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
    first_name = forms.CharField(
        label="First Name",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    last_name = forms.CharField(
        label="Last Name",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    email = forms.EmailField(
        label="Email",
        max_length=100,
        widget=forms.EmailInput(attrs={"class": style_class}),
    )
    phone = forms.CharField(
        label="Phone",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    address = forms.CharField(
        label="Address",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    country = forms.ChoiceField(
        label="Country",
        choices=country_list,
        required=True,
        widget=forms.Select(attrs={"class": style_class}),
    )
    password = forms.CharField(
        label="Password",
        max_length=100,
        widget=forms.PasswordInput(attrs={"class": style_class}),
    )
    password_confirm = forms.CharField(
        label="Confirmed Password",
        max_length=100,
        widget=forms.PasswordInput(attrs={"class": style_class}),
    )
    #
    class Meta:
        model = UserCloud

    #
    def clean_password(self):
        if self.data["password"] != self.data["password_confirm"]:
            raise forms.ValidationError(
                "Votre mot de passe et votre confirmation de mot de passe ne correspondent pas."
            )
        return self.data["password"]

    #
    def clean_email(self):
        email = self.data["email"]
        if (
            UserCloud.objects.filter(email=email).exists()
            or UserCloud.objects.filter(username=email).exists()
        ):
            raise forms.ValidationError("Cet email est déjà utilisé.")
        return self.data["email"]

    #
    def clean(self, *args, **kwargs):
        self.clean_password()
        self.clean_email()
        return super(RegisterUserForm, self).clean(*args, **kwargs)
