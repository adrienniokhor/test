from django.contrib import admin

from commande.models import (
    CommandeCloud,
    LigneCommandeCloud,
    ModePaiementCloud,
    PaiementCloud,
)

# Register your models here.
admin.site.register(CommandeCloud)
admin.site.register(LigneCommandeCloud)
admin.site.register(PaiementCloud)
admin.site.register(ModePaiementCloud)
