from dataclasses import field
import datetime
import django
from django.db import models
from django.forms import ModelForm
from django.urls import reverse
from computedfields.models import ComputedFieldsModel, computed, compute

from services.models import ServiceCloud
from user_cloud.models import UserCloud

# Create your models here.

state = [
    ("open", "Ouvert"),
    ("cancel", "Annulé"),
    ("validat", "Validé"),
    ("paid", "Payé"),
]
state_paiement = [
    ("open", "Ouvert"),
    ("validat", "Validé"),
    ("reject", "Rejeté"),
    ("cancel", "Annulé"),
]
currency = [("fcfa", "FCFA")]

#
class CommandeCloud(ComputedFieldsModel):
    date_created = models.DateTimeField("Date de création", auto_now_add=True)
    state = models.CharField(
        "Etat", max_length=60, null=False, blank=True, choices=state, default="open"
    )
    price = models.FloatField("Montant", null=False, blank=True)
    TVA = models.FloatField("TVA", null=False, blank=True)
    total_price = models.FloatField("Prix Total", null=False, blank=True)
    proprietaire = models.ForeignKey(
        UserCloud,
        on_delete=models.CASCADE,
        related_name="commande_proprietaire",
        null=True,
        blank=True,
    )

    @computed(
        models.CharField("Code", max_length=60, null=False, blank=True),
        depends=[("self", ["id"])],
    )
    def code(self):
        seq = f"00000000{str(self.id)}"
        seq_finale = seq[-8:]
        return f'COMM_{str(datetime.date.today().strftime("%Y"))}_{seq_finale}'

    #

    def __str__(self):
        return self.code


#
class LigneCommandeCloud(models.Model):
    quantity = models.IntegerField("Nombre", null=False, blank=True)
    price = models.FloatField("Prix Unitaire", null=False, blank=True, default=0)
    total_price = models.FloatField("Prix Total", null=False, blank=True)
    service = models.ForeignKey(
        ServiceCloud, on_delete=models.CASCADE, related_name="service_ligne_commande"
    )
    commande = models.ForeignKey(
        CommandeCloud, on_delete=models.CASCADE, related_name="commmande_ligne_commande"
    )
    durree = models.PositiveSmallIntegerField(
        "Durrée", null=False, blank=False, default=1
    )
    reduction = models.PositiveSmallIntegerField(
        "Réduction", null=False, blank=False, default=0
    )


#
class LigneCommandeAppCloud(models.Model):
    quantity = models.IntegerField("Nombre", null=False, blank=True)
    price = models.FloatField("Prix Unitaire", null=False, blank=True, default=0)
    total_price = models.FloatField("Prix Total", null=False, blank=True, default=0)
    code = models.CharField("Code", max_length=200, null=True, blank=True)
    app_name = models.CharField("Application", max_length=100, null=True, blank=True)
    package = models.CharField("Package", max_length=100, null=True, blank=True)
    commande = models.ForeignKey(
        CommandeCloud,
        on_delete=models.CASCADE,
        related_name="commmande_ligne_app_commande",
    )
    durree = models.PositiveSmallIntegerField(
        "Durrée", null=False, blank=False, default=1
    )
    reduction = models.PositiveSmallIntegerField(
        "Réduction", null=False, blank=False, default=0
    )


#
class ModePaiementCloud(ComputedFieldsModel):
    name = models.CharField("Nom", max_length=60, null=False, blank=True, unique=True)
    description = models.TextField("Description")
    enabled = models.BooleanField("Statut", default=True)

    @computed(
        models.CharField("Code", max_length=60, null=False, blank=True),
        depends=[("self", ["id"])],
    )
    def code(self):
        seq = f"00000000{str(self.id)}"
        seq_finale = seq[-8:]
        return f'MPAI_{str(datetime.date.today().strftime("%Y"))}_{seq_finale}'

    #
    def __str__(self):
        return f"{self.name}"

    #


#
class PaiementCloud(ComputedFieldsModel):
    date_created = models.DateTimeField("Date de Paiement", auto_now_add=True)
    date_paiement = models.DateField("Date Paiement", default=django.utils.timezone.now)
    date_valid = models.DateTimeField("Date Validation Paiement", null=True, blank=True)
    date_denial = models.DateTimeField("Date Rejet Paiement", null=True, blank=True)
    date_cancel = models.DateTimeField(
        "Date Annulation Paiement", null=True, blank=True
    )
    commande = models.ForeignKey(
        CommandeCloud,
        on_delete=models.CASCADE,
        null=False,
        blank=True,
        related_name="commande_paiement",
    )
    mode_paiement = models.ForeignKey(
        ModePaiementCloud,
        on_delete=models.SET_NULL,
        related_name="paiement_mode_paiement",
        null=True,
    )
    numTransaction_or_numCarte = models.CharField(
        "Numéro Transsaction ou Numero de carte", max_length=60, null=True, blank=True
    )
    state = models.CharField(
        "Status",
        max_length=60,
        null=False,
        blank=True,
        choices=state_paiement,
        default="open",
    )
    proprietaire = models.ForeignKey(
        UserCloud,
        on_delete=models.CASCADE,
        related_name="paiement_proprietaire",
        null=True,
        blank=True,
    )
    montant = models.FloatField("Montant", null=False, blank=True, default=0.0)
    devise = models.CharField(
        "Devis", max_length=60, null=False, blank=True, choices=currency, default="fcfa"
    )

    @computed(
        models.CharField("Code", max_length=60, null=False, blank=True),
        depends=[("self", ["id"])],
    )
    def code(self):
        seq = f"00000000{str(self.id)}"
        seq_finale = seq[-8:]
        return f'PAIE_{str(datetime.date.today().strftime("%Y"))}_{seq_finale}'

    #
    def __str__(self):
        return self.code

    #
    def get_absolute_url(self):
        if self.code:
            return f"/adm/account/list/paiement/?code={self.code}"
        return reverse("admin_paiement")


#
# #**************************************************************************************
#
class CommandeCloudForm(ModelForm):
    class Meta:
        model = CommandeCloud
        fields = ["state", "price", "TVA", "total_price"]


#
class LigneCommandeCoudForm(ModelForm):
    class Meta:
        model = LigneCommandeCloud
        fields = ["quantity", "total_price", "service", "commande"]


#


class PaiementCloudForm(ModelForm):
    class Meta:
        model = PaiementCloud
        fields = [
            "commande",
            "mode_paiement",
            "numTransaction_or_numCarte",
            "montant",
            "devise",
            "date_paiement",
            "state",
            "proprietaire",
        ]


#
class ModePaiementCloudCloudForm(ModelForm):
    class Meta:
        model = ModePaiementCloud
        fields = ["name", "description"]
