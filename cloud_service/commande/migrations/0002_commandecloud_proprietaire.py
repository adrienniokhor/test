# Generated by Django 4.0.2 on 2022-03-26 14:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("user_cloud", "0002_groupcloud_rolecloud_usercloud_delete_usercoud_and_more"),
        ("commande", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="commandecloud",
            name="proprietaire",
            field=models.ForeignKey(
                blank=True,
                editable=False,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="commande_proprietaire",
                to="user_cloud.usercloud",
            ),
        ),
    ]
