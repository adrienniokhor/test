# Generated by Django 4.0.2 on 2022-03-18 20:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ("services", "0004_alter_instanceactionvaluecloud_action_and_more"),
    ]

    operations = [
        migrations.CreateModel(
            name="CommandeCloud",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "code",
                    models.CharField(blank=True, max_length=60, verbose_name="Code"),
                ),
                (
                    "date_created",
                    models.DateTimeField(
                        auto_now_add=True, verbose_name="Date de création"
                    ),
                ),
                (
                    "state",
                    models.CharField(
                        blank=True,
                        choices=[
                            ("drapt", "Brouillon"),
                            ("open", "Ouvert"),
                            ("paid", "Payé"),
                            ("cancel", "Annulé"),
                        ],
                        default="drapt",
                        max_length=60,
                        verbose_name="Etat",
                    ),
                ),
                ("price", models.FloatField(blank=True, verbose_name="Montant")),
                ("TVA", models.FloatField(blank=True, verbose_name="TVA")),
                (
                    "total_price",
                    models.FloatField(blank=True, verbose_name="Prix Total"),
                ),
            ],
        ),
        migrations.CreateModel(
            name="ModePaiementCloud",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "name",
                    models.CharField(blank=True, max_length=60, verbose_name="Code"),
                ),
                (
                    "code",
                    models.CharField(blank=True, max_length=60, verbose_name="Code"),
                ),
                ("description", models.TextField(verbose_name="Description")),
            ],
        ),
        migrations.CreateModel(
            name="PaiementCloud",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "code",
                    models.CharField(blank=True, max_length=60, verbose_name="Code"),
                ),
                (
                    "date_created",
                    models.DateTimeField(
                        auto_now_add=True, verbose_name="Date de Paiement"
                    ),
                ),
                (
                    "commande",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="commande.commandecloud",
                    ),
                ),
                (
                    "mode_paiement",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="paiement_mode_paiement",
                        to="commande.modepaiementcloud",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="LigneCommandeCloud",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("quantity", models.IntegerField(blank=True, verbose_name="Nombre")),
                (
                    "total_price",
                    models.FloatField(blank=True, verbose_name="Prix Total"),
                ),
                (
                    "commande",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="commmande_ligne_commande",
                        to="commande.commandecloud",
                    ),
                ),
                (
                    "service",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="service_ligne_commande",
                        to="services.servicecloud",
                    ),
                ),
            ],
        ),
    ]
