# Generated by Django 4.0.4 on 2022-08-14 23:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("commande", "0013_lignecommandecloud_durree_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="paiementcloud",
            name="date_cancel",
            field=models.DateTimeField(
                blank=True, null=True, verbose_name="Date Annulation Paiement"
            ),
        ),
        migrations.AddField(
            model_name="paiementcloud",
            name="date_denial",
            field=models.DateTimeField(
                blank=True, null=True, verbose_name="Date Rejet Paiement"
            ),
        ),
        migrations.AddField(
            model_name="paiementcloud",
            name="date_valid",
            field=models.DateTimeField(
                blank=True, null=True, verbose_name="Date Validation Paiement"
            ),
        ),
    ]
