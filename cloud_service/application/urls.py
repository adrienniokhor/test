"""cloud_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from application.views.user.app_view import (
    action_application as user_action_application,
    get_application as user_get_application,
)
from application.views.admin.app_view import (
    action_application as admin_action_application,
    get_application as admin_get_application,
    add_application as admin_add_application,
)


urlpatterns = [
    # * User URL
    path("user/account/application", user_get_application, name="user_application"),
    # Definition des urls des Actions
    path(
        "user/account/action/application/",
        user_action_application,
        name="user_action_application",
    ),
    # * Admin URL
    path(
        "adm/account/list/application/", admin_get_application, name="admin_application"
    ),
    path(
        "adm/account/action/application/",
        admin_action_application,
        name="admin_application_ation",
    ),
    path("adm/account/add/application/", admin_add_application, name="admin_add_application"),
    # path(
    #     "adm/account/update/application/<str:pk>",
    #     update_application.as_view(),
    #     name="update_application",
    # ),
]
