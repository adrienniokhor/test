from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.core.paginator import Paginator
from django.contrib import messages  # import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from admin_cloud.tasks import task_SartApplication, task_StopApplication

from cloud_service.settings import PAGINATION_DEFAULT_PAGINATION

from user_cloud.models import UserCloud

#
MSG_NOT_FOUND_APP = "Aucune application trouvée"
LOGIN_USER_URL = "/user/login"


@login_required(login_url=LOGIN_USER_URL)
def get_application(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(LOGIN_USER_URL)
    if not (user_connect := UserCloud.objects.get(id=request.user.id)):
        return HttpResponseRedirect(LOGIN_USER_URL)
    if code := request.GET.get("code"):
        try:
            if app := user_connect.app_proprietaire.get(code=code):
                return render(
                    request,
                    "account.html",
                    context={
                        "application_detail": app,
                        "object": "application",
                        "action": "list",
                    },
                )
            else:
                messages.warning(request, MSG_NOT_FOUND_APP)
        except ObjectDoesNotExist:
            messages.warning(request, MSG_NOT_FOUND_APP)
    #
    applications = user_connect.app_proprietaire.all().order_by("-id")
    pages = Paginator(applications, PAGINATION_DEFAULT_PAGINATION)
    if page_number := request.GET.get("page"):
        page_number = int(page_number)
        if page_number > pages.num_pages or page_number < 1:
            page_number = 1
        page = pages.page(page_number)
    else:
        page = pages.page(1)
    return render(
        request,
        "account.html",
        context={"page": page, "object": "application", "action": "list"},
    )


#
@login_required(login_url=LOGIN_USER_URL)
def action_application(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(LOGIN_USER_URL)
    if not (user_connect := UserCloud.objects.get(id=request.user.id)):
        return HttpResponseRedirect(LOGIN_USER_URL)
    #
    select_items = request.POST.getlist("select_items")
    if "arreter" in request.POST:
        list_app_id = []
        for item in select_items:
            try:
                if application := user_connect.app_proprietaire.get(id=int(item)):
                    if application.state and application.state == "revoked":
                        messages.warning(
                            request,
                            f"Application {application.code }: est révoquée , donc arrêtée !!",
                        )
                    elif application.state and application.state == "stopped":
                        messages.warning(
                            request, f"Application {application.code }: est déjà arrêtée"
                        )
                    else:
                        application.state = "stopping"
                        application.save()
                        list_app_id.append(item)
                        messages.info(
                            request, f"Application {application.code }: est en cours d'arrêt"
                        )
                else:
                    messages.warning(request, MSG_NOT_FOUND_APP)
            except ObjectDoesNotExist:
                messages.warning(request, MSG_NOT_FOUND_APP)
                continue
            #
            task_StopApplication.delay(select_items)
    #
    elif "demarrer" in request.POST:
        list_app_id = []
        for item in select_items:
            try:
                if application := user_connect.app_proprietaire.get(id=int(item)):
                    if application.state and application.state == "started":
                        messages.warning(
                            request, f"Application {application.code}: est déjà démarrée"
                        )
                    elif application.state and application.state == "pending":
                        messages.warning(
                            request,
                            f"Application {application.code}: est en cours de démarrage veiller patienter !!",  # noqa: E501
                        )
                    else:
                        application.state = "pending"
                        application.save()
                        list_app_id.append(item)
                        messages.info(
                            request,
                            f"Application {application.code}: est en cours de démarrage",
                        )
                #
                else:
                    messages.warning(request, MSG_NOT_FOUND_APP)
            except ObjectDoesNotExist:
                messages.warning(request, MSG_NOT_FOUND_APP)
                continue
            #
            task_SartApplication.delay(select_items)
    #
    return HttpResponseRedirect("/user/account/instance")


#
# @login_required
# def get_instance_access_key(request, code=None, filename=None):
#     if not request.user.is_authenticated:
#         return HttpResponseRedirect(LOGIN_USER_URL)
#     if not (user_connect := UserCloud.objects.get(id=request.user.id)):
#         return HttpResponseRedirect(LOGIN_USER_URL)
#     if not filename:
#         return HttpResponseRedirect(LOGIN_USER_URL)
#     filepath = os.path.join(KEYFILES_DIRS, filename)
#     try:
#         if instance_action_value := InstanceActionValueCloud.objects.get(code=code):
#             user_connect.app_proprietaire.get(
#                 id=int(instance_action_value.instance.id)
#             )
#             if not os.path.exists(filepath):
#                 with open(filepath, "w", encoding="utf-8") as f:
#                     f.write(instance_action_value.value)
#             else:
#                 with open(filepath, "r", encoding="utf-8") as file:
#                     if file.read() != instance_action_value.value:
#                         with open(filepath, "w", encoding="utf-8") as f:
#                             f.write(instance_action_value.value)
#             path = open(filepath, "r", encoding="utf-8")
#             mime_type, _ = mimetypes.guess_type(filepath)
#             response = HttpResponse(path, content_type=mime_type)
#             response["Content-Disposition"] = f"attachment; filename={filename}"
#             return response
#     except Exception:
#         file_content = io.StringIO("----*********----")
#         mime_type, _ = mimetypes.guess_type(filepath)
#         response = HttpResponse(file_content, content_type=mime_type)
#         response["Content-Disposition"] = f"attachment; filename={filename}"

