import datetime

from botocore.exceptions import ClientError as BotoClientError
from django.contrib import messages
from django.contrib.auth.decorators import user_passes_test
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import render
from admin_cloud.tasks import (
    task_DeleteApplication,
    task_RevokeApplication,
    task_SartApplication,
    task_StopApplication
)

from analytics.models import Event
from application.form import ApplicationForm
from application.models import ApplicationCloud
from cloud_service.settings import PAGINATION_DEFAULT_PAGINATION


@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def add_application(request):
    if request.method == "POST":
        form_app = ApplicationForm(request.POST)
        if not form_app.is_valid():
            return render(
                request,
                "admin_account.html",
                context={
                    "form": form_app,
                    "object": "application",
                    "action": "add",
                },
            )
        data = form_app.cleaned_data
        # pylint: disable=E1101
        application = ApplicationCloud.objects.create(**data)
        application.save()
        create_application_event = Event(
            name="Create Application",
            object="application",
            code=application.code,
            created=datetime.datetime.now(),
        )
        create_application_event.save()
        return HttpResponseRedirect("/adm/account/list/application/")
    form_app = ApplicationForm()
    return render(
        request,
        "admin_account.html",
        context={"form": form_app, "object": "application", "action": "add"},
    )


#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def action_application(request):  # sourcery skip: low-code-quality
    #
    try:
        #
        if "arreter" in request.POST:
            select_items = request.POST.getlist("select_items")
            # pylint: disable=E1101
            for app in ApplicationCloud.objects.filter(
                id__in=[int(item) for item in select_items]
            ):
                if app.state and app.state == "revoked":
                    messages.warning(
                        request,
                        f"Application {app.code}: est révoquée , donc arrêtée !!",
                    )
                elif app.state and app.state == "stopped":
                    messages.warning(
                        request, f"Application {app.code}: est déja arrêtée"
                    )
                else:
                    app.state = "stopping"
                    app.save()
                    messages.info(
                        request, f"Application {app.code}: est en cours d'arrêt"
                    )
                    #
                    app.state = "stopped"
                    app.save()
                    messages.info(request, f"Application {app.code}: est bien arrêtée")
            #
            task_StopApplication.delay(select_items)
        #
        elif "demarrer" in request.POST:
            select_items = request.POST.getlist("select_items")
            # pylint: disable=E1101
            for app in ApplicationCloud.objects.filter(
                id__in=[int(item) for item in select_items]
            ):
                if app.state and app.state == "started":
                    messages.warning(
                        request, f"Application {app.code}: est déja démarrée"
                    )
                elif app.state and app.state == "pending":
                    messages.warning(
                        request,
                        f"Apllication {app.code}: est en cours de démarrage Veiller patienter !!",
                    )
                else:
                    app.state = "pending"
                    app.save()
                    messages.info(
                        request,
                        f"Application {app.code}: est en cours de démarrage",
                    )
                    app.state = "started"
                    app.save()
                    messages.info(
                        request,
                        f"Application {app.code}: est bien démarrée",
                    )
            #
            task_SartApplication.delay(select_items)
        #
        elif "supprimer" in request.POST:
            select_items = request.POST.getlist("select_items")
            # pylint: disable=E1101
            for app in ApplicationCloud.objects.filter(
                id__in=[int(item) for item in select_items]
            ):
                app.state = "deleting"
                app.save()
                messages.info(request, f"Application {app.code}: est bien supprimée")
                app.delete()
            #
            task_DeleteApplication.delay(select_items)
        #
        elif "resilier" in request.POST:
            select_items = request.POST.getlist("select_items")
            # pylint: disable=E1101
            for app in ApplicationCloud.objects.filter(
                id__in=[int(item) for item in select_items]
            ):
                if app.state and app.state == "revoked":
                    messages.warning(
                        request, f"Application {app.code}: est déja révoquée"
                    )
                else:
                    app.state = "revoking"
                    app.save()
                    messages.info(request, f"Application {app.code}: est bien révoquée")
                    app.state = "revoked"
                    app.save()
                    messages.warning(
                        request, f"Application {app.code}: est bien révoquée"
                    )
            #
            task_RevokeApplication.delay(select_items)
        #
        # elif "compare" in request.POST:
        #     messages.info(request, "Le Rapprochement est en cours")
        #     list_instance_id = request.POST.getlist("select_items")
        #     task_Compare_local_and_remote_instance.delay(list_instance_id)
    except BotoClientError:
        messages.error(
            request,
            "Une erreur s'est produite, veuillez vérifier que l'action a correctement abouti ,un rapproche peut-être  nécessaire !!",  # noqa: E501
        )
    except Exception:
        messages.error(
            request, "Une erreur s'est produite, un rapproche peut être nécessaire !!"
        )
    return HttpResponseRedirect("/adm/account/list/application/")


#
#
@user_passes_test(
    lambda u: u.is_superuser,
    login_url="/adm/account/login",
    redirect_field_name="next_page",
)
def get_application(request):
    if code := request.GET.get("code"):
        try:
            # pylint: disable=E1101
            app = ApplicationCloud.objects.get(code=code)
            return render(
                request,
                "admin_account.html",
                context={
                    "application_detail": app,
                    "object": "application",
                    "action": "list",
                },
            )
        except ApplicationCloud.DoesNotExist:
            messages.warning(request, "Aucune application correspondante trouvée")
    # pylint: disable=E1101
    apps = ApplicationCloud.objects.all().order_by("-id")
    pages = Paginator(apps, PAGINATION_DEFAULT_PAGINATION)
    if page_number := request.GET.get("page"):
        page_number = int(page_number)
        if page_number > pages.num_pages or page_number < 1:
            page_number = 1
        page = pages.page(page_number)
    else:
        page = pages.page(1)
    return render(
        request,
        "admin_account.html",
        context={"page": page, "object": "application", "action": "list"},
    )


#
#
# @user_passes_test(
#     lambda u: u.is_superuser,
#     login_url="/adm/account/login",
#     redirect_field_name="next_page",
# )
# def compareInstance(request):
#     messages.info(request, "Le Rapprochement est en cours")
#     task_Compare_local_and_remote_instance.delay(request)


# #
# #
# @user_passes_test(
#     lambda u: u.is_superuser,
#     login_url="/adm/account/login",
#     redirect_field_name="next_page",
# )
# def getInstanceFile(request, code=None, filename=None):
#     if not filename:
#         return render(request, "admin.html")
#     if instance_action_value := InstanceActionValueCloud.objects.get(code=code):
#         filepath = os.path.join(KEYFILES_DIRS, filename)
#         if not os.path.exists(filepath):
#             with open(filepath, "w") as f:
#                 f.write(instance_action_value.value)
#         else:
#             with open(filepath, "r") as file:
#                 if file.read() != instance_action_value.value:
#                     with open(filepath, "w") as f:
#                         f.write(instance_action_value.value)
#         path = open(filepath, "r")
#         mime_type, _ = mimetypes.guess_type(filepath)
#         response = HttpResponse(path, content_type=mime_type)
#         response["Content-Disposition"] = f"attachment; filename={filename}"
#         return response


# #
