from django import forms

from application.models import ApplicationCloud


class ApplicationForm(forms.ModelForm):
    class Meta:
        style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
        model = ApplicationCloud
        fields = [
            "date_revoked",
            "state",
            "owner",
            "remote_id",
            "build_number",
            "build_date",
            "code_commande",
            "app_name",
            "app_package",
            "ip_address",
            "domaine_name",
            "login_admin",
            "password_admin"
        ]
        widgets = {
            "date_revoked": forms.DateInput(attrs={"class": style_class}),
            "state": forms.Select(attrs={"class": style_class}),
            "owner": forms.Select(attrs={"class": style_class}),
            "remote_id": forms.TextInput(attrs={"class": style_class}),
            "build_number": forms.NumberInput(attrs={"class": style_class}),
            "build_date": forms.DateInput(attrs={"class": style_class}),
            "code_commande": forms.TextInput(attrs={"class": style_class}),
            "app_name": forms.TextInput(attrs={"class": style_class}),
            "app_package": forms.TextInput(attrs={"class": style_class}),
            "ip_address": forms.TextInput(attrs={"class": style_class}),
            "domaine_name": forms.TextInput(attrs={"class": style_class}),
            "login_admin": forms.TextInput(attrs={"class": style_class}),
            "password_admin": forms.TextInput(attrs={"class": style_class}),
        }
