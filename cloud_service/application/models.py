from datetime import date
from django.db import models
from django.utils import timezone

from django.urls import reverse
from computedfields.models import ComputedFieldsModel, computed

from user_cloud.models import UserCloud

state = [
    ("stopped", "Arrêté"),
    ("started", "Démarré"),
    ("pending", "En cours de Démarrage"),
    ("open", "En cours de Construction"),
    ("deleting", "En cours de Suppression"),
    ("stopping", "En cours d'Arret"),
    ("revoking", "En cours de révocation"),
    ("revoked", "Révoqué"),
    ("error", "Erreur"),
    ("expire", "Expirée"),
]


class ApplicationCloud(ComputedFieldsModel, models.Model):
    date_created = models.DateTimeField("Date de création", auto_now_add=True)
    date_revoked = models.DateTimeField("Date de révocation", null=True, blank=True)
    state = models.CharField(
        "Etat", max_length=60, null=False, blank=True, choices=state, default="open"
    )
    owner = models.ForeignKey(
        UserCloud,
        on_delete=models.CASCADE,
        related_name="app_proprietaire",
        null=True,
        blank=True,
    )
    remote_id = models.CharField(
        "Identifient Distant",
        max_length=100,
        null=False,
        blank=True,
        default="xxxxxxxxxxxxxxxxx",
    )
    build_number = models.CharField(
        "Numéro de build", max_length=60, null=False, blank=True, default="xxxxxx"
    )
    build_number_for_dep = models.CharField(
        "Numéro de build", max_length=60, null=False, blank=True, default="xxxxxx"
    )
    build_date = models.DateTimeField("Date de construction", null=True, blank=True)
    code_commande = models.CharField("Matricule", max_length=60, null=False, blank=True)
    app_name = models.CharField("Nom de l'application", max_length=100)
    app_package = models.CharField("Package de l'application", max_length=100)
    ip_address = models.GenericIPAddressField("Adresse IP", null=True, blank=True)
    domaine_name = models.CharField(
        "Nom de domaine", max_length=255, null=True, blank=True
    )
    login_admin = models.CharField("Login admin", null=True, blank=True, max_length=100)
    password_admin = models.CharField(
        "Mot de passe admin", null=True, blank=True, max_length=255
    )
    ssh_key = models.TextField("Clé SSH", null=False, blank=True)

    @computed(
        models.CharField("Matricule", max_length=60, null=False, blank=True),
        depends=[("self", ["id"])],
    )
    def code(self):
        seq = f"00000000{str(self.id)}"
        seq_finale = seq[-8:]
        return f'APP_{str(date.today().strftime("%Y"))}_{seq_finale}'

    #
    def __str__(self):
        return self.code

    #
    def get_absolute_url(self):
        if self.code:
            return f"/adm/account/list/application/?code={self.code}"
        return reverse("admin_instance")

    #
    def get_percent(self):
        if self.state == "expire":
            return "100%"
        try:
            diff_1 = (timezone.now() - self.date_created).total_seconds()
            if diff_1 <= 0:
                return "O%"
            diff_2 = (self.date_revoked - self.date_created).total_seconds()
            if diff_2 <= 0:
                return "100%"
            percent = float((diff_1 / diff_2) * 100)
            return f"{percent:.2f}%"
        except Exception:
            return None
