///
function do_analytics_for_drawchart() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "/adm/account/analytics/drawchart",
            type: 'GET',
            //data: {
            //  key: 'value',
            //},
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        })
    })
}
// 
function do_analytics_for_drawchart_form(group_by,date_start,date_end,csrfmiddlewaretoken,dataType) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "/adm/account/analytics/drawchart",
            type: "POST",
            data: {
                group_by : group_by,
                date_start : date_start,
                date_end : date_end,
                csrfmiddlewaretoken : csrfmiddlewaretoken,
                dataType : dataType
            },
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        })
    })
}
//
function drawChart() {
    //#### barchart
    var barchart_commandes_options = {
        title: 'Diagramme à bandes des commandes',
        width: 520,
        height: 400,
        legend: 'none'
    };
    //
    var barchart_paiement_options = {
        title: 'Diagramme à bandes des paiements',
        width: 520,
        height: 400,
        legend: 'none'
    };
    //
    var barchart_instance_options = {
        title: 'Diagramme à bandes des instances',
        width: 520,
        height: 400,
        legend: 'none'
    };
    //
    var data_barchart_commande = new google.visualization.DataTable();
    data_barchart_commande.addColumn('string', 'Topping');
    data_barchart_commande.addColumn('number', 'Slices');
    var barchart_div_commande = new google.visualization.BarChart(document.getElementById('barchart_div_commande'));
    //
    var data_barchart_instance = new google.visualization.DataTable();
    data_barchart_instance.addColumn('string', 'Topping');
    data_barchart_instance.addColumn('number', 'Slices');
    var barchart_div_instance = new google.visualization.BarChart(document.getElementById('barchart_div_instance'));
    //
    var data_barchart_paiement = new google.visualization.DataTable();
    data_barchart_paiement.addColumn('string', 'Topping');
    data_barchart_paiement.addColumn('number', 'Slices');
    var barchart_div_paiement = new google.visualization.BarChart(document.getElementById('barchart_div_paiement'));
    //#### piechart_3d
    var piechart_3d_options_paiement = {
        title: 'Paiements',
        is3D: true,
        width: 500,
        height: 300,
        fontSize: 14,
        legend: { position: 'bottom' }
    };
    var piechart_3d_paiement = new google.visualization.PieChart(document.getElementById('paiement_piechart_3d'));
    //
    var piechart_3d_options_commande = {
        title: 'Commandes',
        is3D: true,
        width: 500,
        height: 300,
        fontSize: 14,
        legend: { position: 'bottom' }
    };
    var piechart_3d_commande = new google.visualization.PieChart(document.getElementById('commande_piechart_3d'));
    //
    var piechart_3d_options_instance = {
        title: 'Instances',
        is3D: true,
        width: 500,
        height: 300,
        fontSize: 14,
        legend: { position: 'bottom' }
    };
    var piechart_3d_instance = new google.visualization.PieChart(document.getElementById('instance_piechart_3d'));
    //

    do_analytics_for_drawchart()
        .then((data) => {
            //#### barchart
            data_barchart_commande.addRows(data.data_analytic__barchart.analytic_commande)
            barchart_div_commande.draw(data_barchart_commande, barchart_commandes_options);
            //
            data_barchart_paiement.addRows(data.data_analytic__barchart.analytic_paiement)
            barchart_div_paiement.draw(data_barchart_paiement, barchart_paiement_options);
            //
            data_barchart_instance.addRows(data.data_analytic__barchart.analytic_instance)
            barchart_div_instance.draw(data_barchart_instance, barchart_instance_options);
            //
            //#### piechart_3d
            var piechart_3d_data_paiement = google.visualization.arrayToDataTable(data.data_analytic__for_piechart_3d.analytic_paiement);
            piechart_3d_paiement.draw(piechart_3d_data_paiement, piechart_3d_options_paiement);
            //
            var piechart_3d_data_commande = google.visualization.arrayToDataTable(data.data_analytic__for_piechart_3d.analytic_commande);
            piechart_3d_commande.draw(piechart_3d_data_commande, piechart_3d_options_commande);
            //
            var piechart_3d_data_instance = google.visualization.arrayToDataTable(data.data_analytic__for_piechart_3d.analytic_instance);
            piechart_3d_instance.draw(piechart_3d_data_instance, piechart_3d_options_instance);
            //####
        })
        .catch((error) => {

        }
        )
}
//
function drawChart_form(group_by,date_start,date_end,csrfmiddlewaretoken,dataType) {
    //#### barchart
    var barchart_commandes_options = {
        title: 'Diagramme à bandes des commandes',
        width: 520,
        height: 400,
        legend: 'none'
    };
    //
    var barchart_paiement_options = {
        title: 'Diagramme à bandes des paiements',
        width: 520,
        height: 400,
        legend: 'none'
    };
    //
    var barchart_instance_options = {
        title: 'Diagramme à bandes des instances',
        width: 520,
        height: 400,
        legend: 'none'
    };
    //
    var data_barchart_commande = new google.visualization.DataTable();
    data_barchart_commande.addColumn('string', 'Topping');
    data_barchart_commande.addColumn('number', 'Slices');
    var barchart_div_commande = new google.visualization.BarChart(document.getElementById('barchart_div_commande'));
    //
    var data_barchart_instance = new google.visualization.DataTable();
    data_barchart_instance.addColumn('string', 'Topping');
    data_barchart_instance.addColumn('number', 'Slices');
    var barchart_div_instance = new google.visualization.BarChart(document.getElementById('barchart_div_instance'));
    //
    var data_barchart_paiement = new google.visualization.DataTable();
    data_barchart_paiement.addColumn('string', 'Topping');
    data_barchart_paiement.addColumn('number', 'Slices');
    var barchart_div_paiement = new google.visualization.BarChart(document.getElementById('barchart_div_paiement'));
    //#### piechart_3d
    var piechart_3d_options_paiement = {
        title: 'Paiements',
        is3D: true,
        width: 500,
        height: 300,
        fontSize: 14,
        legend: { position: 'bottom' }
    };
    var piechart_3d_paiement = new google.visualization.PieChart(document.getElementById('paiement_piechart_3d'));
    //
    var piechart_3d_options_commande = {
        title: 'Commandes',
        is3D: true,
        width: 500,
        height: 300,
        fontSize: 14,
        legend: { position: 'bottom' }
    };
    var piechart_3d_commande = new google.visualization.PieChart(document.getElementById('commande_piechart_3d'));
    //
    var piechart_3d_options_instance = {
        title: 'Instances',
        is3D: true,
        width: 500,
        height: 300,
        fontSize: 14,
        legend: { position: 'bottom' }
    };
    var piechart_3d_instance = new google.visualization.PieChart(document.getElementById('instance_piechart_3d'));
    //

    do_analytics_for_drawchart_form(group_by,date_start,date_end,csrfmiddlewaretoken,dataType)
        .then((data) => {
            //#### barchart
            data_barchart_commande.addRows(data.data_analytic__barchart.analytic_commande)
            barchart_div_commande.draw(data_barchart_commande, barchart_commandes_options);
            //
            data_barchart_paiement.addRows(data.data_analytic__barchart.analytic_paiement)
            barchart_div_paiement.draw(data_barchart_paiement, barchart_paiement_options);
            //
            data_barchart_instance.addRows(data.data_analytic__barchart.analytic_instance)
            barchart_div_instance.draw(data_barchart_instance, barchart_instance_options);
            //
            //#### piechart_3d
            var piechart_3d_data_paiement = google.visualization.arrayToDataTable(data.data_analytic__for_piechart_3d.analytic_paiement);
            piechart_3d_paiement.draw(piechart_3d_data_paiement, piechart_3d_options_paiement);
            //
            var piechart_3d_data_commande = google.visualization.arrayToDataTable(data.data_analytic__for_piechart_3d.analytic_commande);
            piechart_3d_commande.draw(piechart_3d_data_commande, piechart_3d_options_commande);
            //
            var piechart_3d_data_instance = google.visualization.arrayToDataTable(data.data_analytic__for_piechart_3d.analytic_instance);
            piechart_3d_instance.draw(piechart_3d_data_instance, piechart_3d_options_instance);
            //####
        })
        .catch((error) => {

        }
        )
}
// 
function api_get_analytics_for_drawchart(group_by,date_start,date_end,csrfmiddlewaretoken,dataType)
{
    // Load Charts and the corechart and barchart packages.
    google.charts.load('current', { 'packages': ['corechart'] });
    // Draw the pie chart and bar chart when Charts is loaded.
    google.charts.setOnLoadCallback(drawChart_form(group_by,date_start,date_end,csrfmiddlewaretoken,dataType));
}

