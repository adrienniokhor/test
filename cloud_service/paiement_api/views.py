import hashlib
from datetime import datetime

# Create your views here.
import paydunya
from django.contrib import messages  # import messages
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from paydunya import InvoiceItem, Store

from admin_cloud.tasks import task_runApplication, task_runInstance
from admin_cloud.view_paiement import validerCommande
from analytics.models import Event
from commande.models import CommandeCloud, PaiementCloud

# sHC4NOuw-jZMa-rWIz-DWr4-kMasS4SZTNc7
# test_private_4HB7cw2Yj57aNez2bIGw2WZ58IV
# 6vIK3mrWQODFTPrktdgt

PAYDUNYA_ACCESS_TOKENS = {
    "PAYDUNYA-MASTER-KEY": "sHC4NOuw-jZMa-rWIz-DWr4-kMasS4SZTNc7",
    "PAYDUNYA-PRIVATE-KEY": "live_private_cNIiscsJokhDWNJNQ79HmgpkeaU",
    "PAYDUNYA-TOKEN": "tpKIm4H5nUndZsX1EfnY",
}

# # Activer le mode 'test'. Le debug est à False par défaut
# paydunya.debug = True

# Configurer les clés d'API
paydunya.api_keys = PAYDUNYA_ACCESS_TOKENS
# Configuration des informations de votre service/entreprise
infos = {
    "name": "Cori Digital",  # Seul le nom est requis
    "tagline": "Notre mission : Vous accompagnez au quotidien dans votre transformation numérique avec le cloud",  # noqa: E501
    "postal_address": "Dakar Plateau",
    "phone_number": "336530583",
    "website_url": "http://coridigital.com/",
    "logo_url": "http://coridigital.com/media/images/logo_cori_1.png",
}


def addPaiementAutomatic(code_commande):
    # pylint: disable=E1101
    if commande := CommandeCloud.objects.get(code=code_commande):

        store = Store(**infos)
        invoice = paydunya.Invoice(store)
        invoice.add_taxes(
            [(f"TVA ({commande.TVA}%)", commande.total_price - commande.price)]
        )
        items = [
            InvoiceItem(
                name=elt.service.name,
                quantity=elt.quantity,
                unit_price=elt.price,
                total_price=elt.total_price,
                description=elt.service.description,
            )
            for elt in commande.commmande_ligne_commande.all()
        ]
        invoice.cancel_url = f"https://coridigital.com/paiement/cancel/{code_commande}"
        invoice.return_url = f"https://coridigital.com/paiement/check/{code_commande}"
        invoice.callback_url = "https://coridigital.com/paiement/callback"
        invoice.add_custom_data([("commande", code_commande)])
        invoice.add_items(items)
        invoice.total_amount = commande.total_price
        successful, response = invoice.create()
        print("*********************************************************")
        print(successful)
        print("#########################################################")
        print(response)
        print("*********************************************************")
        if successful:
            # print('*********************************************************')
            # print(successful)
            # print('#########################################################')
            # print(type(successful))
            # print('*********************************************************')
            # print(response)
            # print('#########################################################')
            # print(type(response))
            # print('*********************************************************')
            if response.get("response_text"):
                return HttpResponseRedirect(response.get("response_text"))

    return redirect("user_commande")


def paiement_check(request, code):  # sourcery skip: low-code-quality
    try:
        if code:
            # pylint: disable=E1101
            if commande := CommandeCloud.objects.get(code=code):
                #
                store = Store(**infos)
                invoice = paydunya.Invoice(store)
                successful, response = invoice.confirm(request.GET.get("token"))
                if successful:
                    if (
                        response["hash"]
                        == hashlib.sha512(
                            b"sHC4NOuw-jZMa-rWIz-DWr4-kMasS4SZTNc7"
                        ).hexdigest()
                    ):
                        if response["custom_data"]["commande"] != code:
                            messages.error(
                                request,
                                "Une erreur ,les factures ne correspondent pas !!",
                            )
                            return redirect(f"/user/account/commande?code={code}")
                        if response["invoice"]["total_amount"] < commande.total_price:
                            messages.warning(
                                request,
                                "Erreur , le montant payé est inférieur au prix total de la commande",  # noqa: E501
                            )
                            return redirect(f"/user/account/commande?code={code}")
                        if response["status"] == "completed":
                            if add_paiement(commande, response["invoice"]["token"]):
                                messages.success(
                                    request,
                                    "Paiement effectué avec succès, le service demandé sera disponible dans quelques munites",  # noqa: E501
                                )
                            else:
                                messages.warning(
                                    request, "Erreur , le paiement n'a pas été validé"
                                )
                                #
                        elif response["status"] == "pending":
                            if add_paiement(commande, response["invoice"]["token"]):
                                messages.success(
                                    request,
                                    "Paiement en cours de validation, le service demandé sera disponible quelques munites après que le paiement soit effectif",  # noqa: E501
                                )
                            else:
                                messages.warning(
                                    request, "Erreur , le paiement n'a pas été validé"
                                )
                        elif response["status"] == "cancelled":
                            messages.warning(request, "Le paiement a été annulé")
                        else:
                            messages.error(request, "Erreur , opération inconnue")
                    else:
                        messages.error(
                            request,
                            "Une erreur ,s'est produite le paiement n'est pas validé",
                        )
                else:
                    messages.error(
                        request,
                        "Une erreur ,s'est produite le paiement n'est pas validé",
                    )
                return redirect(f"/user/account/commande?code={code}")
            else:
                messages.error(
                    request,
                    "Une erreur ,s'est produite le paiement effectué ne correspond à aucune commande",  # noqa: E501
                )
                return redirect("user_commande")
        else:
            messages.error(
                request,
                "Une erreur ,s'est produite le paiement effectué ne correspond à aucune commande",
            )
    except Exception:
        messages.error(request, "Une erreur ,s'est produite lors du paiement de la commande")
    return redirect("user_commande")


def paiement_cancel(request, code):  # sourcery skip: low-code-quality
    try:
        if code:
            # pylint: disable=E1101
            if commande := CommandeCloud.objects.get(code=code):
                #
                store = Store(**infos)
                invoice = paydunya.Invoice(store)
                successful, response = invoice.confirm(request.GET.get("token"))
                #
                if successful:
                    if (
                        response["hash"]
                        == hashlib.sha512(
                            b"sHC4NOuw-jZMa-rWIz-DWr4-kMasS4SZTNc7"
                        ).hexdigest()
                    ):
                        if response["custom_data"]["commande"] != code:
                            messages.error(
                                request,
                                "Une erreur ,les factures ne correspondent pas !!",
                            )
                            return redirect(f"/user/account/commande?code={code}")
                        if response["invoice"]["total_amount"] < commande.total_price:
                            messages.warning(
                                request,
                                "Erreur , le montant payé est inférieur au prix total de la commande",  # noqa: E501
                            )
                            return redirect(f"/user/account/commande?code={code}")
                        if response["status"] == "cancelled":
                            if commande.commande_paiement.all():
                                messages.warning(
                                    request,
                                    "La commande est déja payé , impossible d'annuler votre paiement",  # noqa: E501
                                )
                            else:
                                messages.success(
                                    request, "Le paiement a été bien annulé"
                                )
                        else:
                            messages.error(request, "Erreur , opération inconnue")
                    else:
                        messages.error(
                            request,
                            "Une erreur ,s'est produite le paiement n'est pas validé",
                        )
                else:
                    messages.error(
                        request,
                        "Une erreur ,s'est produite le paiement n'est pas validé",
                    )
                return redirect(f"/user/account/commande?code={code}")
            else:
                messages.error(
                    request,
                    "Une erreur ,s'est produite le paiement effectué ne correspond à aucune commande",  # noqa: E501
                )
                return redirect("user_commande")
        else:
            messages.error(
                request,
                "Une erreur ,s'est produite le paiement effectué ne correspond à aucune commande",
            )
            return redirect("user_commande")
    except Exception as e:
        messages.error(
            request, "Une erreur ,s'est produite lors du paiement de la commande"
        )
    return redirect("user_commande")


@csrf_exempt
def paiement_callback_url(request):
    # try:
    if response := request.POST.dict():
        if (
            response.get("data[hash]")
            and response["data[hash]"]
            == hashlib.sha512(b"sHC4NOuw-jZMa-rWIz-DWr4-kMasS4SZTNc7").hexdigest()
            and response.get("data[invoice][token]")
        ):
            # pylint: disable=E1101
            paiement = PaiementCloud.objects.get(
                numTransaction_or_numCarte=f'{response.get("data[invoice][token]")}'
            )
            if (
                paiement
                and paiement.commande
                and paiement.state == "open"
                and response.get("data[status]")
                and response["data[status]"] == "completed"
                and response.get("data[custom_data][commande]")
                and paiement.commande.code == response["data[custom_data][commande]"]
                and response.get("data[invoice][total_amount]")
                and float(response["data[invoice][total_amount]"])
                >= paiement.commande.total_price
            ):
                paiement.state = "validat"
                paiement.date_valid = datetime.now()
                paiement.save()
                commande = paiement.commande
                return_valid = validerCommande(commande)
                print("return_valid", return_valid)
                if return_valid:
                    task_runInstance.delay(paiement, paiement.commande.id)
                    task_runApplication.delay(paiement.commande.id)
    return JsonResponse({"status": "ok"})
    # except Exception as e:
    #     print('**************************************************************')
    #     print(request.POST)
    #     print(e)
    #     print('**************************************************************')
    #     return JsonResponse({'status': 'error'})
    # finally:
    #     return JsonResponse({'status': 'error'})


# aliadilanie21@gmail.com
# EBALinnalabella9


def add_paiement(commande, code_transaction):
    try:
        if commande:
            # pylint: disable=E1101
            paiement = PaiementCloud.objects.create(
                date_created=timezone.now(),
                date_paiement=timezone.now(),
                commande=commande,
                mode_paiement=None,
                numTransaction_or_numCarte=code_transaction,
                state="open",
                devise="fcfa",
                montant=commande.total_price,
                proprietaire=commande.proprietaire,
            )
            paiement.save()
            create_paiement_event = Event(
                name="Create Paiement",
                object="paiement",
                code=paiement.code,
                created=timezone.now(),
            )
            create_paiement_event.save()
            commande.state = "validat"
            commande.save()
            return True
        else:
            return False
    except Exception:
        return False
