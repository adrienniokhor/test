from django.apps import AppConfig


class PaiementApiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "paiement_api"
