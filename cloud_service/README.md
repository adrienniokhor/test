# Install python3 dependencies :
 - pip3 install -r requirements.txt
# Install Rabbitmd on docker container :
 - sudo docker run -d -p 5672:5672 --name=rabbitmd rabbitmq
# Run migrations :
 - python3 manage.py makemigrations
 - python3 manage.py migrate
# Run Django project :
 - python3 manage.py runserver 0:80
# Run Celery worker and bear :
 - celery -A cloud_service  worker -l info
 - celery -A cloud_service beat -l info
# Run commande in background
 - screen -d -m + commande

screen -d -m sudo python3 manage.py tailwind start

screen -d -m sudo python3 manage.py runserver 0:80 && screen -d -m sudo python3 manage.py tailwind start && screen -d -m sudo celery -A cloud_service  worker -l info && screen -d -m sudo celery -A cloud_service beat -l info
