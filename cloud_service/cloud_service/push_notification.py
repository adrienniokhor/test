import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import channels.layers
from asgiref.sync import async_to_sync


async def notify_customer(user_id, message):
    channel_layer = channels.layers.get_channel_layer()
    # async_to_sync(channel_layer.group_send)(
    #     f'notify_{user_id}', {"type": "notify_message", "message": message})
    await channel_layer.group_send(
        f"notify_{user_id}", {"type": "notify_message", "message": message}
    )
