from celery import shared_task


@shared_task(name="check_instance_config_witch_jenkins")
def check_instance_config_witch_jenkins():
    # pylint: disable=C0415
    from api_service.jenkins.crontab.scheduled_job import checkInstanceConfig

    checkInstanceConfig()


#
@shared_task(name="check_instance_config_witch_boto3")
def check_instance_config_witch_boto3():
    # pylint: disable=C0415
    from api_service.boto3.crontab.scheduled_job import checkInstanceConfig

    checkInstanceConfig()


#
@shared_task(name="check_application_config_witch_jenkins")
def check_application_config_witch_jenkins():
    # pylint: disable=C0415
    from api_service.jenkins.crontab.scheduled_job import checkApplicationConfig

    checkApplicationConfig()


@shared_task(name="check_application_is_init_running")
def check_application_is_init_running():
    # pylint: disable=C0415
    from api_service.jenkins.crontab.scheduled_job import CheckBuildCompleteAndSuccess

    CheckBuildCompleteAndSuccess()


#
@shared_task(name="check_application_config_witch_boto3")
def check_application_config_witch_boto3():
    # pylint: disable=C0415
    from api_service.boto3.crontab.scheduled_job import checkApplicationConfig
    checkApplicationConfig()

