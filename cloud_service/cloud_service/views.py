from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render

from groupe_service.models import GroupServiceCloud
from services.models import ForfaitCloud

# Create your views here.


def acceuil(request):
    if request.GET.get("next_page"):
        next_page = request.GET.get("next_page")
        if next_page == "page_erp":
            return render(request, "index.html", context={"next_page": "page_erp"})
        if next_page == "page_hrm":
            return render(request, "index.html", context={"next_page": "page_hrm"})
        if next_page == "page_education":
            return render(
                request, "index.html", context={"next_page": "page_education"}
            )
        if next_page == "page_e-commerce":
            return render(
                request, "index.html", context={"next_page": "page_e-commerce"}
            )
        if next_page == "page_sondage":
            return render(request, "index.html", context={"next_page": "page_sondage"})
        if next_page == "page_analytique":
            return render(
                request, "index.html", context={"next_page": "page_analytique"}
            )
        if next_page == "page_mailing":
            return render(request, "index.html", context={"next_page": "page_mailing"})
        if next_page == "page_booking":
            return render(request, "index.html", context={"next_page": "page_booking"})
        if next_page == "page_sap-ngu":
            return render(request, "index.html", context={"next_page": "page_sap-ngu"})
        if next_page == "page_projet":
            return render(request, "index.html", context={"next_page": "page_projet"})
        if next_page == "page_support":
            return render(request, "index.html", context={"next_page": "page_support"})
        if next_page == "page_fact-pme":
            return render(request, "index.html", context={"next_page": "page_fact-pme"})
        if next_page == "page_erp_hearlth":
            return render(
                request, "index.html", context={"next_page": "page_erp_hearlth"}
            )
        if next_page == "page_perimetre_intervention ":
            return render(
                request, "index.html", context={"next_page": "page_perimetre_intervention"}
            )

    #
    else:
        grp_service = GroupServiceCloud.objects.filter(enabled=True)
        return render(request, "index.html", context={"grp_services": grp_service})


#
def login(request):
    
    return render(request, "login.html", context={})


#
def error_404_view(request, exception):
    # we add the path to the the 404.html file
    # here. The name of our HTML file is 404.html
    return render(request, "404.html")


#
def access_page(request, page_name):
    return render(request, "page.html", context={"page_name": page_name})


def access_page_footer(request, page_name_footer):
    if page_name_footer == "espace_client":
        return (
            HttpResponseRedirect("/user/account")
            if request.user.is_authenticated
            else HttpResponseRedirect("/user/login")
        )
    else:
        return render(
            request, "page_footer.html", context={"page_name_footer": page_name_footer}
        )
