# celery.py
from __future__ import absolute_import
import os
from celery import Celery
from celery.schedules import crontab  # scheduler
from django.conf import settings

# default django settings
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cloud_service.settings")
app = Celery("cloud_service")
app.conf.timezone = "UTC"
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
#
from .tasks import *
from admin_cloud.tasks import task_check_instance_revoke

#
app.conf.beat_schedule = {
    #Instance jobs
    "check_instance_config_witch_jenkins": {
        "task": "check_instance_config_witch_jenkins",
        "schedule": crontab(minute="*/1"),
    },
    "check_instance_config_witch_boto3": {
        "task": "check_instance_config_witch_boto3",
        "schedule": crontab(minute="*/1"),
    },
    "check_revoke_instance_after_date_revoke": {
        "task": "check_revoke_instance_after_date_revoke",
        "schedule": crontab(minute="*/1"),
    },
    #Application jobs
    "check_application_config_witch_jenkins": {
        "task": "check_application_config_witch_jenkins",
        "schedule": crontab(minute="*/1"),
    },
    "check_application_config_witch_boto3": {
        "task": "check_application_config_witch_boto3",
        "schedule": crontab(minute="*/1"),
    },
    "check_application_is_init_running": {
        "task": "check_application_is_init_running",
        "schedule": crontab(minute="*/1"),
    },
}
