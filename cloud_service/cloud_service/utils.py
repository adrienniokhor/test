from io import BytesIO
from email.errors import MessageError, MessageDefect
from xhtml2pdf import pisa
from django.core.mail import EmailMessage
from django.template.loader import get_template, render_to_string
from celery import shared_task


from cloud_service.settings import EMAIL_CORI_SUPPORT
from commande.models import PaiementCloud


def html_to_pdf(template_src, context_dict=None):
    if context_dict is None:
        context_dict = {}
    #template = get_template(template_src)
    #html = template.render(context_dict)
    html = render_to_string(template_src, context_dict)
    result = BytesIO()
    pisa.pisaDocument(BytesIO(html.encode("utf-8")), result)
    return result.getvalue()


@shared_task
def notify_on_paiement(paiement_id):
    try:
        # pylint: disable=E1101
        paiement = PaiementCloud.objects.get(id=paiement_id)
        commande = paiement.commande
        mail_subject = f"Accusé réception d'une soumission paiement({paiement.code}) pour la commande {commande.code}"
        message = f"""
        Madame, Monsieur,
        Nous avons bien reçu votre paiement({paiement.code}) pour la commande {commande.code}.
        Les services commandés seront disponibles dès que le paiement est validé par une de nos équipes.
        Vous trouverez ci-joint toutes les informations en rapport avec la commande {commande.code}.
        Nous restons à votre entière disposition pour tout renseignement complémentaire.

        
        Bien cordialement,
        """
        to_email = [commande.proprietaire.email]
        cc_email = [EMAIL_CORI_SUPPORT]
        email = EmailMessage(mail_subject, message, to=to_email, cc=cc_email)
        dict_context = {
            "commande": commande,
            "paiement": paiement
        }
        content = html_to_pdf("invoice_template.html", dict_context)
        email.attach(f"devis_{commande.code}.pdf", content, "application/pdf")
        email.send()
    # pylint: disable=E1101
    except (PaiementCloud.DoesNotExist, MessageError, MessageDefect):
        return


@shared_task
def notify_on_paiement_valid(paiement_id):
    try:
        # pylint: disable=E1101
        paiement = PaiementCloud.objects.get(id=paiement_id)
        commande = paiement.commande
        mail_subject = f"Validation paiement {paiement.code} pour la commande {commande.code}"
        message = f"""
        Madame, Monsieur,
        Le paiement {paiement.code} pour la commande {commande.code} effectué à la date du {paiement.date_created}, vient d'être validé. 
        Les services commandés seront dès à présent disponibles.
        Vous trouverez ci-joint toutes les informations en rapport avec la commande {commande.code}.
        Nous restons à votre entière disposition pour tout renseignement complémentaire.

        
        Bien cordialement,
        """
        to_email = [commande.proprietaire.email]
        cc_email = [EMAIL_CORI_SUPPORT]
        email = EmailMessage(mail_subject, message, to=to_email, cc=cc_email)
        dict_context = {
            "commande": commande,
            "paiement": paiement
        }
        content = html_to_pdf("invoice_template.html", dict_context)
        email.attach(f"devis_{commande.code}.pdf", content, "application/pdf")
        email.send()
    # pylint: disable=E1101
    except (PaiementCloud.DoesNotExist, MessageError, MessageDefect):
        return
