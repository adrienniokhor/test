"""cloud_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import path, include, re_path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views import static as static_view
from django.conf.urls.static import static
from . import settings as local_setting
from .views import acceuil, access_page, access_page_footer

urlpatterns = (
    [
        re_path(
            r"^static/(?P<path>.*)$",
            static_view.serve,
            {"document_root": settings.STATIC_ROOT},
            name="static",
        ),
        re_path(
            r"^media/(?P<path>.*)$",
            static_view.serve,
            {"document_root": settings.MEDIA_ROOT},
            name="media",
        ),
        path('admin/', admin.site.urls),
        path("__reload__/", include("django_browser_reload.urls")),
        path("", acceuil, name="acceuil"),
        path("page/<str:page_name>", access_page, name="access_page"),
        path(
            "page/info/<str:page_name_footer>",
            access_page_footer,
            name="access_page_footer",
        ),
        path("groupe_service/", include("groupe_service.urls"), name="groupe_service"),
        path("user/", include("authentification.urls")),
        path("user/register/", include("register.urls")),
        path("", include("user_cloud.urls")),
        path("", include("admin_cloud.urls")),
        path("", include("password_reset.urls")),
        path("api/", include("public_web_service.urls")),
        path("admin/", admin.site.urls),
        path("", include("paiement_api.urls")),
        path("", include("web_ssh.urls")),
        path("", include("application.urls")),
    ]
    + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
)
#
handler404 = "cloud_service.views.error_404_view"
if local_setting.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
