from django.shortcuts import render
from django.http import JsonResponse
from groupe_service.models import GroupServiceCloud

# Create your views here.
def get_all_groupe_services(request):
    all_group_services = GroupServiceCloud.objects.all()
    list_all_group_services_dict = [
        {"code": elt.code, "name": elt.name} for elt in all_group_services
    ]
    return JsonResponse({"groupes_services": list_all_group_services_dict})
