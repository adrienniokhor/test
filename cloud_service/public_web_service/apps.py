from django.apps import AppConfig


class PublicWebServiceConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "public_web_service"
