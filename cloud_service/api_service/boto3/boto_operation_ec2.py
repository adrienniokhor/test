from . import connexion

#


def stop_instance(instance_id):
    ec2_client = connexion.connection_aws_ec2()
    return ec2_client.stop_instances(InstanceIds=[instance_id])


#
def start_instance(instance_id):
    ec2_client = connexion.connection_aws_ec2()
    return ec2_client.start_instances(InstanceIds=[instance_id])


#
def terminate_instance(instance_id):
    ec2_client = connexion.connection_aws_ec2()
    return ec2_client.terminate_instances(InstanceIds=[instance_id])


#
def describe_all_instances():
    ec2_client = connexion.connection_aws_ec2()
    return ec2_client.describe_instances()


#
def describe_list_instances_with_list_id(list_id: list):
    ec2_client = connexion.connection_aws_ec2()
    return ec2_client.describe_instances(
        Filters=[
            {
                "Name": "instance-id",
                "Values": list_id,
            }
        ]
    )


#
def describe_all_instances_format():
    result = describe_all_instances()
    list_describe_instances = []
    for reservation in result.get("Reservations"):
        list_describe_instances.extend(
            {
                "instance_id": instance["InstanceId"],
                "instance_type": instance["InstanceType"],
                "public_ip": instance["PublicIpAddress"],
                "private_ip": instance["PrivateIpAddress"],
            }
            for instance in reservation["Instances"]
        )

    #
    return list_describe_instances


#
def describe_instance_with_id(instance_id):
    ec2_client = connexion.connection_aws_ec2()
    instances = ec2_client.describe_instances(
        Filters=[
            {
                "Name": "instance-id",
                "Values": [instance_id],
            }
        ]
    )
    return instances.get("Reservations")


#
def describe_instance_running_with_id(instance_id):
    ec2_client = connexion.connection_aws_ec2()
    instances = ec2_client.describe_instances(
        Filters=[
            {
                "Name": "instance-id",
                "Values": [instance_id],
            },
            {
                "Name": "instance-state-name",
                "Values": ["running"],
            },
        ]
    )
    return instances.get("Reservations")


#
def get_running_instances():
    ec2_client = connexion.connection_aws_ec2()
    list_instance_running_desc = []
    reservations = ec2_client.describe_instances(
        Filters=[
            {
                "Name": "instance-state-name",
                "Values": ["running"],
            }
        ]
    ).get("Reservations")

    for reservation in reservations:
        for instance in reservation["Instances"]:
            instance_id = instance["InstanceId"]
            instance_type = instance["InstanceType"]
            public_ip = instance["PublicIpAddress"]
            private_ip = instance["PrivateIpAddress"]
            list_instance_running_desc.append(
                {
                    "instance_id": instance_id,
                    "instance_type": instance_type,
                    "private_ip": private_ip,
                    "public_ip": public_ip,
                }
            )
            print(f"{instance_id}, {instance_type}, {public_ip}, {private_ip}")
    #
    return list_instance_running_desc


#


def get_public_ip(instance_id):
    ec2_client = connexion.connection_aws_ec2()
    reservations = ec2_client.describe_instances(InstanceIds=[instance_id]).get(
        "Reservations"
    )

    for reservation in reservations:
        for instance in reservation["Instances"]:
            print(instance.get("PublicIpAddress"))
            return instance.get("PublicIpAddress")


#
def create_instance():
    ec2_client = connexion.connection_aws_ec2()
    instances = ec2_client.run_instances(
        ImageId="ami-0b0154d3d8011b0cd",
        MinCount=1,
        MaxCount=1,
        InstanceType="t4g.nano",
        KeyName="ec2-key-pair",
    )

    print(instances["Instances"][0]["InstanceId"])


#
def terminate_instance(instance_id):
    ec2_client = connexion.connection_aws_ec2()
    return ec2_client.terminate_instances(InstanceIds=[instance_id])


#
def get_security_group_instance(instance_id):
    ec2_client = connexion.connection_aws_ec2()
    instance = ec2_client.describe_instances(InstanceIds=[instance_id])
    return (
        instance.get("Reservations")[0]
        .get("Instances")[0]
        .get("NetworkInterfaces")[0]
        .get("Groups")[0]
        .get("GroupId")
    )


#
def add_sg_erule(grp_id, proto, start_port, end_port, ip_range):
    ec2_client = connexion.connection_aws_ec2()
    response = ec2_client.authorize_security_group_egress(
        GroupId=grp_id,
        IpPermissions=[
            {
                "IpProtocol": proto,
                "FromPort": start_port,
                "ToPort": end_port,
                "IpRanges": [{"CidrIp": ip_range}],
            }
        ],
    )
    if not response.get("Return") or not response.get("SecurityGroupRules"):
        return {}
    security_group_rules = response.get("SecurityGroupRules")[0]
    security_group_rule_dict = {
        "SecurityGroupRuleId": security_group_rules.get("SecurityGroupRuleId")
    }
    security_group_rule_dict["GroupId"] = security_group_rules.get("GroupId")
    security_group_rule_dict["FromPort"] = security_group_rules.get("FromPort")
    security_group_rule_dict["IpProtocol"] = security_group_rules.get("IpProtocol")
    security_group_rule_dict["ToPort"] = security_group_rules.get("ToPort")
    security_group_rule_dict["CidrIpv4"] = security_group_rules.get("CidrIpv4")
    security_group_rule_dict["CidrIpv6"] = security_group_rules.get("CidrIpv6")
    security_group_rule_dict["IsEgress"] = security_group_rules.get("IsEgress")
    return security_group_rule_dict


#
def add_sg_irule(grp_id, proto, start_port, end_port, ip_range):
    ec2_client = connexion.connection_aws_ec2()
    response = ec2_client.authorize_security_group_ingress(
        GroupId=grp_id,
        IpPermissions=[
            {
                "IpProtocol": proto,
                "FromPort": start_port,
                "ToPort": end_port,
                "IpRanges": [{"CidrIp": ip_range}],
            }
        ],
    )
    if not response.get("Return") or not response.get("SecurityGroupRules"):
        return {}
    security_group_rules = response.get("SecurityGroupRules")[0]
    security_group_rule_dict = {
        "SecurityGroupRuleId": security_group_rules.get("SecurityGroupRuleId")
    }
    security_group_rule_dict["GroupId"] = security_group_rules.get("GroupId")
    security_group_rule_dict["FromPort"] = security_group_rules.get("FromPort")
    security_group_rule_dict["IpProtocol"] = security_group_rules.get("IpProtocol")
    security_group_rule_dict["ToPort"] = security_group_rules.get("ToPort")
    security_group_rule_dict["CidrIpv4"] = security_group_rules.get("CidrIpv4")
    security_group_rule_dict["CidrIpv6"] = security_group_rules.get("CidrIpv6")
    security_group_rule_dict["IsEgress"] = security_group_rules.get("IsEgress")
    return security_group_rule_dict


#
def delete_sg_erule(security_grp_id, security_grp_rule_id):
    ec2_client = connexion.connection_aws_ec2()
    response = ec2_client.revoke_security_group_egress(
        GroupId=security_grp_id, SecurityGroupRuleIds=[security_grp_rule_id]
    )
    return response.get("Return")


#
def delete_sgr_irule(security_grp_id, security_grp_rule_id):
    ec2_client = connexion.connection_aws_ec2()
    response = ec2_client.revoke_security_group_ingress(
        GroupId=security_grp_id, SecurityGroupRuleIds=[security_grp_rule_id]
    )
    return response.get("Return")
