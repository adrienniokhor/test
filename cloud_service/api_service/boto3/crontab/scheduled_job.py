from api_service.boto3.boto_operation_ec2 import describe_instance_running_with_id
from application.models import ApplicationCloud
from services.models import InstanceCloud


#
def checkInstanceConfig():
    instances = InstanceCloud.objects.filter(state="pending")
    for instance in instances:
        if instance.remote_id:
            ip_address = get_public_ip(instance)
            try:
                ip_address = get_public_ip(instance)
                if (
                    instance_instance_value := instance.instance_action_value_instance.all()
                ):
                    for element in instance_instance_value:
                        if element.action.name == "IP":
                            element.value = ip_address
                            element.save()
                instance.state = "started"
            except Exception as e:
                continue
            #
            instance.save()


#
def checkApplicationConfig():
    applications = ApplicationCloud.objects.filter(state="pending")
    for application in applications:
        if application.remote_id:
            try:
                ip_address = get_public_ip(application)
                application.ip_address = ip_address
                application.save()
                application.state = "started"
            except Exception as e:
                continue
            #


#
def get_public_ip(instance_local):
    if result := describe_instance_running_with_id(instance_local.remote_id):
        if reservation := result[0]:
            if instance := reservation.get("Instances"):
                return instance[0].get("PublicIpAddress")


#
