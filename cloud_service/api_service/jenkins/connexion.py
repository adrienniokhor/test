import jenkins


class Connexion(jenkins.Jenkins):
    server = None

    def __init__(self, url, username=None, password=None, timeout=None):
        self.url = url
        self.username = username
        self.password = password
        self.timeout = timeout
        super().__init__(url, username, password, timeout)

 