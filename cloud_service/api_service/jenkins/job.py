from datetime import datetime
import time
from api_service.jenkins.connexion import Connexion
from cloud_service.settings import (
    Jenkins_password,
    Jenkins_url,
    Jenkins_user,
    jenkins_token,
)


def call_jenkins_job(job_name, *args, **kwargs):
    server = Connexion(
        url=Jenkins_url, username=Jenkins_user, password=Jenkins_password
    )
    datetime_str = datetime.now().strftime("%Y-%m-%d_%H-%M-%S-%f")
    params = {"project_name": f"{job_name}_{datetime_str}"}
    
    if kwargs:
        for key, value in kwargs.items():
            if key in ["instance_type", "image", "Disk", "Services"]:
                params[key] = value
    server.build_job(name=job_name, parameters=params, token=jenkins_token)
    #time.sleep(10)
    return server.get_job_info(job_name)["nextBuildNumber"]


def call_jenkins_job_dep_app(job_name, *args, **kwargs):
    server = Connexion(
        url=Jenkins_url, username=Jenkins_user, password=Jenkins_password
    )
    server.build_job(name=job_name, parameters=kwargs, token=jenkins_token)
    #time.sleep(10)
    return server.get_job_info(job_name)["nextBuildNumber"]


#
def format_output_jenkins(console_output):
    output = console_output.split("\n")
    output_formatted = [line[6:] for line in output if line.startswith("[INFO]")]
