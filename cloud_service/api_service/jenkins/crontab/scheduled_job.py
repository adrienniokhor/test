import asyncio
from admin_cloud.tasks import background_task_dep_app
from api_service.jenkins.connexion import Connexion
from application.models import ApplicationCloud
from cloud_service.push_notification import notify_customer
from cloud_service.settings import Jenkins_password, Jenkins_url, Jenkins_user
from services.models import InstanceCloud
from tools.command_on_remonte_server import Commands


#
# @shared_task(name='check_instance_config_with_kenkins')
def checkInstanceConfig():
    # pylint: disable=E1101
    instances = InstanceCloud.objects.filter(state="open")
    # instances=InstanceCloud.objects.filter(code='INST_2022_00000036')
    for instance in instances:
        if instance.build_number and instance.service.executor:
            # if instance.build_number :
            try:
                if info := getBuildJobInfo(
                    instance.service.executor, int(instance.build_number)
                ):
                    if (
                        instance_instance_value := instance.instance_action_value_instance.all()
                    ):
                        for element in instance_instance_value:
                            if element.action.name == "IP":
                                element.value = info.get("IP")
                                element.save()
                            if element.action.name == "SSH":
                                element.value = info.get("SSH")
                                element.save()
                            if element.action.name == "USER":
                                element.value = info.get("USER")
                                element.save()
                        if info.get("REMOTE_ID"):
                            instance.state = "started"
                            instance.remote_id = info.get("REMOTE_ID")
                            instance.save()
                            asyncio.set_event_loop(asyncio.new_event_loop())
                            asyncio.run(
                                notify_customer(
                                    instance.owner.id,
                                    f"Votre instance {instance.code} est maintenant disponible <br> Merci d'avoir patienter !!!!",
                                )
                            )
            except Exception as e:
                instance.state = "open"
                instance.save()
                continue


def checkApplicationConfig():
    # pylint: disable=E1101
    applications = ApplicationCloud.objects.filter(state="open")
    for application in applications:
        if application.build_number:
            try:
                if info := getBuildJobInfo(
                    "aws-ansible", int(application.build_number)
                ):
                    if info.get("IP"):
                        application.ip_address = info.get("IP")
                    if info.get("SSH"):
                        application.ssh_key = info.get("SSH")
                    if info.get("REMOTE_ID"):
                        application.remote_id = info.get("REMOTE_ID")
                    application.state = "pending"
                    application.save()
                    asyncio.set_event_loop(asyncio.new_event_loop())
                    asyncio.run(
                        notify_customer(
                            application.owner.id,
                            f"Votre application {application.code} est en cours de déploiement <br> Merci de patienter !!!!",
                        )
                    )
                    background_task_dep_app.delay(application.id)
                    #
                    
            except Exception as e:
                application.state = "open"
                application.save()
                continue

def checkApplicationRunning():
    # pylint: disable=E1101
    applications = ApplicationCloud.objects.filter(state="pending")
    for application in applications:
        if application.build_number:
            try:
                if info := getBuildJobInfo(
                    "aws-ansible", int(application.build_number)
                ):
                    if info.get("IP"):
                        application.ip_address = info.get("IP")
                    if info.get("SSH"):
                        application.ssh_key = info.get("SSH")
                    if info.get("REMOTE_ID"):
                        application.remote_id = info.get("REMOTE_ID")
                    application.state = "pending"
                    application.save()
                    asyncio.set_event_loop(asyncio.new_event_loop())
                    asyncio.run(
                        notify_customer(
                            application.owner.id,
                            f"Votre application {application.code} est en cours de déploiement <br> Merci de patienter !!!!",
                        )
                    )
                    background_task_dep_app.delay(application.id)
                    #
                    
            except Exception as e:
                application.state = "open"
                application.save()
                continue


#
def getBuildJobInfo(jobName, buil_number):
    try:
        server = Connexion(
            url=Jenkins_url, username=Jenkins_user, password=Jenkins_password
        )
        # return server.get_build_info(jobName,buil_number)
        output = server.get_build_console_output(jobName, buil_number)
        # list_output = list(output.split("+ echo '##########'"))
        # list_output.pop(0)
        # list_output.pop(len(list_output) - 1)
        # output_1 = list_output[0].strip()
        # output_2 = list_output[1].strip()
        # list_output_1 = output_1.split("\n")
        # list_output_1_1 = list_output_1[len(list_output_1)-1].split(" ")
        # list_output_2 = output_2.split("\n")
        # list_output_2.pop(0)
        # list_output_2.pop(0)
        # list_output_2_1 = "\n".join(list_output_2)
        # return {'IP':list_output_1_1[0],'USER':list_output_1_1[1].split("=")[1],'SSH':list_output_2_1}
        ###############
        list_output = list(output.split("+ echo '##########'"))
        list_output.pop(0)
        list_output.pop(len(list_output) - 1)
        output_1 = list_output[0].strip()
        output_2 = list_output[1].strip()
        list_output_1 = output_1.split("\n")
        list_output_1_1 = list_output_1[len(list_output_1) - 1].split(" ")
        list_output_2 = output_2.split("\n")
        list_output_2.pop(0)
        list_output_2.pop(0)
        list_output_2_1 = "\n".join(list_output_2)
        list_output.pop(0)
        list_output.pop(0)
        output_3 = list_output[0].strip()
        list_output_3 = output_3.split("changed: [localhost] => (item=")
        list_output_3.pop(0)
        output_3_1 = (
            list_output_3[0]
            .strip()
            .replace("u'", "'")
            .replace(")", "")
            .replace("'", '"')
            .replace("None", '"None"')
            .replace("True", '"True"')
            .replace("False", '"False"')
        )
        import json

        dict_output_3 = json.loads(output_3_1)
        #
        return {
            "IP": list_output_1_1[0],
            "USER": list_output_1_1[1].split("=")[1],
            "SSH": list_output_2_1,
            "REMOTE_ID": dict_output_3.get("id"),
        }
    except Exception:
        return False


def get_build_status_app(jobName, buil_number):
    try:
        server = Connexion(
            url=Jenkins_url, username=Jenkins_user, password=Jenkins_password
        )
        info = server.get_build_info(jobName, int(buil_number))
        print("*********************************************************")
        print(info.get("result"))
        print("*********************************************************")
        if not info :
            return "error"
        if not info.get("result"):
            return "pending"
        if info.get("result") == "SUCCESS":
            return "started"
        elif info.get("result") == "FAILURE":
            return "error"
        else:
            return f"pending"
    except Exception as e:
        return f"pending"


def CheckBuildCompleteAndSuccess():
    # pylint: disable=E1101
    applications = ApplicationCloud.objects.filter(state="pending")
    for application in applications:
        if application.build_number_for_dep:
            if not f"{application.build_number_for_dep}".isnumeric():
                continue
            try:
                application.state = get_build_status_app("application_deployer", application.build_number_for_dep)
                application.save()
                if application.state == "started":
                    asyncio.set_event_loop(asyncio.new_event_loop())
                    asyncio.run(
                        notify_customer(
                            application.owner.id,
                            f"Votre application {application.code} est maintenant disponible <br> Merci d'avoir patienter !!!!",
                        )
                    )
            except Exception as e:
                print("########################################")
                print(e)
                print("########################################")
                continue