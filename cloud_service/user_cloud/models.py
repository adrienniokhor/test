import datetime
from django.db import models
from django.contrib.auth.models import User, AbstractUser
from django.forms import ModelForm
from django.shortcuts import redirect
from django.urls import reverse
from phonenumber_field.modelfields import PhoneNumberField
from django_countries.fields import CountryField
from computedfields.models import ComputedFieldsModel, computed

#
state = [("enabled", "Activé"), ("disabled", "Désactiver")]


class UserCloud(User, ComputedFieldsModel):
    phone = PhoneNumberField("Téléphone", blank=True)
    address = models.CharField("Adresse", max_length=60)
    country = CountryField("Pays", blank_label="(choisissez votre pays)")
    enabled = models.BooleanField("Statut", default=True)

    @computed(
        models.CharField("Matricule", max_length=60, null=False, blank=True),
        depends=[("self", ["id"])],
    )
    def code(self):
        seq = f"00000000{str(self.id)}"
        seq_finale = seq[-8:]
        return f'USER_{str(datetime.date.today().strftime("%Y"))}_{seq_finale}'

    #
    # USERNAME_FIELD = 'email'
    # objects = CustomUserManager()
    # email = models.EmailField('Email', unique=True)
    #
    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    #
    def get_absolute_url(self):
        if self.code:
            return f"/adm/account/list/user/?code={self.code}"
        return reverse("admin_user")


#
class RoleCloud(ComputedFieldsModel):
    name = models.CharField("Nom", max_length=60, null=False, blank=True)
    description = models.TextField("Description")

    @computed(
        models.CharField("Code", max_length=60, null=False, blank=True),
        depends=[("self", ["id"])],
    )
    def code(self):
        seq = f"00000000{str(self.id)}"
        seq_finale = seq[-8:]
        return f'ROLE{str(datetime.date.today().strftime("%Y"))}_{seq_finale}'

    #
    def __str__(self):
        return self.name


#
class GroupCloud(ComputedFieldsModel):
    name = models.CharField("Nom", max_length=60, null=False, blank=True)
    description = models.TextField("Description")
    date_created = models.DateTimeField("Date de création", auto_now_add=True)
    state = models.CharField(
        "Etat", max_length=60, null=False, blank=True, choices=state
    )
    role = models.ManyToManyField(RoleCloud, blank=True, related_name="role_group")
    owner = models.ForeignKey(UserCloud, on_delete=models.CASCADE)

    @computed(
        models.CharField("Code", max_length=60, null=False, blank=True),
        depends=[("self", ["id"])],
    )
    def code(self):
        seq = f"00000000{str(self.id)}"
        seq_finale = seq[-8:]
        return f'GROUPE{str(datetime.date.today().strftime("%Y"))}_{seq_finale}'

    #
    def __str__(self):
        return self.name


state = [
    ("encours", "encours de traitement"),
    ("cancel", "Annulé"),
    ("validat", "Validé"),
]
demande = [
    ("Conseil / Etude", "conseil / etude"),
    ("Projet Applicatif", "projet applicatif"),
    ("Projet d’Infrastructure", "projet d’infrastructure"),
    ("Evolution d’Application", "evolution d’application"),
    ("Evolution d’Infrastructure", "evolution d’infrastructure"),
    ("Autre", "autre"),
]
critique_demande = [("Base", "base"), ("Normale", "normale"), ("Haute", "haute")]


class OffreSurMesure(ComputedFieldsModel):
    message = models.TextField("message")
    detail_demande = models.TextField("detail_demande")
    date_demande = models.DateTimeField("Date de la demande", auto_now_add=True)
    date_livraison_souhaite = models.DateField("Date de livraison souhaitée")
    date_livraison_plus_tard = models.DateField("Date de livraison au plus tard")
    type_demande = models.CharField("Type de demande", max_length=60, choices=demande)
    critique_demande = models.CharField(
        "critique de la demande", max_length=60, choices=critique_demande
    )
    condition = models.BooleanField(default=True)
    state = models.CharField(
        "Etat", max_length=60, null=False, blank=True, choices=state, default="encours"
    )
    proprietaire = models.ForeignKey(
        UserCloud,
        on_delete=models.CASCADE,
        related_name="offresur_proprietaire",
        null=True,
        blank=True,
    )

    @computed(
        models.CharField("Code", max_length=60, null=False, blank=True),
        depends=[("self", ["id"])],
    )
    def code(self):
        seq = f"00000000{str(self.id)}"
        seq_finale = seq[-8:]
        return f'OFFRE_{str(datetime.date.today().strftime("%Y"))}_{seq_finale}'

    #
    def __str__(self):
        return self.message


#
# #***********************************************************************************************************************#
#
class UserCloudForm(ModelForm):
    class Meta:
        model = UserCloud
        fields = [
            "first_name",
            "last_name",
            "email",
            "password",
            "phone",
            "address",
            "country",
        ]


#
class RoleCloudForm(ModelForm):
    class Meta:
        model = RoleCloud
        fields = ["name", "description"]


#
