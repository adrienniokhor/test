from django.shortcuts import render
from django import forms
from authentification.views import logout
from commande.models import CommandeCloud, PaiementCloud
from services.models import InstanceCloud

from user_cloud.models import UserCloud
from django.contrib import messages  # import messages
from django.contrib.sessions.models import Session
from django_countries import countries
from django.contrib.auth.forms import PasswordChangeForm
from django.core.exceptions import ValidationError
from django.contrib.auth.decorators import login_required

#

#
@login_required
def getProfil(request):
    user_connect = UserCloud.objects.get(id=request.user.id)
    user_sessions = getActiveSession(user_connect.id)
    form_password = PasswordUserChangeForm(request.user)
    form = EditUserForm(instance=user_connect)
    count_commande = CommandeCloud.objects.filter(
        proprietaire__id=user_connect.id
    ).count()
    count_instance = InstanceCloud.objects.filter(owner__id=user_connect.id).count()
    count_paiement = PaiementCloud.objects.filter(
        proprietaire__id=user_connect.id
    ).count()

    return render(
        request,
        "account.html",
        context={
            "object": "profil",
            "user_connect": user_connect,
            "user_sessions": user_sessions,
            "form": form,
            "form_password": form_password,
            "count_commandes": count_commande,
            "count_instances": count_instance,
            "count_paiements": count_paiement,
        },
    )


#
@login_required
def editProfil(request, code):
    user = UserCloud.objects.get(code=code)
    if user.id != request.user.id:
        return logout(request)
    if request.method == "POST":
        form = EditUserForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            messages.success(
                request,
                "Votre profil a été bien modifié ; Veuillez vous connecter à nouveau",
            )
            return logout(request)
        else:
            user_connect = UserCloud.objects.get(id=request.user.id)
            user_sessions = getActiveSession(user_connect.id)
            form_password = PasswordChangeForm(user_connect)
            return render(
                request,
                "account.html",
                context={
                    "object": "profil",
                    "user_connect": user_connect,
                    "user_sessions": user_sessions,
                    "form": form,
                    "tab": "editprofile",
                    "form_password": form_password,
                },
            )
        #
    return getProfil(request)


#
@login_required
def password_change(request):
    user = request.user
    if request.method == "POST":
        form_password = PasswordUserChangeForm(user, request.POST)
        if form_password.is_valid():
            form_password.save()
            messages.success(
                request,
                "Votre mot de passse a été bien modifié ; Veuillez vous connecter à nouveau",
            )
            return logout(request)
        else:
            user_connect = UserCloud.objects.get(id=request.user.id)
            user_sessions = getActiveSession(user_connect.id)
            form = EditUserForm(instance=user_connect)
            return render(
                request,
                "account.html",
                context={
                    "object": "profil",
                    "user_connect": user_connect,
                    "user_sessions": user_sessions,
                    "form": form,
                    "tab": "editpassword",
                    "form_password": form_password,
                },
            )
    return getProfil(request)


def getActiveSession(user_id):
    session_query = Session.objects.all()
    list_user_sessions = []
    # list_user_sessions.append(user_id)
    for s in session_query:
        session_data = s.get_decoded()
        if session_data.get("_auth_user_id") and user_id == int(
            session_data.get("_auth_user_id")
        ):
            list_user_sessions.append(s)
    #
    return list_user_sessions


def logoff_all(user_id):
    session_query = Session.objects.all()
    for s in session_query:
        session_data = s.get_decoded()
        if session_data.get("_auth_user_id") and user_id == int(
            session_data.get("_auth_user_id")
        ):
            s.delete()


#
@login_required
def deleteSession(request, session_key):
    sessions = Session.objects.filter(session_key=session_key)
    for elt in sessions:
        session_data = elt.get_decoded()
        if session_data.get("_auth_user_id") and request.user.id == int(
            session_data.get("_auth_user_id")
        ):
            elt.delete()
            if request.session.session_key == session_key:
                return logout(request)
    return getProfil(request)


#
class EditUserForm(forms.ModelForm):
    country_list = list(countries)
    country_list.insert(0, (None, "(choisissez votre pays)"))
    style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
    first_name = forms.CharField(
        label="First Name",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    last_name = forms.CharField(
        label="Last Name",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    email = forms.EmailField(
        label="Email",
        max_length=100,
        widget=forms.EmailInput(attrs={"class": style_class}),
    )
    phone = forms.CharField(
        label="Phone",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    address = forms.CharField(
        label="Address",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    country = forms.ChoiceField(
        label="Country",
        choices=country_list,
        required=True,
        widget=forms.Select(attrs={"class": style_class}),
    )
    #
    class Meta:
        model = UserCloud
        fields = ["first_name", "last_name", "email", "phone", "address", "country"]

    #
    def clean_email(self):
        email = self.data["email"]
        if UserCloud._default_manager.exclude(email=self.instance.email).filter(
            email=email
        ):
            raise forms.ValidationError("Cet email est déjà utilisé.")
        return email

    #
    def clean(self, *args, **kwargs):
        self.clean_email()
        return super(EditUserForm, self).clean(*args, **kwargs)


#
class PasswordUserChangeForm(PasswordChangeForm):
    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(user, *args, **kwargs)
        self.fields["old_password"].widget.attrs.update({"class": "form-control"})
        self.fields["new_password1"].widget.attrs.update({"class": "form-control"})
        self.fields["new_password2"].widget.attrs.update({"class": "form-control"})

    #
    def clean_new_password1(self):
        new_password1 = self.cleaned_data.get("new_password1")
        old_password = self.cleaned_data.get("old_password")
        if new_password1 and new_password1 == old_password:
            raise ValidationError("New password must differ from old password")
        return new_password1

    #
    def clean_password(self):
        password = self.cleaned_data["password"]
        if not self.user.check_password(password):
            raise forms.ValidationError(
                "Incorrect password.",
                code="password_incorrect",
            )
        return password

    def save(self, commit=True):
        password = self.cleaned_data["new_password1"]
        self.user.set_password(password)
        if commit:
            self.user.save()
        return self.user

    class Meta:
        model = UserCloud
        fields = ["old_password", "new_password1", "new_password2"]
