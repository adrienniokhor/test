from datetime import datetime
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from user_cloud.models import UserCloud

# Create your views here.
@login_required()
def account(request):
    context = {}
    if user_connect := UserCloud.objects.get(id=request.user.id):
        commande_count = user_connect.commande_proprietaire.all().count()
        commande_count_open = user_connect.commande_proprietaire.filter(
            state="open"
        ).count()
        commande_count_validat = user_connect.commande_proprietaire.filter(
            state__in=["validat", "paid"]
        ).count()
        commande_count_reject_or_cancel = user_connect.commande_proprietaire.filter(
            state__in=["reject", "cancel"]
        ).count()
        recent_activity_commades = user_connect.commande_proprietaire.filter(
            date_created__gte=datetime.now() - relativedelta(months=1)
        ).order_by("-id")[:5]
        context["commande"] = {
            "count": commande_count,
            "count_open": commande_count_open,
            "count_validat": commande_count_validat,
            "count_reject_or_cancel": commande_count_reject_or_cancel,
            "recent_commandes": recent_activity_commades,
        }
        #
        instance_count = user_connect.instance_proprietaire.all().count()
        instance_count_start = user_connect.instance_proprietaire.filter(
            state__in=["started", "pending"]
        ).count()
        instance_count_stop = user_connect.instance_proprietaire.filter(
            state__in=["stopped", "stopping", "open", "deleting", "revoking"]
        ).count()
        instance_count_expire_or_error = user_connect.instance_proprietaire.filter(
            state__in=["revoked", "expire", "error"]
        ).count()
        instance_time_execution = get_time_execution(user_connect)
        recent_activity_instances = user_connect.instance_proprietaire.filter(
            date_created__gte=datetime.now() - relativedelta(months=3)
        ).order_by("-id")[:5]
        context["instance"] = {
            "count": instance_count,
            "count_start": instance_count_start,
            "count_stop": instance_count_stop,
            "count_expire_or_error": instance_count_expire_or_error,
            "time_execution": instance_time_execution,
            "recent_instances": recent_activity_instances,
        }
        #
        recent_activity_paiement = user_connect.paiement_proprietaire.filter(
            date_created__gte=datetime.now() - relativedelta(months=3)
        ).order_by("-id")[:5]
        context["paiement"] = {"recent_paiements": recent_activity_paiement}

    return render(request, "account.html", context=context)


#
def get_time_execution(user):
    today_now = timezone.now()
    instances = user.instance_proprietaire.filter(
        state__in=["stopped", "stopping", "started", "pending"]
    )
    list_delta_time = [
        (instance.date_revoked - today_now).total_seconds()
        for instance in instances
        if instance.date_revoked >= today_now
    ]
    sum_seconds_execution = min(list_delta_time, default=0)
    days_seconde = 3600 * 24
    execution_days = sum_seconds_execution // (days_seconde)
    sum_seconds_execution_ = sum_seconds_execution % (3600 * 24)
    execution_hours = sum_seconds_execution_ // 3600
    execution_minutes = (sum_seconds_execution_ % 3600) // 60
    execution_secondes = (sum_seconds_execution_ % 3600) % 60
    return {
        "d": f"{int(execution_days):02d}",
        "h": f"{int(execution_hours):02d}",
        "m": "{:02d}".format(int(execution_minutes)),
        "s": f"{int(execution_secondes):02d}",
    }
