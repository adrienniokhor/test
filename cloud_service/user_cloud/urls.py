"""cloud_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from user_cloud.view_commande import actionCommande, getCommande
from user_cloud.view_intance import actionInstance, get_instance_access_key, getInstance
from user_cloud.view_message import send_message, send_reclamation
from user_cloud.view_offresurmesure import getOffresurmesure
from user_cloud.view_paiement import (
    actionPaiement,
    addPaiement,
    getPaiement,
    newPaiment,
    addPaiementWithMode,
)
from user_cloud.view_profil import deleteSession, editProfil, getProfil, password_change
from user_cloud.view_service import getService
from .views import account
from user_cloud.geneted_pdf import GeneratePdf

urlpatterns = [
    path("user/account/", account, name="account"),
    path("user/account/commande", getCommande, name="user_commande"),
    path("user/account/offresurmesure", getOffresurmesure, name="user_offresurmesure"),
    path("user/account/instance", getInstance, name="user_instance"),
    path("user/account/service", getService, name="user_service"),
    path(
        "user/account/commande/<str:code_commande>/add_paiement/",
        addPaiement,
        name="add_paiement",
    ),
    path(
        "user/account/commande/<str:code_commande>/add_paiement/<str:mode>",
        addPaiementWithMode,
        name="add_paiement_with_mode",
    ),
    path("user/account/paiement", getPaiement, name="user_paiement"),
    path("user/account/paiement/new", newPaiment, name="new_paiement"),
    # Definition des urls des Actions
    path("user/account/action/instance/", actionInstance, name="user_action_instance"),
    path("user/account/action/commande/", actionCommande, name="user_action_commande"),
    path("user/account/action/paiement/", actionPaiement, name="user_action_paiement"),
    # Generation de la facture
    path("user/account/action/facture/", GeneratePdf.as_view()),
    # Profil
    path("user/account/profil/", getProfil, name="user_profil"),
    path("user/account/profil/edit/<str:code>", editProfil, name="user_profil_edit"),
    path(
        "user/account/profil/password/edit",
        password_change,
        name="user_profil_change_password",
    ),
    path(
        "user/account/profil/session/logout/<str:session_key>",
        deleteSession,
        name="user_logout_session",
    ),
    # Get Key
    path(
        "user/account/instance/get_key/<str:code>/<str:filename>",
        get_instance_access_key,
        name="get_instance_access_key",
    ),
    # send message
    path("user/account/send-message/", send_message, name="send-message"),
    path("user/account/send-reclamation/", send_reclamation, name="send-reclamation"),
]
