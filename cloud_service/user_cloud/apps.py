from django.apps import AppConfig


class UserCloudConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "user_cloud"
