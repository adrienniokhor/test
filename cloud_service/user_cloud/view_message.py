from django.shortcuts import redirect, render
from django.contrib import messages
from django.core.mail import EmailMessage
from django.template.loader import get_template
from cloud_service.settings import EMAIL_CORI_SUPPORT
from user_cloud.forms import MessageForm
from user_cloud.models import UserCloud


base_link = "/user/account/profil/"


def send_message(request):
    if request.META.get("HTTP_HX_REQUEST") != "true":
        return redirect(base_link)
    if request.method == "POST":
        form = MessageForm(request.POST)
        if form.is_valid():
            if user_connect := UserCloud.objects.get(id=request.user.id):
                data = form.cleaned_data
                mail_subject = f"Message utilisateur ==> {data.get('subject')}"
                message = get_template("htmx/template_message.html").render(
                    {
                        "user_code": user_connect.code,
                        "user_first_name": user_connect.first_name,
                        "user_last_name": user_connect.last_name,
                        "user_email": user_connect.email,
                        "user_phone": user_connect.phone,
                        "message_content": data.get("body"),
                    }
                )
                email = EmailMessage(mail_subject, message, to=[EMAIL_CORI_SUPPORT])
                email.content_subtype = "html"
                if email.send():
                    messages.info(
                        request,
                        "Votre message est bien envoyé à l'équipe support de cori",
                    )
                    return render(request, "notification_message.html")
                else:
                    messages.warning(
                        request, "Votre message n'a pas été envoyé , veillez réessayer"
                    )

    else:
        form = MessageForm()
    return render(
        request,
        "htmx/send_message.html",
        {
            "form": form,
        },
    )


def send_reclamation(request):
    if request.META.get("HTTP_HX_REQUEST") != "true":
        return redirect(base_link)
    if request.method == "POST":
        form = MessageForm(request.POST)
        if form.is_valid():
            if user_connect := UserCloud.objects.get(id=request.user.id):
                data = form.cleaned_data
                mail_subject = f"Réclamation ==> {data.get('subject')}"
                message = get_template("htmx/template_message.html").render(
                    {
                        "user_code": user_connect.code,
                        "user_first_name": user_connect.first_name,
                        "user_last_name": user_connect.last_name,
                        "user_email": user_connect.email,
                        "user_phone": user_connect.phone,
                        "message_content": data.get("body"),
                    }
                )
                email = EmailMessage(mail_subject, message, to=[EMAIL_CORI_SUPPORT])
                email.content_subtype = "html"
                if email.send():
                    messages.info(
                        request,
                        "Votre réclamation est bien envoyé à l'équipe support de cori",
                    )
                    return render(request, "notification_message.html")
                else:
                    messages.warning(
                        request,
                        "Votre réclamation n'a pas été envoyé , veillez réessayer",
                    )
    else:
        form = MessageForm()
    return render(
        request,
        "htmx/send_reclamation.html",
        {
            "form": form,
        },
    )
