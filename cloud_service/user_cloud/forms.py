from django import forms


class MessageForm(forms.Form):
    subject = forms.CharField(required=True)
    body = forms.CharField(required=True)


class Reclamation(forms.Form):
    subject = forms.CharField(required=True)
    body = forms.CharField(required=True)
