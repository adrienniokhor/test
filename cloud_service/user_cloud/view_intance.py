import io
import mimetypes
import os
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from admin_cloud.tasks import task_SartInstance, task_StopInstance
from cloud_service.settings import KEYFILES_DIRS, PAGINATION_DEFAULT_PAGINATION
from services.models import InstanceActionValueCloud, InstanceCloud

from user_cloud.models import UserCloud
from django.core.paginator import Paginator
from django.contrib import messages  # import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist

#

# Create your views here.
@login_required
def getInstance(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/user/login")
    if not (user_connect := UserCloud.objects.get(id=request.user.id)):
        return HttpResponseRedirect("/user/login")
    if code := request.GET.get("code"):
        try:
            if instance := user_connect.instance_proprietaire.get(code=code):
                return render(
                    request,
                    "account.html",
                    context={
                        "instance_detail": instance,
                        "object": "instance",
                        "action": "list",
                    },
                )
            else:
                messages.warning(request, "Aucune instance trouvée")
        except ObjectDoesNotExist:
            messages.warning(request, "Aucune instance trouvée")
    #
    instances = user_connect.instance_proprietaire.all().order_by("-id")
    pages = Paginator(instances, PAGINATION_DEFAULT_PAGINATION)
    if page_number := request.GET.get("page"):
        page_number = int(page_number)
        if page_number > pages.num_pages or page_number < 1:
            page_number = 1
        page = pages.page(page_number)
    else:
        page = pages.page(1)
    return render(
        request,
        "account.html",
        context={"page": page, "object": "instance", "action": "list"},
    )


# ******
# messages.debug(request, '%s SQL statements were executed.')
# messages.info(request, 'Three credits remain in your account.')
# messages.success(request, 'Profile details updated.')
# messages.warning(request, 'Your account expires in three days.')
# messages.error(request, 'Document deleted.')
# ******
#
@login_required
def actionInstance(request):  # sourcery skip: low-code-quality
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/user/login")
    if not (user_connect := UserCloud.objects.get(id=request.user.id)):
        return HttpResponseRedirect("/user/login")
    #
    select_items = request.POST.getlist("select_items")
    if "arreter" in request.POST:
        list_instance_id = []
        for item in select_items:
            try:
                if instance := user_connect.instance_proprietaire.get(id=int(item)):
                    if instance.state and instance.state == "revoked":
                        messages.warning(
                            request,
                            f"Instance {instance.code}: est révoquée , donc arrêtée !!",
                        )
                    elif instance.state and instance.state == "stopped":
                        messages.warning(
                            request, f"Instance {instance.code}: est déjà arrêtée"
                        )
                    else:
                        instance.state = "stopping"
                        instance.save()
                        list_instance_id.append(item)
                        messages.info(
                            request, f"Instance {instance.code}: est en cours d'arrêt"
                        )
                else:
                    messages.warning(request, "Aucune instance trouvée")
            except ObjectDoesNotExist:
                messages.warning(request, "Aucune instance trouvée")
                continue
            #
            task_StopInstance.delay(select_items)
    #
    elif "demarrer" in request.POST:
        list_instance_id = []
        for item in select_items:
            try:
                if instance := user_connect.instance_proprietaire.get(id=int(item)):
                    if instance.state and instance.state == "started":
                        messages.warning(
                            request, f"Instance {instance.code}: est déjà démarrée"
                        )
                    elif instance.state and instance.state == "pending":
                        messages.warning(
                            request,
                            f"Instance {instance.code}: est en cours de démarrage Veiller patienter !!",
                        )
                    elif instance.state and instance.state == "revoked":
                        messages.warning(
                            request,
                            f"Instance {instance.code}: est révoquée !!",
                        )
                    else:
                        instance.state = "pending"
                        instance.save()
                        list_instance_id.append(item)
                        messages.info(
                            request,
                            f"Instance {instance.code}: est en cours de démarrage",
                        )
                #
                else:
                    messages.warning(request, "Aucune instance trouvée")
            except ObjectDoesNotExist:
                messages.warning(request, "Aucune instance trouvée")
                continue
            #
            task_SartInstance.delay(select_items)
    #
    return HttpResponseRedirect("/user/account/instance")


#
@login_required
def get_instance_access_key(request, code=None, filename=None):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/user/login")
    if not (user_connect := UserCloud.objects.get(id=request.user.id)):
        return HttpResponseRedirect("/user/login")
    if not filename:
        return HttpResponseRedirect("/user/login")
    filepath = os.path.join(KEYFILES_DIRS, filename)
    try:
        if instance_action_value := InstanceActionValueCloud.objects.get(code=code):
            user_connect.instance_proprietaire.get(
                id=int(instance_action_value.instance.id)
            )
            if not os.path.exists(filepath):
                with open(filepath, "w", encoding="utf-8") as f:
                    f.write(instance_action_value.value)
            else:
                with open(filepath, "r", encoding="utf-8") as file:
                    if file.read() != instance_action_value.value:
                        with open(filepath, "w", encoding="utf-8") as f:
                            f.write(instance_action_value.value)
            path = open(filepath, "r", encoding="utf-8")
            mime_type, _ = mimetypes.guess_type(filepath)
            response = HttpResponse(path, content_type=mime_type)
            response["Content-Disposition"] = f"attachment; filename={filename}"
            return response
    except Exception:
        file_content = io.StringIO("----*********----")
        mime_type, _ = mimetypes.guess_type(filepath)
        response = HttpResponse(file_content, content_type=mime_type)
        response["Content-Disposition"] = f"attachment; filename={filename}"
