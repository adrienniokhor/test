from django.contrib import admin

from user_cloud.models import GroupCloud, OffreSurMesure, RoleCloud, UserCloud

# Register your models here.from .models import Question

admin.site.register(UserCloud)
admin.site.register(RoleCloud)
admin.site.register(GroupCloud)
admin.site.register(OffreSurMesure)
