from cgitb import enable
import io
from django.http import HttpResponseRedirect
from django.shortcuts import render
from groupe_service.models import GroupServiceCloud
from user_cloud.models import OffreSurMesure

from user_cloud.models import UserCloud
from django.contrib.auth.decorators import login_required
from django.contrib import messages  # import messages
from django.core.exceptions import ObjectDoesNotExist

#


#
@login_required
def getOffresurmesure(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/user/login")
    if not (user_connect := UserCloud.objects.get(id=request.user.id)):
        return HttpResponseRedirect("/user/login")
    if code := request.GET.get("code"):
        try:
            if offres_sur_mesure := user_connect.offresur_proprietaire.get(code=code):
                return render(
                    request,
                    "account.html",
                    context={
                        "offresurmesure_detail": offres_sur_mesure,
                        "object": "Offresurmesuredetail",
                    },
                )
            messages.warning(request, "Aucune offre sur mesure trouvée")
        except ObjectDoesNotExist:
            messages.warning(request, "Aucune offre sur mesure trouvée")
    #
    Offresurmesure = user_connect.offresur_proprietaire.all().order_by("-id")
    return render(
        request,
        "account.html",
        context={"Offresurmesure": Offresurmesure, "object": "Offresurmesure"},
    )


#
