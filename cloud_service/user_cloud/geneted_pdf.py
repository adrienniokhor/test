# importing the necessary libraries
from datetime import datetime
from io import BytesIO
from xhtml2pdf import pisa
from django.http import HttpResponse
from django.views.generic import View
from django.template.loader import get_template
from commande.models import CommandeCloud


def html_to_pdf(template_src, context_dict=None):
    if context_dict is None:
        context_dict = {}
    template = get_template(template_src)
    html = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("utf-8")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type="application/pdf")
    return None


# Creating a class based view
class GeneratePdf(View):
    def get(self, request, *args, **kwargs):
        if code := request.GET.get("code"):
            if commande := CommandeCloud.objects.filter(code=code):
                data = commande[0]
                date_emission = datetime.now()
                if data.state == "paid":
                    state_facture = data.state
                    # open("templates/temp_user.html", "w").write(
                    #     render_to_string(
                    #         "facture_user.html",
                    #         {
                    #             "data": data,
                    #             "state_facture": state_facture,
                    #             "date_emission": date_emission,
                    #         },
                    #     )
                    # )
                    dict_context = {
                        "data": data,
                        "state_facture": state_facture,
                        "date_emission": date_emission,
                    }
                    pdf = html_to_pdf("facture_user.html", dict_context)
                else:
                    # open("templates/temp_user.html", "w").write(
                    #     render_to_string(
                    #         "facture.html",
                    #         {"data": data, "date_emission": date_emission},
                    #     )
                    # )
                    dict_context = {"data": data, "date_emission": date_emission}
                    pdf = html_to_pdf("facture.html", dict_context)
                return HttpResponse(pdf, content_type="application/pdf")
