import io
from django.http import FileResponse, HttpResponseRedirect
from django.shortcuts import render
from cloud_service.settings import PAGINATION_DEFAULT_PAGINATION
from commande.models import CommandeCloud
from user_cloud.models import UserCloud
from django.core.paginator import Paginator

#
from user_cloud.report.facture_report import PDFPSReporte
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages  # import messages

#
@login_required
def getCommande(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/user/login")
    if not (user_connect := UserCloud.objects.get(id=request.user.id)):
        return HttpResponseRedirect("/user/login")
    if code := request.GET.get("code"):
        try:
            if commande := user_connect.commande_proprietaire.get(code=code):
                action = request.GET.get("action")
                if action and action == "view":
                    buffer = io.BytesIO()
                    PDFPSReporte(buffer)
                    buffer.seek(0)
                    return FileResponse(
                        buffer, as_attachment=True, filename="hello.pdf"
                    )
                #
                prev_commande = (
                    user_connect.commande_proprietaire.filter(id__lt=commande.id)
                    .exclude(id=commande.id)
                    .order_by("-id")
                    .first()
                )
                #
                next_commande = (
                    user_connect.commande_proprietaire.filter(id__gt=commande.id)
                    .exclude(id=commande.id)
                    .order_by("id")
                    .first()
                )
                #
                return render(
                    request,
                    "account.html",
                    context={
                        "commande_detail": commande,
                        "check_paiement": check_paiment_commande(commande),
                        "next_commande": next_commande,
                        "prev_commande": prev_commande,
                        "object": "commande",
                        "action": "list",
                    },
                )
            else:
                messages.warning(request, "Aucune commande trouvée")
        except ObjectDoesNotExist:
            messages.warning(request, "Aucune commande trouvée")
    #
    commandes = user_connect.commande_proprietaire.all().order_by("-id")
    pages = Paginator(commandes, PAGINATION_DEFAULT_PAGINATION)
    if page_number := request.GET.get("page"):
        page_number = int(page_number)
        if page_number > pages.num_pages or page_number < 1:
            page_number = 1
        page = pages.page(page_number)
    else:
        page = pages.page(1)
    return render(
        request,
        "account.html",
        context={"page": page, "object": "commande", "action": "list"},
    )


#
@login_required
def actionCommande(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/user/login")
    if not (user_connect := UserCloud.objects.get(id=request.user.id)):
        return HttpResponseRedirect("/user/login")
    #
    dict_state = {
        "open": "Ouvert",
        "cancel": "Annulé",
        "validat": "Validé",
        "paid": "Payé",
    }
    if "valider" in request.POST:
        select_items = request.POST.getlist("select_items")
        for item in select_items:
            try:
                commande = user_connect.commande_proprietaire.get(id=int(item))
                if commande and commande.state == "cancel":
                    commande.state = "open"
                    commande.save()
                    messages.success(
                        request, f"Commande {commande.code} est bien validé"
                    )
                else:
                    messages.warning(
                        request,
                        f"Commande {commande.code} est {dict_state.get(commande.state)}, donc déja validé",
                    )
            except ObjectDoesNotExist:
                messages.warning(request, "Aucune commande trouvée")
                continue
    #
    elif "annuler" in request.POST:
        select_items = request.POST.getlist("select_items")
        for item in select_items:
            try:
                commande = user_connect.commande_proprietaire.get(id=int(item))
                if commande and commande.state == "open":
                    commande.state = "cancel"
                    commande.save()
                    messages.success(
                        request, f"Commande {commande.code} est bien annulée"
                    )
                else:
                    messages.warning(
                        request,
                        f"Commande {commande.code} est {dict_state.get(commande.state)}, donc ne peut pas être annuler",
                    )
            except ObjectDoesNotExist:
                messages.warning(request, "Aucune commande trouvée")
                continue
    #
    elif "supprimer" in request.POST:
        select_items = request.POST.getlist("select_items")
        for item in select_items:
            try:
                commande = user_connect.commande_proprietaire.get(id=int(item))
                if commande and commande.state in ["cancel", "open"]:
                    commande.delete()
                    messages.success(
                        request, f"Commande {commande.code} est bien suprimée"
                    )
                else:
                    messages.warning(
                        request,
                        f"Commande {commande.code} est {dict_state.get(commande.state)}, donc ne peut pas être supprimer",
                    )
            except ObjectDoesNotExist:
                messages.warning(request, "Aucune commande trouvée")
                continue
    #
    return HttpResponseRedirect("/user/account/commande")


#
def check_paiment_commande(command):
    if paiement_commande := command.commande_paiement.all():
        for paiement in paiement_commande:
            if paiement.state in ["open", "validat"]:
                return True
    return False


#
def bezier2(canvas):
    from reportlab.lib.colors import yellow, green, red, black
    from reportlab.lib.units import inch

    # make a sequence of control points
    xd, yd = 5.5 * inch / 2, 3 * inch / 2
    xc, yc = xd, yd
    dxdy = [
        (0, 0.33),
        (0.33, 0.33),
        (0.75, 1),
        (0.875, 0.875),
        (0.875, 0.875),
        (1, 0.75),
        (0.33, 0.33),
        (0.33, 0),
    ]
    pointlist = []
    for xoffset in (1, -1):
        yoffset = xoffset
        for (dx, dy) in dxdy:
            px = xc + xd * xoffset * dx
            py = yc + yd * yoffset * dy
            pointlist.append((px, py))
        yoffset = -xoffset
        for (dy, dx) in dxdy:
            px = xc + xd * xoffset * dx
            py = yc + yd * yoffset * dy
            pointlist.append((px, py))
    # draw tangent lines and curves
    canvas.setLineWidth(inch * 0.1)
    while pointlist:
        [(x1, y1), (x2, y2), (x3, y3), (x4, y4)] = pointlist[:4]
        del pointlist[:4]
        canvas.setLineWidth(inch * 0.1)
        canvas.setStrokeColor(green)
        canvas.line(x1, y1, x2, y2)
        canvas.setStrokeColor(red)
        canvas.line(x3, y3, x4, y4)
        # finally draw the curve
        canvas.setStrokeColor(black)
        canvas.bezier(x1, y1, x2, y2, x3, y3, x4, y4)
    #
    return canvas
