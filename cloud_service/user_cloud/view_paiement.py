import contextlib
from datetime import datetime, date
from django import forms
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from kombu.exceptions import OperationalError
from redis.exceptions import ConnectionError
from analytics.models import Event
from cloud_service.settings import PAGINATION_DEFAULT_PAGINATION
from cloud_service.utils import notify_on_paiement
from commande.models import CommandeCloud, ModePaiementCloud, PaiementCloud
from paiement_api.views import addPaiementAutomatic
from user_cloud.models import UserCloud

#
devise = [("fcfa", "FCFA")]


# Create your views here.
@login_required
def addPaiement(request, code_commande):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/user/login")
    if not (user_connect := UserCloud.objects.get(id=request.user.id)):
        return HttpResponseRedirect("/user/login")
    if code_commande:
        if commande := user_connect.commande_proprietaire.filter(code=code_commande):
            commande = commande[0]
            if request.method == "POST":
                default_data = {
                    "commande": commande,
                    "date_paiement": date.today(),
                    "montant": commande.total_price,
                }
                form_paiement = AddPaiementForm(request.POST, initial=default_data)
                if not form_paiement.is_valid():
                    return render(
                        request,
                        "account.html",
                        context={
                            "commande_detail": commande,
                            "form_paiement": form_paiement,
                            "action": "add_paiement",
                            "object": "commande",
                        },
                    )
                data = form_paiement.cleaned_data
                # pylint: disable=E1101
                paiement = PaiementCloud.objects.create(
                    date_created=datetime.now(),
                    date_paiement=data.get("date_paiement"),
                    commande=data.get("commande"),
                    mode_paiement=data.get("mode_paiement"),
                    numTransaction_or_numCarte=data.get("numTransaction_or_numCarte"),
                    state="open",
                    devise=data.get("devise"),
                    montant=data.get("montant"),
                    proprietaire=user_connect,
                )
                paiement.save()
                create_paiement_event = Event(
                    name="Create Paiement",
                    object="paiement",
                    code=paiement.code,
                    created=datetime.now(),
                )
                create_paiement_event.save()
                if not paiement:
                    return HttpResponseRedirect("/user/account/commande")
                commande.state = "validat"
                commande.save()
                messages.info(
                    request,
                    f"Paiement {paiement.code}: est bien effectué les ressources demandées seront disponibles après validation de ce dernier par l'équipe de cori",
                )
                with contextlib.suppress(OperationalError, ConnectionError):
                    notify_on_paiement.delay(paiement.id)
                return HttpResponseRedirect(
                    f"/user/account/commande?code={commande.code}"
                )
            else:
                default_data = {
                    "commande": commande,
                    "date_paiement": date.today(),
                    "montant": commande.total_price,
                }
                form_paiement = AddPaiementForm(initial=default_data)
                return render(
                    request,
                    "account.html",
                    context={
                        "commande_detail": commande,
                        "form_paiement": form_paiement,
                        "action": "add_paiement",
                        "object": "commande",
                    },
                )
        return HttpResponseRedirect("/user/account/commande")
    return HttpResponseRedirect("/user/account/commande")


#
@login_required
def addPaiementManual(request, code_commande):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/user/login")
    if not (user_connect := UserCloud.objects.get(id=request.user.id)):
        return HttpResponseRedirect("/user/login")
    if code_commande:
        try:
            if commande := user_connect.commande_proprietaire.get(code=code_commande):
                if request.method == "POST":
                    default_data = {
                        "commande": commande,
                        "date_paiement": date.today(),
                        "montant": commande.total_price,
                    }
                    form_paiement = AddPaiementForm(request.POST, initial=default_data)
                    if not form_paiement.is_valid():
                        return render(
                            request,
                            "account.html",
                            context={
                                "commande_detail": commande,
                                "form_paiement": form_paiement,
                                "action": "add_paiement",
                                "object": "commande",
                            },
                        )
                    data = form_paiement.cleaned_data
                    # pylint: disable=E1101
                    paiement = PaiementCloud.objects.create(
                        date_created=datetime.now(),
                        date_paiement=data.get("date_paiement"),
                        commande=data.get("commande"),
                        mode_paiement=data.get("mode_paiement"),
                        numTransaction_or_numCarte=data.get(
                            "numTransaction_or_numCarte"
                        ),
                        state="open",
                        devise=data.get("devise"),
                        montant=data.get("montant"),
                        proprietaire=user_connect,
                    )
                    paiement.save()
                    create_paiement_event = Event(
                        name="Create Paiement",
                        object="paiement",
                        code=paiement.code,
                        created=datetime.now(),
                    )
                    create_paiement_event.save()
                    if not paiement:
                        return HttpResponseRedirect("/user/account/commande")
                    commande.state = "validat"
                    commande.save()
                    messages.success(
                        request,
                        f"Paiement {paiement.code }: est bien effectué les ressources demandées seront disponibles après validation de ce dernier par l'équipe de cori",
                    )
                    notify_on_paiement(commande, paiement)
                    return HttpResponseRedirect(
                        f"/user/account/commande?code={commande.code}"
                    )
                else:
                    default_data = {
                        "commande": commande,
                        "date_paiement": date.today(),
                        "montant": commande.total_price,
                    }
                    form_paiement = AddPaiementForm(initial=default_data)
                    return render(
                        request,
                        "account.html",
                        context={
                            "commande_detail": commande,
                            "form_paiement": form_paiement,
                            "action": "add_paiement",
                            "object": "commande",
                        },
                    )
        # pylint: disable=E1101
        except CommandeCloud.DoesNotExist:
            messages.warning(request, "Aucune commande trouvée")
        return HttpResponseRedirect("/user/account/commande")


#
@login_required
def addPaiementWithMode(request, code_commande, mode):
    proprietaire = UserCloud.objects.filter(pk=request.user.id)[0]
    if not request.user.is_authenticated or proprietaire.id != request.user.id:
        return HttpResponseRedirect("/user/login")
    if code_commande and mode:
        if mode == "manual":
            return addPaiementManual(request, code_commande)
        elif mode == "automatic":
            return addPaiementAutomatic(code_commande)
        else:
            messages.warning(request, "Erreur , mode de paiement non pris en charger")
            return redirect(f"/user/account/commande?code={code_commande}")
    return HttpResponseRedirect("/user/account/commande")


#
@login_required
def getPaiement(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/user/login")
    if not (user_connect := UserCloud.objects.get(id=request.user.id)):
        return HttpResponseRedirect("/user/login")
    #
    paiements = user_connect.paiement_proprietaire.all().order_by("-id")
    pages = Paginator(paiements, PAGINATION_DEFAULT_PAGINATION)
    if page_number := request.GET.get("page"):
        page_number = int(page_number)
        if page_number > pages.num_pages or page_number < 1:
            page_number = 1
        page = pages.page(page_number)
    else:
        page = pages.page(1)
    return render(
        request,
        "account.html",
        context={"page": page, "object": "paiement", "action": "list"},
    )


#
@login_required
def newPaiment(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/user/login")
    if not (user_connect := UserCloud.objects.get(id=request.user.id)):
        return HttpResponseRedirect("/user/login")
    if request.method == "POST":
        default_data = {"date_paiement": date.today()}
        form_paiement = AddPaiementForm2(request.POST, initial=default_data)
        if not form_paiement.is_valid():
            return render(
                request,
                "account.html",
                context={
                    "object": "paiement",
                    "action": "add",
                    "form_paiement": form_paiement,
                },
            )
        data = form_paiement.cleaned_data
        # pylint: disable=E1101
        paiement = PaiementCloud.objects.create(
            date_created=datetime.now(),
            date_paiement=data.get("date_paiement"),
            commande=data.get("commande"),
            mode_paiement=data.get("mode_paiement"),
            numTransaction_or_numCarte=data.get("numTransaction_or_numCarte"),
            state="open",
            devise=data.get("devise"),
            montant=data.get("montant"),
            proprietaire=user_connect,
        )
        paiement.save()
        if not paiement:
            return HttpResponseRedirect("/user/account/paiement")
        if commande := CommandeCloud.objects.get(code=data.get("commande")):
            commande.state = "validat"
            commande.save()
        return HttpResponseRedirect("/user/account/paiement")
    else:
        default_data = {
            "date_paiement": date.today(),
        }
        form_paiement = AddPaiementForm2(initial=default_data)
        return render(
            request,
            "account.html",
            context={
                "object": "paiement",
                "action": "add",
                "form_paiement": form_paiement,
            },
        )


#
@login_required
def actionPaiement(request):
    if not (proprietaire := UserCloud.objects.filter(id=request.user.id)):
        return HttpResponseRedirect("/user/login")
    if request.method == "POST":
        pass
        #
        # if 'valider' in request.POST:
        #     select_items = request.POST.getlist('select_items')
        #     for item in select_items:
        #         try:
        #             paiement = PaiementCloud.objects.get(id=int(item),proprietaire=request.user.id)
        #             if paiement.state == "open":
        #                 pass
        #         except PaiementCloud.DoesNotExist:
        #             continue

        # #
        # elif 'annuler' in request.POST:
        #     select_items = request.POST.getlist('select_items')
        #     print(select_items)
        #     for item in select_items:
        #         paiement = PaiementCloud.objects.filter(id=int(item),proprietaire=request.user.id)[0]
        # #
        # elif 'supprimer' in request.POST:
        #     select_items = request.POST.getlist('select_items')
        #     print(select_items)
        #     for item in select_items:
        #         try:
        #             paiement = PaiementCloud.objects.get(id=int(item),proprietaire=request.user.id)
        #             if paiement.state == "open":
        #                 paiement.delete()
        #                 messages.success(request, f"Paiement {paiement.code} est bien supprimé")
        #             else:
        #                 messages.warning(request, f"Paiement {paiement.code} est {paiement.state}, donc ne peut pas être supprimer")
        #         except PaiementCloud.DoesNotExist:
        #             continue
    #     #
    #     elif 'rejeter' in request.POST:
    #         select_items = request.POST.getlist('select_items')
    #         print(select_items)
    #         for item in select_items:
    #             paiement = PaiementCloud.objects.get(id=int(item))
    #         #
    return HttpResponseRedirect("/user/account/paiement")


#
# ##
class AddPaiementForm(forms.Form):

    style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
    # code = forms.CharField(label='Nom', max_length=100,widget=forms.TextInput(attrs={'class':style_class}))
    # date_created = forms.DateTimeField(label='Date Création',widget=forms.DateInput(attrs={'class':style_class}))
    date_paiement = forms.DateField(
        label="Date Paiement", widget=forms.DateInput(attrs={"class": style_class})
    )
    commande = forms.ModelChoiceField(
        label="Commande",
        # pylint: disable=E1101
        queryset=CommandeCloud.objects.all(),
        disabled=True,
        widget=forms.Select(attrs={"class": style_class}),
    )
    mode_paiement = forms.ModelChoiceField(
        # pylint: disable=E1101
        label="Mode de Paiement",
        queryset=ModePaiementCloud.objects.all(),
        widget=forms.Select(attrs={"class": style_class}),
    )
    numTransaction_or_numCarte = forms.CharField(
        label="Numéro Transaction ou Numero de carte",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    montant = forms.FloatField(
        label="Montant",
        widget=forms.NumberInput(attrs={"class": style_class}),
        disabled=True,
    )
    devise = forms.CharField(
        label="Devis",
        max_length=100,
        widget=forms.Select(choices=devise, attrs={"class": style_class}),
    )

    class Meta:
        model = PaiementCloud


#
class AddPaiementForm2(forms.Form):

    style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
    # code = forms.CharField(label='Nom', max_length=100,widget=forms.TextInput(attrs={'class':style_class}))
    # date_created = forms.DateTimeField(label='Date Création',widget=forms.DateInput(attrs={'class':style_class}))
    date_paiement = forms.DateField(
        label="Date Paiement", widget=forms.DateInput(attrs={"class": style_class})
    )
    commande = forms.ModelChoiceField(
        label="Commande",
        # pylint: disable=E1101
        queryset=CommandeCloud.objects.all(),
        widget=forms.Select(attrs={"class": style_class}),
    )
    mode_paiement = forms.ModelChoiceField(
        label="Mode de Paiement",
        # pylint: disable=E1101
        queryset=ModePaiementCloud.objects.all(),
        widget=forms.Select(attrs={"class": style_class}),
    )
    numTransaction_or_numCarte = forms.CharField(
        label="Numéro Transaction ou Numero de carte",
        max_length=100,
        widget=forms.TextInput(attrs={"class": style_class}),
    )
    montant = forms.FloatField(
        label="Montant", widget=forms.NumberInput(attrs={"class": style_class})
    )
    devise = forms.CharField(
        label="Devis",
        max_length=100,
        widget=forms.Select(choices=devise, attrs={"class": style_class}),
    )

    class Meta:
        model = PaiementCloud
