from cgitb import enable
import io
from django.http import HttpResponseRedirect
from django.shortcuts import render
from groupe_service.models import GroupServiceCloud
from commande.models import CommandeCloud

from user_cloud.models import UserCloud
from django.contrib.auth.decorators import login_required

#

#
@login_required
def getService(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/user/login")
    grp_service = GroupServiceCloud.objects.filter(enabled=True)
    return render(
        request,
        "account.html",
        context={"user_services": grp_service, "object": "service", "action": "list"},
    )
