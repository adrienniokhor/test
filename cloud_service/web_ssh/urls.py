"""cloud_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from web_ssh.views import index, upload_ssh_key, web_console


urlpatterns = [
    # Web SSH
    path("user/account/instance/console_cori", index, name="web_ssh_console"),
    path("upload_ssh_key/", upload_ssh_key, name="web_ssh_upload_ssh_key"),
    path(
        "user/account/instance/ssh/<str:instance_id>/",
        web_console,
        name="web_ssh_console_instance",
    ),
]
