from django.apps import AppConfig


class WebSshConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "web_ssh"
