#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import random
import hashlib


def get_key_obj(pkeyobj, pkey_file=None, pkey_obj=None, password=None):
    if pkey_file:
        with open(pkey_file) as fo:
            try:
                return pkeyobj.from_private_key(fo, password=password)
            except Exception:
                pass
    else:
        try:
            return pkeyobj.from_private_key(pkey_obj, password=password)
        except Exception:
            pkey_obj.seek(0)


def unique():
    ctime = str(time.time())
    salt = str(random.random())
    m = hashlib.md5(bytes(salt, encoding="utf-8"))
    m.update(bytes(ctime, encoding="utf-8"))
    return m.hexdigest()
