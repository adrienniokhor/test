from django.urls import re_path
from notifications.consumer_notify import ChatConsumer

from web_ssh.tools.channel import websocket_cori

websocket_urlpatterns = [
    re_path("webssh/", websocket_cori.WebSSHCori.as_asgi()),
    re_path("notify/", ChatConsumer.as_asgi()),
]
