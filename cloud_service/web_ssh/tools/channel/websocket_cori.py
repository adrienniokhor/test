from channels.generic.websocket import WebsocketConsumer
from web_ssh.tools.ssh import SSH
from django.http.request import QueryDict
from six import StringIO
from cloud_service.settings import KEYFILES_DIRS, TMP_DIR
import os
import json
import base64
from services.models import InstanceActionValueCloud, InstanceCloud
from user_cloud.models import UserCloud
from django.contrib.sessions.models import Session

from web_ssh.tools.ssh_web import SSHWEB


class WebSSHCori(WebsocketConsumer):
    message = {"status": 0, "message": None}
    """
    status:
        0 : la connexion ssh est normale, le websocket est normal
        1 : Une erreur inconnue s'est produite, fermant les connexions ssh et websocket
    message:
        Lorsque le statut est 1, message est le message d'erreur spécifique
        Lorsque le statut est 0, le message est les données renvoyées par ssh, et la page frontale obtiendra les données renvoyées par ssh et les écrira sur la page du terminal
    """

    def __init__(self, *args, **kwargs):
        self.ssh = None
        super().__init__(args, kwargs)

    def connect(self):
        """
        Ouvrez la connexion websocket et essayez de vous connecter à l'hôte ssh via les paramètres transmis depuis le frontal
        :return:
        """
        self.accept()
        query_string = self.scope.get("query_string")
        ssh_args = QueryDict(query_string=query_string, encoding="utf-8")

        width = ssh_args.get("width")
        height = ssh_args.get("height")
        width = int(width)
        height = int(height)
        instance_id = ssh_args.get("instance_id")
        user_id = ssh_args.get("user_id")
        session_id = ssh_args.get("session_key")
        ssh_key_name = f"{instance_id}_access_key.pem"
        user_connect = UserCloud.objects.get(code=user_id)
        session = Session.objects.get(session_key=session_id)
        session_data = session.get_decoded()
        if session_data.get("_auth_user_id") and user_connect.id == int(
            session_data.get("_auth_user_id")
        ):
            if instance := user_connect.instance_proprietaire.get(code=instance_id):
                self.extracted_from_connect_1(instance, width, height, ssh_key_name)

    # TODO Rename this here and in `connect`
    def extracted_from_connect_1(self, instance, width, height, ssh_key_name):
        action_ip = instance.instance_action_value_instance.get(action__name="IP")
        host = action_ip.value
        user = "ec2-user"
        self.ssh = SSHWEB(websocker=self, message=self.message)

        port = 22
        ssh_connect_dict = {
            "host": host,
            "user": user,
            "port": port,
            "timeout": 30,
            "pty_width": width,
            "pty_height": height,
        }
        self.extracted_from_connect(ssh_key_name, ssh_connect_dict)
        self.ssh.connect(**ssh_connect_dict)

    #
    def extracted_from_connect(self, ssh_key_name, ssh_connect_dict):
        """get content file ssh key"""
        try:
            ssh_key_file = os.path.join(KEYFILES_DIRS, ssh_key_name)
            if not os.path.exists(ssh_key_file):
                action_code = ssh_key_name.replace("_access_key.pem", "")
                instance_action_value = InstanceActionValueCloud.objects.get(
                    instance__code=action_code, action__name="SSH"
                )
                with open(ssh_key_file, "w", encoding="utf-8") as f:
                    f.write(instance_action_value.value)
            with open(f"{ssh_key_file}", "r", encoding="utf-8") as file_ssh_key:
                string_io = StringIO()
                string_io.write(file_ssh_key.read())
                string_io.flush()
                string_io.seek(0)
                ssh_connect_dict["ssh_key"] = string_io
        except Exception as e:
            string_io = StringIO()
            ssh_connect_dict["ssh_key"] = string_io

    #

    def disconnect(self, code):
        try:
            self.ssh.close()
        except Exception as except_deconnect:
            print(except_deconnect)

    #

    def receive(self, text_data=None, bytes_data=None):
        data = json.loads(text_data)
        if isinstance(data, dict):
            status = data["status"]
            if status == 0:
                data = data["data"]
                self.ssh.shell(data)
            else:
                cols = data["cols"]
                rows = data["rows"]
                self.ssh.resize_pty(cols=cols, rows=rows)
