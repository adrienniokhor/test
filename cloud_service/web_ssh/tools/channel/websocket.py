from channels.generic.websocket import WebsocketConsumer
from web_ssh.tools.ssh import SSH
from django.http.request import QueryDict
from six import StringIO
from cloud_service.settings import TMP_DIR
import os
import json
import base64


class WebSSH(WebsocketConsumer):
    message = {"status": 0, "message": None}
    """
    status:
        0 : la connexion ssh est normale, le websocket est normal
        1 : Une erreur inconnue s'est produite, fermant les connexions ssh et websocket
    message:
        Lorsque le statut est 1, message est le message d'erreur spécifique
        Lorsque le statut est 0, le message est les données renvoyées par ssh, et la page frontale obtiendra les données renvoyées par ssh et les écrira sur la page du terminal
    """

    def __init__(self, *args, **kwargs):
        self.ssh = None
        super().__init__(args, kwargs)

    def connect(self):
        """
        Ouvrez la connexion websocket et essayez de vous connecter à l'hôte ssh via les paramètres transmis depuis le frontal
        :return:
        """
        self.accept()
        query_string = self.scope.get("query_string")
        ssh_args = QueryDict(query_string=query_string, encoding="utf-8")

        width = ssh_args.get("width")
        height = ssh_args.get("height")
        port = ssh_args.get("port")

        width = int(width)
        height = int(height)
        port = int(port)

        auth = ssh_args.get("auth")
        ssh_key_name = ssh_args.get("ssh_key")
        passwd = ssh_args.get("password")

        host = ssh_args.get("host")
        user = ssh_args.get("user")

        passwd = base64.b64decode(passwd).decode("utf-8") if passwd else None
        self.ssh = SSH(websocker=self, message=self.message)

        ssh_connect_dict = {
            "host": host,
            "user": user,
            "port": port,
            "timeout": 30,
            "pty_width": width,
            "pty_height": height,
            "password": passwd,
        }

        if auth == "key":
            self.extracted_from_connect(ssh_key_name, ssh_connect_dict)
        self.ssh.connect(**ssh_connect_dict)

    #
    def extracted_from_connect(self, ssh_key_name, ssh_connect_dict):
        """get content file ssh key"""
        ssh_key_file = os.path.join(TMP_DIR, ssh_key_name)
        with open(f"{ssh_key_file}", "r", encoding="utf-8") as file_ssh_key:
            string_io = StringIO()
            string_io.write(file_ssh_key.read())
            string_io.flush()
            string_io.seek(0)
            ssh_connect_dict["ssh_key"] = string_io

    #

    def disconnect(self):
        try:
            self.ssh.close()
        except Exception as except_deconnect:
            print(except_deconnect)

    #

    def receive(self, text_data=None, bytes_data=None):
        data = json.loads(text_data)
        if isinstance(data, dict):
            status = data["status"]
            if status == 0:
                data = data["data"]
                self.ssh.shell(data)
            else:
                cols = data["cols"]
                rows = data["rows"]
                self.ssh.resize_pty(cols=cols, rows=rows)
