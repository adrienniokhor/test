from django.http import HttpResponseRedirect
from django.shortcuts import render, HttpResponse
from user_cloud.models import UserCloud
from web_ssh.tools.tools import unique
from cloud_service.settings import TMP_DIR
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist

import os


def index(request):
    return render(request, "web_ssh_index.html")


#
@login_required
def web_console(request, instance_id):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/user/login")
    if not (user_connect := UserCloud.objects.get(id=request.user.id)):
        return HttpResponseRedirect("/user/login")
    try:
        if instance := user_connect.instance_proprietaire.get(code=instance_id):
            return render(
                request,
                "web_ssh.html",
                context={
                    "instance_id": instance.code,
                    "user_id": user_connect.code,
                    "session_key": request.session.session_key,
                },
            )
    except ObjectDoesNotExist:
        return render(
            request,
            "web_ssh.html",
            context={"error": "Erreur vous n'etes pas autorise a acceder a cette page"},
        )
    # return render(request, 'web_ssh.html')


def upload_ssh_key(request):
    # return HttpResponse('test')
    if request.method == "POST":
        pkey = request.FILES.get("pkey")
        ssh_key = pkey.read().decode("utf-8")
        filename = unique()
        ssh_key_path = os.path.join(TMP_DIR, filename)
        with open(ssh_key_path, "w") as f:
            f.write(ssh_key)
        return HttpResponse(filename)
