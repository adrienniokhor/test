from django.apps import AppConfig


class GroupeServiceConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "groupe_service"
