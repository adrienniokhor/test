"""cloud_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from .views import (
    CountPanierContent,
    ajouter_application_panier,
    get_forfait_for_app,
    goupe_service,
    loadService,
    ajouter_service_panier,
    loadPanier,
    validerPanier,
    loadPanierserviceaddqtt,
    loadPanierserviceremoveqtt,
    offresurmesure,
)

urlpatterns = [
    path("", goupe_service),
    path("offresurmesure/", offresurmesure, name="add_offre_sur_mesure"),
    path("<str:groupe_service>/<str:service>/", loadService),
    path("<str:groupe_service>/<str:service>/add_panier/", ajouter_service_panier),
    path(
        "page/application/forfait/<str:application>/<str:package>",
        get_forfait_for_app,
        name="application_forfait",
    ),
    path(
        "panier/add-app/<str:application_name>/<str:package>/",
        ajouter_application_panier,
        name="ajouter_application_panier",
    ),
    path("panier_services/", loadPanier, name="panier_services"),
    path(
        "panier_services_add_qtt/",
        loadPanierserviceaddqtt,
        name="panier_services_add_qtt",
    ),
    path(
        "panier_services_remove_qtt/",
        loadPanierserviceremoveqtt,
        name="panier_services_remove_qtt",
    ),
    path("valider_panier/", validerPanier, name="valider_panier"),
    path(
        "panier_services_count_content/",
        CountPanierContent,
        name="count_panier_content",
    ),
]
