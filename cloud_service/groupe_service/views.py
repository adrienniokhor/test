from datetime import datetime
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render

from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import get_template
from django.http import JsonResponse
from django.contrib import messages
from django.core.mail import EmailMessage

from admin_cloud.models import ParameterCloud
from analytics.models import Event
from commande.models import CommandeCloud, LigneCommandeAppCloud, LigneCommandeCloud
from groupe_service.models import GroupServiceCloud
from groupe_service.forms import OffreSurMesureForm
from services.models import ForfaitCloud, ServiceCloud
from user_cloud.models import UserCloud
from user_cloud.models import OffreSurMesure
from cloud_service.settings import DICT_APPLICATION, DICT_PACKAGE, EMAIL_CORI_SUPPORT



def goupe_service(request):
    name = request.GET.get("name")
    service = request.GET.get("service")
    methode = request.GET.get("methode")
    if not (grp_services := GroupServiceCloud.objects.filter(name=name, enabled=True)):
        return HttpResponseRedirect("/")
    grp_service = grp_services[0]
    if not name:
        return HttpResponseRedirect("/")
    if name == "Offre sur Mesure":
        form = OffreSurMesureForm()
        return render(
            request, "groupe_service_personnaliser.html", context={"form": form}
        )

    return (
        render(
            request,
            "groupe_service.html",
            context={
                "services": grp_service.services_groupe_service.filter(enabled=True),
                "groupe_service": grp_service,
                "methode": methode or "list",
                "service": service,
            },
        )
        if methode
        else HttpResponseRedirect(
            f"/groupe_service/?name={name}&methode=list",
            {"list_service": grp_service.services_groupe_service.filter(enabled=True)},
        )
    )


def offresurmesure(request):
    if request.method != "POST":
        form = OffreSurMesureForm()
        return render(
            request, "groupe_service_personnaliser.html", context={"form": form}
        )
    else:
        if not request.user.is_authenticated:
            return HttpResponseRedirect(
                "/user/login?next_page=/groupe_service/offresurmesure/"
            )
        form = OffreSurMesureForm(request.POST)
        return (
            save_offer_and_notify(request, form)
            if form.is_valid()
            else render(
                request,
                "groupe_service_personnaliser.html",
                context={"form": form},
            )
        )


def save_offer_and_notify(request, form):
    user_connect = UserCloud.objects.filter(id=request.user.id)
    proprietaire = user_connect[0]
    data = form.cleaned_data
    offre_sur_mesure = OffreSurMesure.objects.create(
        message=data["message"],
        detail_demande=data["detail_demande"],
        type_demande=data["type_demande"],
        critique_demande=data["critique_demande"],
        condition=data["condition"],
        date_demande=datetime.now(),
        date_livraison_souhaite=data["date_livraison_souhaite"],
        date_livraison_plus_tard=data["date_livraison_plus_tard"],
        proprietaire=proprietaire,
    )
    offre_sur_mesure.save()
    event = Event.objects.create(
        name="Create Offre",
        object="offre",
        code=offre_sur_mesure.code,
        created=datetime.now()
    )
    event.save()
    messages.success(
        request,
        (
            f"Cher {proprietaire.first_name} {proprietaire.last_name}, l'équipe d'administration de Coridigital étudie votre demande. Vous obtiendrai une réponse dans 48H"
        )
    )
    current_site = get_current_site(request)
    mail_subject = "Validation d'une commande Offre sur Mesure "
    message = get_template("email_notification_offre_sur_mesure.html").render(
        {
            "commande_code": offre_sur_mesure.code,
            "user_first_name": proprietaire.first_name,
            "user_last_name": proprietaire.last_name,
            "user_email": proprietaire.email,
            "user_phone": proprietaire.phone,
            "message": offre_sur_mesure.message,
            "detail_demande":offre_sur_mesure.detail_demande,
            "type_demande":offre_sur_mesure.type_demande,
            "critique_demande":offre_sur_mesure.critique_demande,
            "date_livraison_souhaite":offre_sur_mesure.date_livraison_souhaite,
            "date_livraison_plus_tard":offre_sur_mesure.date_livraison_plus_tard,
        }
    )
    to_email = EMAIL_CORI_SUPPORT
    email = EmailMessage(mail_subject, message, to=[to_email])
    email.content_subtype = "html"
    email.send()
    return HttpResponseRedirect("/user/account/offresurmesure")


def loadService(request, groupe_service, service):
    methode = request.GET.get("methode")
    if grp_services := GroupServiceCloud.objects.filter(
        name=groupe_service, enabled=True
    ):
        grp_service = grp_services[0]
        if not groupe_service:
            return HttpResponseRedirect("/")
        services = grp_service.services_groupe_service.filter(
            name=service, enabled=True
        )
        if not services:
            return HttpResponseRedirect(
                f"/groupe_service/?name={grp_service.name}&methode=list"
            )

        if not methode or methode != "form":
            return HttpResponseRedirect(
                f"/groupe_service/?name={grp_service.name}&methode=list"
            )
        forfaits = ForfaitCloud.objects.filter(enabled=True)
        return render(
            request,
            "groupe_service.html",
            context={
                "groupe_service": grp_service,
                "methode": "form",
                "service": services[0],
                "forfaits": forfaits,
            },
        )
    else:
        return HttpResponseRedirect("/")


#
def ajouter_service_panier(request, groupe_service, service):
    srv = (
        srvs
        if (
            srvs := ServiceCloud.objects.get(
                name=service, groupe_service__name=groupe_service
            )
        )
        else None
    )

    if not srv:
        return HttpResponseRedirect(
            f"/groupe_service/?name={groupe_service}&methode=list"
        )

    panier = {} if "panier" not in request.session else request.session["panier"]
    forfait = int(request.POST.get("forfait")) or 0
    if str(srv.id) in panier:
        if str(forfait) in panier[str(srv.id)]:
            if forfait == 0:
                panier[str(srv.id)][str(forfait)]["quantite"] = (
                    panier[str(srv.id)][str(forfait)]["quantite"] + 1
                )

            elif forfait_select := ForfaitCloud.objects.filter(id=forfait):
                forfait_select = forfait_select[0]
                panier[str(srv.id)][str(forfait_select.id)]["quantite"] = (
                    panier[str(srv.id)][str(forfait_select.id)]["quantite"] + 1
                )

        elif forfait == 0:
            panier[str(srv.id)][str(forfait)] = {
                "durree": 1,
                "reduction": 0,
                "quantite": 1,
            }

        elif forfait_select := ForfaitCloud.objects.filter(id=forfait):
            forfait_select = forfait_select[0]
            panier[str(srv.id)][str(forfait_select.id)] = {
                "durree": forfait_select.durree,
                "reduction": forfait_select.reduction,
                "quantite": 1,
            }

    elif forfait == 0:
        panier[str(srv.id)] = {
            str(forfait): {"durree": 1, "reduction": 0, "quantite": 1}
        }

    elif forfait_select := ForfaitCloud.objects.filter(id=forfait):
        forfait_select = forfait_select[0]
        panier[str(srv.id)] = {
            str(forfait_select.id): {
                "durree": forfait_select.durree,
                "reduction": forfait_select.reduction,
                "quantite": 1,
            }
        }

    request.session["panier"] = panier
    return HttpResponseRedirect(
            f"/groupe_service/?name={groupe_service}&methode=list"
        )


#
def loadPanierserviceaddqtt(request):
    panier = {} if "panier" not in request.session else request.session["panier"]
    if service_to_add_qtt := request.GET.get("service_to_add_qtt"):
        if reduction := request.GET.get("reduction"):
            if str(service_to_add_qtt) in panier:
                for key, value in panier[str(service_to_add_qtt)].items():
                    if value["reduction"] == int(reduction):
                        panier[str(service_to_add_qtt)][str(key)]["quantite"] = (
                            panier[str(service_to_add_qtt)][str(key)]["quantite"] + 1
                        )
                        break
    request.session["panier"] = panier
    return HttpResponseRedirect("/groupe_service/panier_services")


def loadPanierserviceremoveqtt(request):
    panier = {} if "panier" not in request.session else request.session["panier"]
    if service_to_add_qtt := request.GET.get("service_to_add_qtt"):
        if reduction := request.GET.get("reduction"):
            if str(service_to_add_qtt) in panier:
                for key, value in panier[str(service_to_add_qtt)].items():
                    if value["reduction"] == int(reduction):
                        if panier[str(service_to_add_qtt)][str(key)]["quantite"] > 1:
                            panier[str(service_to_add_qtt)][str(key)]["quantite"] = (
                                panier[str(service_to_add_qtt)][str(key)]["quantite"]
                                - 1
                            )
                        else:
                            del panier[str(service_to_add_qtt)][str(key)]
                        break

    request.session["panier"] = panier
    return HttpResponseRedirect("/groupe_service/panier_services")


def loadPanier(request):
    panier = {} if "panier" not in request.session else request.session["panier"]
    if service_to_delete := request.GET.get("service_to_delete"):
        if reduction := request.GET.get("reduction"):
            if str(service_to_delete) in panier:
                for key, value in panier[str(service_to_delete)].items():
                    if value["reduction"] == int(reduction):
                        del panier[str(service_to_delete)][str(key)]
                        if not panier[str(service_to_delete)]:
                            del panier[str(service_to_delete)]
                        request.session["panier"] = panier
                        break
    panier_application = (
        {}
        if "panier_application" not in request.session
        else request.session["panier_application"]
    )
    if app_to_delete := request.GET.get("app_to_delete"):
        if reduction := request.GET.get("reduction"):
            if app_to_delete in panier_application:
                for key, value in panier_application[str(app_to_delete)].items():
                    if value["reduction"] == int(reduction):
                        del panier_application[str(app_to_delete)][str(key)]
                        if not panier_application[str(app_to_delete)]:
                            del panier_application[str(app_to_delete)]
                        request.session["panier_application"] = panier_application
                        break
    context = load_panier_serveur(request)
    context.update(load_panier_application(request))
    try:
        tva_parameter = ParameterCloud.objects.get(name="tva")
        tva = int(tva_parameter.value)
    except Exception:
        tva = 0
    context.update(
        {
            "match_total": context["match_services_total"]
            + context["match_apps_total"],
            "tva": tva,
            "taxes_tva": (
                (context["match_services_total"] + context["match_apps_total"]) / 100
            )
            * tva,
        }
    )
    #
    return render(request, "panier_services.html", context=context)


def validerPanier(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(
            "/user/login?next_page=/groupe_service/panier_services"
        )

    panier = {} if "panier" not in request.session else request.session["panier"]
    panier_application = (
        {}
        if "panier_application" not in request.session
        else request.session["panier_application"]
    )
    if not panier and not panier_application:
        return HttpResponseRedirect("/user/account/commande")

    # if not services:
    #     return HttpResponseRedirect("/user/account/commande")
    if user_connect := UserCloud.objects.filter(id=request.user.id):
        proprietaire = user_connect[0]
    else:
        return HttpResponseRedirect("/user/login?next_page=/commande")
    proprietaire = UserCloud.objects.filter(id=request.user.id)[0]
    #
    match_services_panier_server = validerPanierServer(panier)
    match_apps_panier_server = validerPanierApp(panier_application)
    match_services = match_services_panier_server["match_services"]
    match_services_total = match_services_panier_server["match_services_total"]
    #
    match_apps = match_apps_panier_server["match_apps"]
    match_apps_total = match_apps_panier_server["match_apps_total"]
    #
    # tva
    if tva_parameter := ParameterCloud.objects.get(name="tva"):
        try:
            tva = int(tva_parameter.value)
        except Exception as e:
            tva = 0
    else:
        tva = 0
    #
    commande_dict = {
        "proprietaire": proprietaire,
        "price": sum([match_services_total, match_apps_total]),
        "TVA": tva,
    }

    commande_dict["total_price"] = format(
        commande_dict["price"]
        + ((commande_dict["TVA"] / 100) * commande_dict["price"]),
        ".2f",
    )

    commande = CommandeCloud.objects.create(**commande_dict)
    commande.save()
    create_commande_event = Event(
        name="Create Commande",
        object="commande",
        code=commande.code,
        created=datetime.now(),
    )

    create_commande_event.save()
    if not commande:
        return HttpResponseRedirect("/user/account/commande")
    save_ligne_commande_server(commande, match_services)
    save_ligne_commande_app(commande, match_apps)
    if request.session.get("panier"):
        del request.session["panier"]
    if request.session.get("panier_application"):
        del request.session["panier_application"]
    return HttpResponseRedirect("/user/account/commande")


#
def save_ligne_commande_server(commande, match_services):
    for match_service in match_services:
        for detail in match_service["details"]:
            ligne_commande = LigneCommandeCloud()
            ligne_commande.service = match_service["service"]
            ligne_commande.quantity = detail["quantite"]
            ligne_commande.durree = detail["durree"]
            ligne_commande.reduction = detail["reduction"]
            ligne_commande.price = match_service["service"].price
            ligne_commande.total_price = (
                match_service["service"].price
                * (1 - detail["reduction"] / 100)
                * detail["durree"]
                * detail["quantite"]
            )
            ligne_commande.commande = commande
            ligne_commande.save()


#
def save_ligne_commande_app(commande, match_apps):
    for match_app in match_apps:
        for detail in match_app["details"]:
            ligne_commande_app = LigneCommandeAppCloud()
            ligne_commande_app.code = match_app["app"]["code"]
            ligne_commande_app.app_name = match_app["app"]["app_name"]
            ligne_commande_app.package = match_app["app"]["package"]
            ligne_commande_app.quantity = detail["quantite"]
            ligne_commande_app.durree = detail["durree"]
            ligne_commande_app.reduction = detail["reduction"]
            ligne_commande_app.price = match_app["app"]["price"]
            ligne_commande_app.total_price = (
                match_app["app"]["price"]
                * (1 - detail["reduction"] / 100)
                * detail["durree"]
                * detail["quantite"]
            )
            ligne_commande_app.commande = commande
            ligne_commande_app.save()


#
def validerPanierServer(panier):
    if panier:
        services = ServiceCloud.objects.filter(id__in=[int(id) for id in panier.keys()])
        match_services = [
            {"service": service, "details": list(panier[str(service.id)].values())}
            for service in services
        ]
        list_prix = []
        for elt in match_services:
            for detail in elt["details"]:
                if detail["reduction"] == 0:
                    list_prix.append(
                        elt["service"].price * detail["durree"] * detail["quantite"]
                    )
                else:
                    prix_reduction_un_mois = (detail["reduction"] / 100) * elt[
                        "service"
                    ].price
                    prix_reduction_all_mois = prix_reduction_un_mois * detail["durree"]
                    prix_sans_reduction_all_mois = (
                        elt["service"].price * detail["durree"]
                    )
                    prix_unitaire_reduction_all_mois = (
                        prix_sans_reduction_all_mois - prix_reduction_all_mois
                    )
                    prix_unitaire_total_reduction = (
                        prix_unitaire_reduction_all_mois * detail["quantite"]
                    )
                    list_prix.append(prix_unitaire_total_reduction)
    else:
        match_services = []
        list_prix = [0]
    return {"match_services": match_services, "match_services_total": sum(list_prix)}


#
def validerPanierApp(panier_application):
    if panier_application:
        match_apps = [
            {
                "app": {
                    "code": elt,
                    "app_name": f"{DICT_APPLICATION[str(elt).split('**')[0]]}",
                    "package": f"{DICT_PACKAGE[str(elt).split('**')[1]][0]}",
                    "price": DICT_PACKAGE[str(elt).split("**")[1]][1],
                },
                "details": list(panier_application[elt].values()),
            }
            for elt in panier_application
        ]
        list_prix = []
        for elt in match_apps:
            for detail in elt["details"]:
                if detail["reduction"] == 0:
                    list_prix.append(
                        elt["app"]["price"] * detail["durree"] * detail["quantite"]
                    )
                else:
                    prix_reduction_un_mois = (detail["reduction"] / 100) * elt["app"][
                        "price"
                    ]
                    prix_reduction_all_mois = prix_reduction_un_mois * detail["durree"]
                    prix_sans_reduction_all_mois = (
                        elt["app"]["price"] * detail["durree"]
                    )
                    prix_unitaire_reduction_all_mois = (
                        prix_sans_reduction_all_mois - prix_reduction_all_mois
                    )
                    prix_unitaire_total_reduction = (
                        prix_unitaire_reduction_all_mois * detail["quantite"]
                    )
                    list_prix.append(prix_unitaire_total_reduction)

        match_apps_total = sum(list_prix)

    else:
        match_apps = []
        match_apps_total = 0
    #
    return {
        "match_apps": match_apps,
        "match_apps_total": match_apps_total,
    }


#
def CountPanierContent(request):
    try:
        counter = 0
        panier = {} if "panier" not in request.session else request.session["panier"]
        panier_application = (
            {}
            if "panier_application" not in request.session
            else request.session["panier_application"]
        )
        for elt in panier:
            counter = counter + len(panier[elt])
        for elt in panier_application:
            counter = counter + len(panier_application[elt])
        return JsonResponse({"counter": counter})
    except Exception:
        return JsonResponse({"counter": 0})


def get_forfait_for_app(request, application, package):
    if request.META.get("HTTP_HX_REQUEST") != "true":
        return redirect("/")
    if package not in DICT_PACKAGE.keys():
        return render(request, "page.html", context={"page_name": application})
    price = DICT_PACKAGE[package][1]
    forfaits = ForfaitCloud.objects.all()
    return render(
        request,
        "htmx/forfait_application.html",
        context={
            "forfaits": forfaits,
            "application": application.removeprefix("page_"),
            "package": package,
            "price": price,
        },
    )


def ajouter_application_panier(request, application_name, package):
    if (
        application_name not in DICT_APPLICATION.keys()
        or package not in DICT_PACKAGE.keys()
    ):
        return HttpResponseRedirect("/")
    panier_application = (
        {}
        if "panier_application" not in request.session
        else request.session["panier_application"]
    )
    forfait = int(request.POST.get("forfait")) or 0
    application_name_combine = f"{application_name}**{package}"
    if application_name_combine in panier_application:
        if str(forfait) in panier_application[application_name_combine]:
            if forfait == 0:
                panier_application[application_name_combine][str(forfait)][
                    "quantite"
                ] = (
                    panier_application[application_name_combine][str(forfait)][
                        "quantite"
                    ]
                    + 1
                )

            elif forfait_select := ForfaitCloud.objects.filter(id=forfait):
                forfait_select = forfait_select[0]
                panier_application[application_name_combine][str(forfait_select.id)][
                    "quantite"
                ] = (
                    panier_application[application_name_combine][
                        str(forfait_select.id)
                    ]["quantite"]
                    + 1
                )

        elif forfait == 0:
            panier_application[application_name_combine][str(forfait)] = {
                "durree": 1,
                "reduction": 0,
                "quantite": 1,
            }

        elif forfait_select := ForfaitCloud.objects.filter(id=forfait):
            forfait_select = forfait_select[0]
            panier_application[application_name_combine][str(forfait_select.id)] = {
                "durree": forfait_select.durree,
                "reduction": forfait_select.reduction,
                "quantite": 1,
            }

    elif forfait == 0:
        panier_application[application_name_combine] = {
            str(forfait): {"durree": 1, "reduction": 0, "quantite": 1}
        }

    elif forfait_select := ForfaitCloud.objects.filter(id=forfait):
        forfait_select = forfait_select[0]
        panier_application[application_name_combine] = {
            str(forfait_select.id): {
                "durree": forfait_select.durree,
                "reduction": forfait_select.reduction,
                "quantite": 1,
            }
        }

    request.session["panier_application"] = panier_application
    return HttpResponseRedirect(f"/page/page_{application_name}")


def load_panier_serveur(request):
    panier = {} if "panier" not in request.session else request.session["panier"]
    if panier:
        services = ServiceCloud.objects.filter(id__in=[int(id) for id in panier.keys()])
        match_services = [
            {"service": service, "details": list(panier[str(service.id)].values())}
            for service in services
        ]
        list_prix = []
        for elt in match_services:
            for detail in elt["details"]:
                if detail["reduction"] == 0:
                    list_prix.append(
                        elt["service"].price * detail["durree"] * detail["quantite"]
                    )
                else:
                    prix_reduction_un_mois = (detail["reduction"] / 100) * elt[
                        "service"
                    ].price
                    prix_reduction_all_mois = prix_reduction_un_mois * detail["durree"]
                    prix_sans_reduction_all_mois = (
                        elt["service"].price * detail["durree"]
                    )
                    prix_unitaire_reduction_all_mois = (
                        prix_sans_reduction_all_mois - prix_reduction_all_mois
                    )
                    prix_unitaire_total_reduction = (
                        prix_unitaire_reduction_all_mois * detail["quantite"]
                    )
                    list_prix.append(prix_unitaire_total_reduction)

        match_services_total = sum(list_prix)

    else:
        match_services = []
        match_services_total = 0
    #
    return {
        "match_services": match_services,
        "match_services_total": match_services_total,
    }


def load_panier_application(request):
    panier_application = (
        {}
        if "panier_application" not in request.session
        else request.session["panier_application"]
    )
    if panier_application:
        match_apps = [
            {
                "app": {
                    "code": elt,
                    "name": f"{DICT_APPLICATION[str(elt).split('**')[0]]} : {DICT_PACKAGE[str(elt).split('**')[1]][0]}",
                    "price": DICT_PACKAGE[str(elt).split("**")[1]][1],
                },
                "details": list(panier_application[elt].values()),
            }
            for elt in panier_application
        ]
        list_prix = []
        for elt in match_apps:
            for detail in elt["details"]:
                if detail["reduction"] == 0:
                    list_prix.append(
                        elt["app"]["price"] * detail["durree"] * detail["quantite"]
                    )
                else:
                    prix_reduction_un_mois = (detail["reduction"] / 100) * elt["app"][
                        "price"
                    ]
                    prix_reduction_all_mois = prix_reduction_un_mois * detail["durree"]
                    prix_sans_reduction_all_mois = (
                        elt["app"]["price"] * detail["durree"]
                    )
                    prix_unitaire_reduction_all_mois = (
                        prix_sans_reduction_all_mois - prix_reduction_all_mois
                    )
                    prix_unitaire_total_reduction = (
                        prix_unitaire_reduction_all_mois * detail["quantite"]
                    )
                    list_prix.append(prix_unitaire_total_reduction)

        match_apps_total = sum(list_prix)

    else:
        match_apps = []
        match_apps_total = 0
    #
    return {
        "match_apps": match_apps,
        "match_apps_total": match_apps_total,
    }
