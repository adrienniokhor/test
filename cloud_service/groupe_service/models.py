import datetime
from django.db import models
from django.forms import ModelForm
from django.urls import reverse
from phonenumber_field.modelfields import PhoneNumberField
from django_countries.fields import CountryField
from computedfields.models import ComputedFieldsModel, computed, compute


class GroupServiceCloud(ComputedFieldsModel):
    name = models.CharField("Nom", max_length=60, null=False, blank=True, unique=True)
    image = models.ImageField(
        "Image", upload_to="group_service/photos", null=False, blank=True, unique=True
    )
    description = models.TextField("Description")
    info_sup = models.TextField("Informations Supplémentaires", null=True, blank=True)
    enabled = models.BooleanField("Statut", default=True)

    @computed(
        models.CharField("Code", max_length=60, null=False, blank=True),
        depends=[("self", ["id"])],
    )
    def code(self):
        seq = f"00000000{str(self.id)}"
        seq_finale = seq[-8:]
        return f'GSERV_{str(datetime.date.today().strftime("%Y"))}_{seq_finale}'

    #
    def __str__(self):
        return self.name

    #
    def get_absolute_url(self):
        if self.code:
            return f"/adm/account/list/groupe_service/?code={self.code}"
        return reverse("admin_groupe_service")


class GroupServiceCloudForm(ModelForm):
    class Meta:
        model = GroupServiceCloud
        fields = ["name", "image", "description", "info_sup"]


#
