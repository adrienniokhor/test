from django import forms
from cloud_service.settings import DATE_INPUT_FORMATS

from user_cloud.models import OffreSurMesure


type_demande = [
    ("Projet Applicatif", "projet applicatif"),
    ("Conseil / Etude", "conseil / etude"),
    ("Projet d’Infrastructure", "projet d’infrastructure"),
    ("Evolution d’Application", "evolution d’application"),
    ("Evolution d’Infrastructure", "evolution d’infrastructure"),
    ("Autre", "autre"),
]
critique_demande = [("Base", "base"), ("Normale", "normale"), ("Haute", "haute")]


class OffreSurMesureForm(forms.Form):
    style_class_checkbox = "form-control px-1 py-1 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
    style_class = "form-control w-full px-3 py-2 text-md leading-tight border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
    message = forms.CharField(
        label="Message",
        max_length=200,
        widget=forms.Textarea(attrs={"rows": 5, "class": style_class}),
    )
    detail_demande = forms.CharField(
        label="Detail demande",
        max_length=200,
        widget=forms.Textarea(attrs={"rows": 5, "class": style_class}),
    )
    date_livraison_souhaite = forms.DateField(
        label="Date de livraison souhaitée",
        input_formats=DATE_INPUT_FORMATS,
        widget=forms.DateInput(
            attrs={"class": style_class, "placeholder": "jj/mm/aaaa"}
        ),
    )
    date_livraison_plus_tard = forms.DateField(
        label="Date de livraison plus tard",
        input_formats=DATE_INPUT_FORMATS,
        widget=forms.DateInput(
            attrs={"class": style_class, "placeholder": "jj/mm/aaaa"}
        ),
    )
    type_demande = forms.CharField(
        label="type de demande",
        max_length=100,
        widget=forms.Select(choices=type_demande, attrs={"class": style_class}),
    )
    critique_demande = forms.CharField(
        label="critique de la demande",
        max_length=100,
        widget=forms.Select(choices=critique_demande, attrs={"class": style_class}),
    )
    condition = forms.BooleanField(initial=True, required=False)

    class Meta:
        model = OffreSurMesure
